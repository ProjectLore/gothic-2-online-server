addEventHandler("onInit", function()
{
	print("ORS: Graphical User Interface");
});

//FONT_OLD_LOWER <- "FONT_OLD_10_WHITE_HI.TGA";
FONT_OLD_LOWER <- "MOYENAGE_10.TGA";
FONT_OLD_LOWER_BACK <- "MOYENAGE_12.TGA";
//FONT_OLD_UPPER <- "FONT_OLD_20_WHITE_HI.TGA";
FONT_OLD_UPPER <- "MOYENAGE_32.TGA";
BG_TEXTURE <- "BACKGROUND_01.TGA";
activeMenu <- 0;
local resolution = getResolution(); 

class GUI_Data
{
	constructor()
	{
		button_id = -1;
		slider_id = -1;
		edit_box_id = -1;
		choice_id = -1;
		p_button = -1;
	}
	
	function clean_collision()
	{
		p_button = button_id;
		choice_id = -1;
		slider_id = -1;
		edit_box_id = -1;
	}
	
	p_button = null;
	choice_id = null;
	button_id = null;
	slider_id = null;
	edit_box_id = null;
}

class GUI_Menu
{
        active = false;
        menu_pattern = null;
        button = null;
        button_count = -1;
        label_field = null;
        label_field_count = -1;
        pic = null;
        pic_count = -1;
        slider = null;
        slider_count = -1;
		active_slider = -1;
        edit_box = null;
        edit_box_count = -1;
        edit_block = null;
        edit_block_count = -1;
		choice_group = [];
		autoinput = -1;
        active_edb = -1;
		active_edblock = -1;
        x = null; y = null;
        length = null; height = null;
       
        bgless = false;
        chatless = false;
       
        constructor(cl,bl, posX, posY, lg, hg)
        {
                active = false;
                menu_pattern = null;
                button = [];
                button_count = -1;
                label_field = [];
                label_field_count = -1;
				pic = [];
				pic_count = -1;
                slider = [];
                slider_count = -1;
				active_slider = -1;
				autoinput = -1;
                edit_box = [];
                edit_box_count = -1;
                edit_block = [];
                edit_block_count = -1;
				choice_group = [];
                active_edb = -1;
       
                chatless = cl;
                bgless = bl;
                x = posX; y = posY; height = hg; length = lg;
                if(!bgless)
                    menu_pattern = txtCreate( x, y, length, height, BG_TEXTURE);
               
        }
       
		function setBackground(texture)
		{
			if(!bgless)
				txtSetFilename(menu_pattern, texture);
		}
		
        function createButton(posX, posY, length, height, name)
        {
                button_count++;
                button.append(GUI_Button(posX, posY, length, height, name));   
        }
       
        function createLabel(posX, posY, red, green, blue, text, font)
        {
                label_field_count++;
                label_field.append(GUI_Label(posX, posY, red, green, blue, text, font));               
        }
       
        function createPic(posX, posY, lg, hg, texture)
        {
                pic_count++;
                pic.append(GUI_Pic(posX, posY, lg, hg, texture));               
        }
		
        function createSlider(posX, posY, length, minimum, maximum)
        {
                slider_count++;
                slider.append(GUI_Slider(posX, posY, length, minimum, maximum));               
        }
       
        function createEditBox(posX, posY, length, height, font)
        {
                edit_box_count++;
                edit_box.append(GUI_EditBox(posX, posY, length, height, font));        
        }
       
        function createEditBlock(text, posX, posY, length, height, font, editable)
        {
                edit_block_count++;
                edit_block.append(GUI_EditBlock(text, posX, posY, length, height, font, editable));        
        }
		
	   function createChoiceGroup()
	   {
			choice_group.append(GUI_ChoiceGroup());
	   }
       
        function button_collide()
        {
                local c = getCursorPosition();
                local index = -1;
                if(button_count>=0)
                        for(local i=0; i<=button_count; i++)
                                if(button[i].collision(c.x, c.y)){index = i;}
                return index;
        }
       
		function choice_collide()
		{
			foreach(group in choice_group)
				group.collide();
		}
		
        function slider_collide()
        {
                local c = getCursorPosition();
                local index = -1;
                if(slider_count>=0)
                        for(local i=0; i<=slider_count; i++)
                                if(slider[i].collision(c.x, c.y)){index = i;}
								
				active_slider = index;
                return index;  
        }
       
        function eb_collide()
        {
                local c = getCursorPosition();
                local index = -1;
                if(edit_box_count>=0)
                        for(local i=0; i<=edit_box_count; i++)
                                if(edit_box[i].collision(c.x, c.y)){index = i;}
                return index;  
        }
       
        function eblock_collide()
        {
                local c = getCursorPosition();
                local index = -1;
                if(edit_block_count>=0)
                    for(local i=0; i<=edit_block_count; i++)
                        if(edit_block[i].collision(c.x, c.y) && edit_block[i].edit){index = i;}
                return index;  
        }
		
		function setAutoInput(id)
		{
			autoinput = id;
		}
		
		function update()
		{
			if(edit_box_count >= 0)
				for(local i=0; i<edit_box_count; i++)
					if(active_edb == i)
						edit_box[i].update(true);
					else
						edit_box[i].update(false);
						
			if(edit_box_count >= 0)
				for(local i=0; i<edit_block_count; i++)
					if(active_edblock == i)
						edit_block[i].update(true);
					else
						edit_block[i].update(false);
						
				if(chatless)
                        activeChat.hide();
                if(!bgless)
                        txtSetVisible(menu_pattern, true);
						
                if(button_count>=0)
                        for(local i=0; i<=button_count; i++)
                                button[i].show();
                if(label_field_count>=0)
                        for(local i=0; i<=label_field_count; i++)
                                label_field[i].show();
                if(pic_count>=0)
                        for(local i=0; i<=pic_count; i++)
                                pic[i].show();
                if(slider_count>=0)
                        for(local i=0; i<=slider_count; i++)
                                slider[i].show();
                if(edit_box_count>=0)
                        for(local i=0; i<=edit_box_count; i++)
                                edit_box[i].show();
								
				foreach(block in edit_block)
					block.show();
					
				foreach (group in choice_group)
					group.show();
		}
		
        function show()
        {	
			if(!user.onGUI)
			{
				activeMenu = this;
				//Visual setup
				setCursorTxt("CURSOR_01.TGA");
                active = true;
				user.onGUI = true;
                user.toggleFreeze(true);
                setCursorVisible(true);
                chatInputClose();
				
				//Player setups
				//playAni("S_RUN");
				//enableCameraMovement(false);
				setFreeze(true);
					
                enableKeys(false);
				
				if(autoinput > -1)
					{
						active_edb = autoinput;
						edit_box[autoinput].update(true);
					}
                if(chatless)
                        activeChat.hide();
                if(!bgless)
                        txtSetVisible(menu_pattern, true);
                if(button_count>=0)
                        for(local i=0; i<=button_count; i++)
                                button[i].show();
                if(label_field_count>=0)
                        for(local i=0; i<=label_field_count; i++)
                                label_field[i].show();
                if(pic_count>=0)
                        for(local i=0; i<=pic_count; i++)
                                pic[i].show();
                if(slider_count>=0)
                        for(local i=0; i<=slider_count; i++)
                                slider[i].show();
                if(edit_box_count>=0)
                        for(local i=0; i<=edit_box_count; i++)
                                edit_box[i].show();
								
				foreach(block in edit_block)
					block.show();
					
				foreach (group in choice_group)
					group.show();
			}
        }
       
        function hide()
        {
				activeMenu = 0;
                active = false;
				user.onGUI = false;
                user.toggleFreeze(false);
                setCursorVisible(false);
				
				
				//enableCameraMovement(true);
				if(!user.freezed)
					setFreeze(false);
                enableKeys(true);
				
                if(chatless)
                        activeChat.show();
                if(!bgless)
                        txtSetVisible(menu_pattern, false);
                if(button_count>=0)
                        for(local i=0; i<=button_count; i++)
                                button[i].hide();
                if(label_field_count>=0)
                        for(local i=0; i<=label_field_count; i++)
                                label_field[i].hide();
                if(pic_count>=0)
                        for(local i=0; i<=pic_count; i++)
                                pic[i].hide();
                if(slider_count>=0)
                        for(local i=0; i<=slider_count; i++)
                                slider[i].hide();
                if(edit_box_count>=0)
                        for(local i=0; i<=edit_box_count; i++)
                                edit_box[i].hide();
								
				foreach(block in edit_block)
					block.hide();
					
				foreach (group in choice_group)
					group.hide();
        }
       
}
 
class GUI_Button
{
        constructor(posX, posY, length, height, name)
        {
                x = posX; y = posY; lg = length; hg = height; label_name = name;
                background = txtCreate(posX, posY, length, height, "BUTTON_04.TGA");
				local labelx_back = x+(lg-drawGetWidth(drawCreate(0, 0, 0, 0, 0, FONT_OLD_LOWER_BACK, label_name)))/2;
				local labelx = x+(lg-drawGetWidth(drawCreate(0, 0, 0, 0, 0, FONT_OLD_LOWER, label_name)))/2;
				enabled = true;
                label_back = drawCreate(labelx_back.tointeger(), (y+hg/3), 0, 0, 0, FONT_OLD_LOWER_BACK, name);		
                label = drawCreate(labelx.tointeger(), (y+hg/3), 255, 255, 255, FONT_OLD_LOWER, name);		
				shown = false;
        }
       
		function setPosition(new_x, new_y)
		{
			x = new_x; y = new_y;
			txtSetPosition(background, x, y);
			local labelx = x+(lg-drawGetWidth(drawCreate(0, 0, 0, 0, 0, FONT_OLD_LOWER, label_name)))/2;
			local labelx_back = x+(lg-drawGetWidth(drawCreate(0, 0, 0, 0, 0, FONT_OLD_LOWER_BACK, label_name)))/2;
			drawSetPosition(label, (labelx), (y+hg/3));
			drawSetPosition(label_back, (labelx_back), (y+hg/3));
		}
		
        function show()
        {
				shown = true;
                txtSetVisible(background, true);
                drawSetVisible(label_back, true);
                drawSetVisible(label, true);
        }
		
		function enable(toggle)
		{
			if(toggle)
			{
				drawSetColor(label, 255, 255, 255);   	
			}
			else
			{
                drawSetColor(label, 100, 100, 100);	
			}
			
			enabled = toggle;
		}
       
        function hide()
        {
				shown = false;
                txtSetVisible(background, false);
                drawSetVisible(label_back, false);
                drawSetVisible(label, false);
       
        }
       
        function collision(cx, cy)
        {
			if(enabled)
                {
					if(cx >= x && cx <= x+lg && cy>=y && cy<=y+hg && shown)
							{
									drawSetColor(label, 153, 0, 0);
									return true;
							}
					else
							{
									drawSetColor(label, 255, 255, 255);                    
									return false;
							}
				}
			else
				return false;
        }
       
        label_name = null;
        lg = null;
        x = null;
        hg = null;
        y = null;
        background = null;
        label = null;
		label_back = null;
		shown = null;
		enabled = null;
}

local label_pointer = null;
function Clear_Label()
{
	drawSetText(label_pointer, "");
	label_pointer = null;
}

class GUI_Label
{
        constructor(posX, posY, red, green, blue, text, font)
        {
                x = posX; y = posY; r = red; g = green; b = blue; label = text;
                label_draw = drawCreate(x.tointeger(), y.tointeger(), r, g, b, font , label);
        }
       
        function show()
        {
                drawSetVisible(label_draw, true);
        }
		
		function update(text)
		{
			label = text;
			drawSetText(label_draw, label);
		}
       
	   function update_color(red, green, blue)
	   {
			r = red; g = green; b = blue;
			drawSetColor(label_draw, red, green, blue);
	   }
	   
	   function timed_clean(seconds)
	   {
			label_pointer = label_draw;
			setTimer(Clear_Label, seconds*1000, 1);
	   }
	   
        function hide()
        {
                drawSetVisible(label_draw, false);
        }
       
        label_draw = null;
        label = null;
        x = null;
        y = null;
        r = null;
        g = null;
        b = null;
}

class GUI_Pic
{
        constructor(posX, posY, lg, hg, tx)
        {
                x = posX; y = posY; length = lg; height = hg; texture = tx;
                pic_pattern = txtCreate(x, y, length, height, texture);
        }
       
        function show()
        {
                txtSetVisible(pic_pattern, true);
        }
		
		function update(tx)
		{
		
		}
       
        function hide()
        {
                txtSetVisible(pic_pattern, false);
        }
	
	x = null;
	y = null;
	length = null;
	height = null;
	texture = null;
	pic_pattern = null;
}

class GUI_Slider
{
        constructor(posX, posY, length, minimum, maximum)
        {
                x = posX; y = posY; lg = length; 
				min_value = minimum; value = minimum; max_value = maximum;
                x_pointer = posX;
                back = txtCreate(posX, posY, length, 200, "MENU_SLIDER_BACK.TGA");
                pointer = txtCreate(posX-150, (posY-50), length/15, (300), "MENU_SLIDER_POS.TGA");
               
        }
       
        function collision(cx, cy)
        {
                if(cx >= x && cx <= x+lg && cy>=y-50 && cy<=y+300)
                        {
                                return true;
                        }
                else
                        {                      
                                return false;
                        }
        }
       
        function show()
        {
                txtSetVisible(back, true);
                txtSetVisible(pointer, true);
        }
       
        function getValue(cx)
        {
			if(cx >= x && cx <= x+lg)
			{
				local scale = lg/(max_value - min_value);
				local valp = cx - x + scale/2;
				
				value = valp/(scale)+min_value;
				local newposX = x+(value*scale) - (min_value*scale);
				updatePointer(newposX);
			}
			else if (cx < x)
				value = min_value;
			else if (cx > x+lg)
				value = max_value;
			
			//print (value);
			return value;
        }
		
		function setValue(val)
		{
			local scale = lg/(max_value - min_value);
			value = val+min_value;
			local newposX = x+(value*scale) - (min_value*scale);
			updatePointer(newposX);
		}
		
		function update_range(minv, maxv)
		{
			min_value = minv;
			max_value = maxv;
		}
       
        function updatePointer(to_x)
        {
                x_pointer = to_x;
                txtSetPosition(pointer, x_pointer-(lg/15)/2, y-50);
        }
       
        function hide()
        {
                txtSetVisible(back, false);
                txtSetVisible(pointer, false);
        }
       
        back = null;
        pointer = null;
        min_value = null;
        max_value = null;
        value = null;
        x_pointer = null;
        x = null;
        y = null;
        lg = null;
        hg = null;
}
 
class GUI_EditBox
{
        constructor(posX, posY, length, height, font)
        {
                x = posX.tointeger(); y = posY.tointeger(); lg = length; hg = height; fnt = font;
                back = txtCreate(x, y, length, height,"INV_SLOT.TGA");
                text_field = drawCreate(x+30, y+(height/3), 255, 255, 255, font, text);
        }
       
        function collision(cx, cy)
        {
                if(cx >= x && cx <= x+lg && cy>=y && cy<=y+hg)
                        {
                                return true;
                        }
                else
                        {                      
                                return false;
                        }
        }
       
       
        function addLitera(letter)
        {
				foreach(letter in text)
					if(letter == "\"")
						letter = "'";
				
                if(letter)
                {
                        text = format("%s%s",text,letter);
                }
				
				local text_width = drawGetWidth(drawCreate(0, 0, 0, 0, 0, fnt, text+"_"));
				
				local sliced=0;
				if(text_width <= lg)
					text_cut = text;
				else
					while(text_width > lg)
					{
						sliced++;
						text_cut = text.slice(sliced, text.len());
						text_width = drawGetWidth(drawCreate(0, 0, 0, 0, 0, fnt, text_cut+"_"));
					}
						
                mask = "";
                while(mask.len()<text_cut.len())
                        mask = format("%s%s",mask,"#");
						
				//Old school bitch//
				/*if(text.len()*130 > lg)
					text_cut = text.slice(text.len()-(lg/130), text.len());
				else
					text_cut = text;
					
                mask = "";
                while(mask.len()<text_cut.len())
                        mask = format("%s%s",mask,"#");
				*/
				
				
                update(true);
        }
       
        function deleteLitera()
        {
                if(text.len() > 0)
                {
                    text = text.slice(0, text.len()-1);
					
					local text_width = drawGetWidth(drawCreate(0, 0, 0, 0, 0, fnt, text+"_"));
					
					//local index;
					local sliced=0;
					if(text_width <= lg)
						text_cut = text;
					else
						while(text_width > lg)
						{
							sliced++;
							text_cut = text.slice(sliced, text.len());
							text_width = drawGetWidth(drawCreate(0, 0, 0, 0, 0, fnt, text_cut+"_"));
						}
						
					//Old School bitch
					/*if(text.len()*130 > lg)
						text_cut = text.slice(text.len()-(lg/130), text.len());
					else
						text_cut = text;
					
					mask = "";
					while(mask.len()<text_cut.len())
							mask = format("%s%s",mask,"#");*/
                }
                update(true);
        }
       
        function update(underline)
        {
			local ul = null;
			if(underline)
				ul = "_";
			else
				ul = "";
			
                if(masked)
                        drawSetText(text_field, mask+ul);
                else
                        drawSetText(text_field, text_cut+ul);
        }
       
        function show()
        {
                txtSetVisible(back, true);
                drawSetVisible(text_field, true);
        }
       
        function toggleMasked()
        {
                if(masked)
                        masked = false;
                else
                        masked = true;
        }
       
        function hide()
        {
                txtSetVisible(back, false);
                drawSetVisible(text_field, false);
        }
       
        masked = false;
        mask = "";
        back = null;
        text_field = null;
		fnt = null;
        text = "";
		text_cut = "";
        lg = null;
        hg = null;
        x = null;
        y = null;
}

class GUI_EditBlock
{
        constructor(txt, posX, posY, length, height, font, ea)
        {
                x = posX.tointeger(); y = posY.tointeger(); lg = length; hg = height; fnt = font;
                back = txtCreate(x, y, length, height,"DLG_CONVERSATION.TGA");
				text = txt;
				edit = ea;
				visible = false;
				elines = [];
				
				for(local l = 0; l < hg/200; l++)
					{
						if(edit)
							elines.append(drawCreate(x+30, y+200+l*200, 255, 255, 255, font, ""))
						else
							elines.append(drawCreate(x+30, y+200+l*200, 150, 150, 150, font, ""))
					}
				update_visual();
        }
       
        function collision(cx, cy)
        {
                if(cx >= x && cx <= x+lg && cy>=y && cy<=y+hg)
                        {
                                return true;
                        }
                else
                        {                      
                                return false;
                        }
        }
       
       
        function addLitera(letter)
        {
				foreach(letter in text)
					if(letter == "\"")
						letter = "'";
						
                if(letter)
                {
                        text = format("%s%s",text,letter);
                }
				
				local text_width = drawGetWidth(drawCreate(0, 0, 0, 0, 0, fnt, text+"_"));			
				
				update_visual();
                update(true);
        }
       
        function deleteLitera()
        {
                if(text.len() > 0)
                {
                    text = text.slice(0, text.len()-1);
					
                }
				
				update_visual();
                update(true);
        }
       
        function update(underline)
        {
			local ul = null;
			if(underline)
				ul = "_";
			else
				ul = "";
        }
		
		function setText(txt)
		{
			text = txt;
			update_visual();
		}
		
        function update_visual()
        {
			foreach(line in elines)
				drawSetText(line, "");
			local text_width = drawGetWidth(drawCreate(0, 0, 0, 0, 0, fnt, text));
			local cut_width = text_width;
			local math_lines = 0;
			local text_cut = text;
			local text_temp = text;
			math_lines = text_width/lg;
			
			for(local i = 0; i <= math_lines; i++)
			{
				local sliced = 0;
				
				while(cut_width > lg)
				{
					sliced++;
					text_cut = text_temp.slice(0, text_temp.len()-sliced);
					cut_width = drawGetWidth(drawCreate(0, 0, 0, 0, 0, fnt, text_cut));
				}
				
				drawSetText(elines[i], text_cut);
				if(sliced > 0)
					text_temp = text_temp.slice(text_cut.len(), text_temp.len());
				else
					text_temp = text_temp.slice(0, text_temp.len());
					
				text_cut = text_temp;
				cut_width = drawGetWidth(drawCreate(0, 0, 0, 0, 0, fnt, text_cut));
			}		
        }
		
       
        function show()
        {
			if(edit)
                txtSetVisible(back, true);
				foreach(line in elines)
					drawSetVisible(line, true);	
			visible = true;
        }
       
        function hide()
        {
			if(edit)
                txtSetVisible(back, false);
				foreach(line in elines)
					drawSetVisible(line, false);	
			visible = false;
        }
       
        back = null;
        text_field = null;
		elines = null;
		fnt = null;
		edit = null;
		visible = null;
        text = "";
		text_cut = "";
        lg = null;
        hg = null;
        x = null;
        y = null;
}

class GUI_ChoiceGroup
{
	constructor()
	{
		choices = [];
		active = 0;
	}
	
	function addChoice(posX, posY, length, height, name)
	{
		choices.append(GUI_Choice(posX, posY, length, height, name));
	}
	
	function collide()
	{
		local c = getCursorPosition();
		local index = 0;
		foreach(choice in choices)
		{
			if(choice.collision(c.x,c.y))
				return index;
			
			index++;
		}
		
		return -1;
	}
	
	function show()
	{
		foreach(choice in choices)
			choice.show();
	}
	
	function setActive(id)
	{
		choices[active].setIdle();
		active = id;
		choices[id].setActive();
	}
	
	function getActive()
	{
		return active;
	}
	
	function hide()
	{
		foreach(choice in choices)
			choice.hide();
	}

	choices = [];
	active = 0;
}

class GUI_Choice
{
	constructor(posX, posY, lg, hg, name)
        {
				active = false;
                x = posX.tointeger(); y = posY.tointeger(); length = lg; height = hg; label_name = name;
                background = txtCreate(posX, posY, length, height, "INV_SLOT.TGA");
				highlight = txtCreate(posX, posY, length, height, "INV_SLOT_HIGHLIGHTED.TGA")
				local labelx = x+(length-drawGetWidth(drawCreate(0, 0, 0, 0, 0, FONT_OLD_LOWER, name)))/2;
                label = drawCreate((labelx.tointeger()), (y+height/3), 255, 255, 255, FONT_OLD_LOWER, name);
        }
       
        function setActive()
        {
			active = true;
			if(txtIsVisible(background))
				txtSetVisible(highlight, true);
        }
       
		function setIdle()
		{
			active = false;
			txtSetVisible(highlight, false);		
		}
		
		function setText(new_text)
		{
			label_name = new_text;
			drawSetText(label, label_name);
			drawSetPosition(label, x+(length-drawGetWidth(drawCreate(0, 0, 0, 0, 0, FONT_OLD_LOWER, label_name)))/2, (y+height/3))
		}
		
        function show()
        {
                txtSetVisible(background, true);
                drawSetVisible(label, true);
				if(active)
					txtSetVisible(highlight, true);
					
        }
       
        function hide()
        {
                txtSetVisible(background, false);
                drawSetVisible(label, false);
				txtSetVisible(highlight, false);
        }
       
        function collision(cx, cy)
        {
                if(cx >= x && cx <= x+length && cy >= y && cy <= y+height)
                        {
                                drawSetColor(label, 153, 0, 0);
								txtSetFilename(background, "INV_BACK_SELL.TGA");
                                return true;
                        }
                else
                        {
                                drawSetColor(label, 255, 255, 255);      
								txtSetFilename(background, "INV_SLOT.TGA");              
                                return false;
                        }
        }
       
        label_name = null;
        length = null;
        x = null;
        height = null;
        y = null;
        background = null;
		highlight = null;
        label = null;
		active = false;

}

///////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////Menus Init/////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//Data
	gui_data <- GUI_Data();
//

main_menu <- GUI_Menu(true, false, 0, 0, 8192.0, 8192.0);
       
register_menu <- GUI_Menu(true, false, (8192/2)-(3000/2), 2600, 3000, 3600);
register_menu.setBackground("BACKGROUND_02.TGA");
               
login_menu <- GUI_Menu(true, false, (8192/2)-(3000/2), 2600, 3000, 3600);
login_menu.setBackground("BACKGROUND_02.TGA");
					
skinedit_menu <- GUI_Menu(true, true, 0, 0, 0, 0);
						
exit <- GUI_Menu(true, false, 2200, 2600, 3000, 1500);
exit.setBackground("BACKGROUND_02.TGA");

adminpanel <- GUI_Menu(true, false, 0, 0, 8192.0, 8192.0);

									freecam <- GUI_Menu(true, true, 0, 0, 8192.0, 8192.0);
									//freecam.createPic(6692, 6692, 1000, 1000, "VEXILLUM_01.TGA");
								
///////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////Menus Init END/////////////////////////////
///////////////////////////////////////////////////////////////////////////////
 
addEventHandler("onInit",function()
{
		createExiting();
        createMainMenu();
		createAdminPanel();
        createRegisterMenu();
        createLoginMenu();
		createSkineditMenu();
		createAnims();
       
});



//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////Exit////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

function createExiting()
{
        exit.createButton(2300, 3300, 1300, 500, "��");
        exit.createButton(3700, 3300, 1300, 500, "���");
        exit.createLabel(2700, 2800, 255, 255, 0, "����� �� ����?", FONT_OLD_UPPER);
}


//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////Admin Panel/////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

function createAdminPanel()
{
		adminpanel.createLabel(200, 200, 255, 255, 0, "����� ������", FONT_OLD_UPPER);
		adminpanel.createLabel(1000, 1000, 255, 255, 0, "��", FONT_OLD_LOWER);
        adminpanel.createEditBox(1000, 1200, 1000, 500, FONT_OLD_LOWER);
		adminpanel.createLabel(2200, 1000, 255, 255, 0, "�� ��������", FONT_OLD_LOWER);
		adminpanel.createLabel(2200, 1900, 255, 255, 0, "������: ", FONT_OLD_LOWER);
        adminpanel.createEditBox(2200, 1200, 1000, 500, FONT_OLD_LOWER);
		adminpanel.createLabel(3400, 1000, 255, 255, 0, "���-��", FONT_OLD_LOWER);
        adminpanel.createEditBox(3400, 1200, 1000, 500, FONT_OLD_LOWER);
        adminpanel.createButton(4600, 1200, 1000, 500, "������");
		adminpanel.createLabel(1000, 2600, 255, 255, 0, "��", FONT_OLD_LOWER);
        adminpanel.createEditBox(1000, 2800, 1000, 500, FONT_OLD_LOWER);
		adminpanel.createLabel(1000, 3500, 255, 255, 0, "������: ", FONT_OLD_LOWER);
        adminpanel.createButton(2200, 2800, 1000, 500, "���");
        adminpanel.createButton(3400, 2800, 1000, 500, "���");
        adminpanel.createButton(4600, 2800, 1000, 500, "���������");
        adminpanel.createButton(1000, 4000, 1000, 500, "� �����");
        adminpanel.createButton(2200, 4000, 1000, 500, "� ������");
        adminpanel.createButton(3400, 4000, 1000, 500, "� ��");
        adminpanel.createButton(4600, 4000, 1000, 500, "� ���");
        adminpanel.createButton(5800, 4000, 1000, 500, "� ����������");
        adminpanel.createButton(1000, 5200, 1000, 500, "� ��������");
        adminpanel.createButton(1000, 6200, 1000, 500, "�����. ����");
        adminpanel.createButton(2200, 6200, 1000, 500, "�����. ����");
        adminpanel.createButton(1000, 6900, 1500, 500, "��������������");
}

local TARGET_NAME = "NONE";
function returnedName(name)
{
	if(name)
		TARGET_NAME = name;
}


//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////Main Menu///////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

function createMainMenu()
{
        main_menu.createButton(1000, 1000, 2000, 500, "����������");
		
		
		main_menu.createLabel(4600, 1500, 160, 2, 2, "�����", FONT_OLD_UPPER);
        main_menu.createButton(1000, 5000, 2000, 500, "����� ���������");
        main_menu.createButton(4200, 2600, 2000, 500, "��������� ���������");
		
        main_menu.createButton(1000, 2000, 2000, 500, "���� ���� ������");
        main_menu.createButton(1000, 3000, 2000, 500, "����������");
        main_menu.createButton(1000, 4000, 2000, 500, "���������");
        main_menu.createButton(1000, 6000, 2000, 500, "�����");
		main_menu.createLabel(3600, 300, 160, 2, 2, "Official Russian Server RP: Role Play", FONT_OLD_UPPER);
		main_menu.createLabel(1200, 3700, 255, 0, 0, "", FONT_OLD_LOWER);
		main_menu.createLabel(3600, 700, 241, 247, 151, "����� ���������� � ��� ������ ������!", FONT_OLD_LOWER);
		main_menu.createLabel(3600, 900, 241, 247, 151, "T�� ����� �����", FONT_OLD_LOWER);
		main_menu.createLabel(3600, 1100, 241, 247, 151, "Numpad 1-7 - ���� ��������", FONT_OLD_LOWER);
		//main_menu.createPic(6692, 6692, 1000, 1000, "VEXILLUM_01.TGA");
		
		 
        //main_menu.show();
 
}
 

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////Register Menu///////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
function createRegisterMenu()
{
        register_menu.createLabel((8192/2)-(2100/2)-30, 2370, 0, 0, 0, "Official Russian Server RP", FONT_OLD_UPPER);
        register_menu.createLabel((8192/2)-(2100/2), 2400, 255, 255, 255, "Official Russian Server RP", FONT_OLD_UPPER);
        register_menu.createLabel(register_menu.x + ( register_menu.length - drawGetWidth(drawCreate(0, 0, 0, 0, 0, FONT_OLD_LOWER, "��� ��������")))/2, 3000, 220, 220, 220, "��� ��������", FONT_OLD_LOWER);
        register_menu.createLabel(register_menu.x + ( register_menu.length - drawGetWidth(drawCreate(0, 0, 0, 0, 0, FONT_OLD_LOWER, getPlayerName(heroId))))/2, 3300, 160, 2, 2, getPlayerName(heroId), FONT_OLD_LOWER);
        register_menu.createLabel(register_menu.x + ( register_menu.length - drawGetWidth(drawCreate(0, 0, 0, 0, 0, FONT_OLD_LOWER, "�������� ������ ��� ��������")))/2, 3600, 220, 220, 220, "�������� ������ ��� ��������", FONT_OLD_LOWER);
        register_menu.createEditBox((8192/2)-(2200/2), 4200, 2200, 350, FONT_OLD_LOWER);
		register_menu.setAutoInput(0);
        //register_menu.edit_box[0].toggleMasked();
        register_menu.createLabel((8192/2)-(3200/2), 4550, 255, 0, 0, "", FONT_OLD_LOWER);
        register_menu.createButton((8192/2)-(2200/2), 4750,2200,350, "����������������");
        register_menu.createButton((8192/2)-(2200/2), 5200,2200,350, "�����");
}
 

//////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////Login Menu///////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

function createLoginMenu()
{
        login_menu.createLabel((8192/2)-(2100/2)-30, 2370, 0, 0, 0, "Official Russian Server RP", FONT_OLD_UPPER);
        login_menu.createLabel((8192/2)-(2100/2), 2400, 255, 255, 255, "Official Russian Server RP", FONT_OLD_UPPER);
        login_menu.createLabel(login_menu.x + (login_menu.length - drawGetWidth(drawCreate(0, 0, 0, 0, 0, FONT_OLD_LOWER, "��� ��������")))/2, 3000, 220, 220, 220, "��� ��������", FONT_OLD_LOWER);
        login_menu.createLabel(login_menu.x + (login_menu.length - drawGetWidth(drawCreate(0, 0, 0, 0, 0, FONT_OLD_LOWER, getPlayerName(heroId))))/2, 3300, 160, 2, 2, getPlayerName(heroId), FONT_OLD_LOWER);
        login_menu.createLabel(login_menu.x + (login_menu.length - drawGetWidth(drawCreate(0, 0, 0, 0, 0, FONT_OLD_LOWER, "������� ������ �� ��������")))/2, 3600, 220, 220, 220, "������� ������ �� ��������", FONT_OLD_LOWER);
        login_menu.createEditBox((8192/2)-(2200/2), 4200, 2200, 350, FONT_OLD_LOWER);
		login_menu.setAutoInput(0);
        //login_menu.edit_box[0].toggleMasked();
        login_menu.createLabel((8192/2)-(2200/2), 4650, 255, 0, 0, "", FONT_OLD_LOWER);
        login_menu.createButton((8192/2)-(2200/2), 4950, 2200, 350, "�����");
        login_menu.createButton((8192/2)-(2200/2), 5400, 2200, 350, "�����");
}
 

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////SkinEdit Menu///////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

function createSkineditMenu()
{
		//Елементы меню смены внешности
		skinedit_menu.createButton(0, 7692, 2500, 500, "<");
		skinedit_menu.createButton(5692, 7692, 2500, 500, ">");
		skinedit_menu.createButton(6000, 6000, 1500, 500, "��������� ����");
        skinedit_menu.createLabel(6000, 1700, 188, 188, 188, "���", FONT_OLD_LOWER);		
        skinedit_menu.createSlider(6000, 2000, 2000, 0, 1);
        skinedit_menu.createLabel(6000, 2300, 188, 188, 188, "�������� ����", FONT_OLD_LOWER);		
        skinedit_menu.createSlider(6000, 2600, 2000, 0, 40);
        skinedit_menu.createLabel(6000, 2900, 188, 188, 188, "������ ������", FONT_OLD_LOWER);		
        skinedit_menu.createSlider(6000, 3200, 2000, 0, 14);
        skinedit_menu.createLabel(1500, 1200, 188, 188, 188, "�������� ������", FONT_OLD_LOWER);		
        skinedit_menu.createSlider(1500, 1500, 5192, 0, 338);
}
 
function callRegister()
{
	register_menu.show();
}

function callLogin()
{
	login_menu.show();
}

addEventHandler("onRender",function()
{
	local cur = getCursorPosition();
        if(main_menu.active == true)
        {
				gui_data.button_id = main_menu.button_collide(); 
				main_menu.update();
        }
        if(adminpanel.active == true)
        {
			if(user.status != "Technical Admin" && user.status != "Admin" && user.status != "Game Master")
				adminpanel.hide();
			
                gui_data.button_id = adminpanel.button_collide(); 
				adminpanel.update();
				
				local item_id = adminpanel.edit_box[1].text;
				if(item_id)
					adminpanel.label_field[3].update("������: "+getItemName(item_id));
					
				if(adminpanel.active_edb == 3)
				{	
					local text = adminpanel.edit_box[3].text;
					if(text.len() > 0)
					{
						local id = sscanf("d",text);
					if (id[0] >= 0)
							callServerFunc("callNameById", getID(), id[0]);
					}
					adminpanel.label_field[6].update("������: "+TARGET_NAME);
				}
					
        }
        if(exit.active == true)
        {
                gui_data.button_id = exit.button_collide(); 
				exit.update();
        }
        if(register_menu.active == true)
        {
                gui_data.button_id = register_menu.button_collide();
				register_menu.update();
        }
        if(login_menu.active == true)
        {
                gui_data.button_id = login_menu.button_collide();   
				login_menu.update();                
        }
        if(skinedit_menu.active == true)
        {
                gui_data.button_id = skinedit_menu.button_collide();   
				skinedit_menu.update();  
				if (skinedit_menu.active_slider > -1)
					skinedit_menu.slider[skinedit_menu.active_slider].getValue(cur.x);              
        }
		
		
	if(gui_data.button_id != gui_data.p_button && gui_data.button_id >= 0)
	{
		GUI_Collide_Sound();
	}
	gui_data.clean_collision();
});

function ShowMainMenu()
{
	if(user.autorized && (user.status == "Admin" ||  user.status == "Technical Admin" || user.status == "Game Master"))
		main_menu.button[3].enable(true);
	else
		main_menu.button[3].enable(false);
		
	main_menu.show();
}
 
addEventHandler("onKey",function(key)
{
        switch(key)
        {
                case KEY_ESCAPE:
                        if(main_menu.active)
                                main_menu.hide();
                        else 
							ShowMainMenu()                  
                break;
				case KEY_F10:
					if(!freecam.active)
					{
						freecam.show();
						if(freecam.active)
						{
							setFreeze(false);
							//enableCameraMovement(true);
							setCursorVisible(false);
						}
					}
					else
					{
						freecam.hide();
					}
				break;
                /*case KEY_F8:
                        if(exit.active)
                                exit.hide();
                        else
                                exit.show();                    
                break;*/
        }
       
		if(adminpanel.active && key == KEY_ESCAPE)
			{
				adminpanel.hide(); 
				ShowMainMenu()
			}
			
        if (main_menu.active == true && main_menu.active_edb >= 0)
                        if(key == KEY_BACK)
                                main_menu.edit_box[main_menu.active_edb].deleteLitera();
                        else if (key == KEY_ESCAPE || key == KEY_RETURN)
                                main_menu.active_edb = -1;
                        else
                                main_menu.edit_box[main_menu.active_edb].addLitera(getKeyLetter(key));
 
 
        if (adminpanel.active == true && adminpanel.active_edb >= 0)
                        if(key == KEY_BACK)
                                adminpanel.edit_box[adminpanel.active_edb].deleteLitera();
                        else if (key == KEY_ESCAPE || key == KEY_RETURN)
                                adminpanel.active_edb = -1;
                        else
                                adminpanel.edit_box[adminpanel.active_edb].addLitera(getKeyLetter(key));
 
        if (register_menu.active == true && register_menu.active_edb >= 0)
                        if(key == KEY_BACK)
                                register_menu.edit_box[register_menu.active_edb].deleteLitera();
                        else if (key == KEY_ESCAPE)
                                register_menu.active_edb = -1;
						else if (key == KEY_RETURN)						
							if(register_menu.edit_box[0].text.len()>0)
								{
									local text = register_menu.edit_box[0].text;
									callServerFunc("UpdatePassword", getPlayerName(heroId), text);
									//help1.show();
								}
								else
								{
									register_menu.label_field[3].update("Password empty!");	
									register_menu.label_field[3].timed_clean(2);								
								}
                        else
                                register_menu.edit_box[register_menu.active_edb].addLitera(getKeyLetter(key));
                               
        if (login_menu.active == true && login_menu.active_edb >= 0)
                        if(key == KEY_BACK)
                                login_menu.edit_box[login_menu.active_edb].deleteLitera();
                        else if (key == KEY_ESCAPE)
                                login_menu.active_edb = -1;
						else if (key == KEY_RETURN)
							callServerFunc("RespondLogin", getID(), login_menu.edit_box[0].text)							
                        else
                                login_menu.edit_box[login_menu.active_edb].addLitera(getKeyLetter(key));
	
});

addEventHandler("onMouseClick", function(button)
{
	if(button == 0)
		{
			if(adminpanel.active == true)
					{      
							local edb_id = adminpanel.eb_collide();
							if(edb_id >= 0)
									adminpanel.active_edb = edb_id;
									
							local button_id = adminpanel.button_collide();
							switch(button_id)
							{
								case 0:
									local id = sscanf("d", adminpanel.edit_box[0].text);
									local item_id = adminpanel.edit_box[1].text;
									local amount = sscanf("d", adminpanel.edit_box[2].text);
										callServerFunc("callClientFunc", id[0], "giveItem", item_id, amount[0]);
								break;
								case 1:
									local id = sscanf("d", adminpanel.edit_box[3].text);
									callServerFunc("kick", id[0], getID(), "[�����������]");
								break;
								case 2:
									local id = sscanf("d", adminpanel.edit_box[3].text);
									callServerFunc("ban", id[0], getID(), "[�����������]");
								break;
								case 3:
									local id = sscanf("d", adminpanel.edit_box[3].text);
									callServerFunc("callClientFunc", id[0], "setHealth", 0);
								break;
								case 4:
									local id = sscanf("d", adminpanel.edit_box[3].text);
									if(user.status == "Admin" ||  user.status == "Technical Admin")
										callServerFunc("UpdateStatus", id[0], "User");
								break;
								case 5:
									local id = sscanf("d", adminpanel.edit_box[3].text);
									if(user.status == "Admin" ||  user.status == "Technical Admin")
										callServerFunc("UpdateStatus", id[0], "Admin");
								break;
								case 6:
									local id = sscanf("d", adminpanel.edit_box[3].text);
									if(user.status == "Admin" ||  user.status == "Technical Admin")
										callServerFunc("UpdateStatus", id[0], "Technical Admin");
								break;
								case 7:
									local id = sscanf("d", adminpanel.edit_box[3].text);
									if(user.status == "Admin" ||  user.status == "Technical Admin")
										callServerFunc("UpdateStatus", id[0], "Game Master");
								break;
								case 8:
									local id = sscanf("d", adminpanel.edit_box[3].text);
									if(user.status == "Admin" ||  user.status == "Technical Admin")
										callServerFunc("UpdateStatus", id[0], "Moderator");
								break;
								case 9:
									local id = sscanf("d", adminpanel.edit_box[3].text);
									if(user.status == "Admin" ||  user.status == "Technical Admin")
										callServerFunc("UpdateStatus", id[0], "Assistant");
								break;
								case 10:
									local id = sscanf("d", adminpanel.edit_box[3].text);
									local pos = getPlayerPosition(heroId)
									callServerFunc("setPlayerPosition", id[0],  pos.x,  pos.y,  pos.z);
								break;
								case 11:
									local id = sscanf("d", adminpanel.edit_box[3].text);
									callServerFunc("RequestTeleport", getID(), id[0]);
								break;
								case 12:
									adminpanel.hide();
									ShowInstanceMenu();
								break;
							}
					}
			if(main_menu.active == true)
					{      
							local button_id = main_menu.button_collide();
							switch(button_id)
							{
								case 0:
									main_menu.hide();
								break;
								case 1:
									//main_menu.hide();
									//soulmenu.show();
									main_menu.hide();
									playerLogout();
								break;
								case 2:
									main_menu.hide();
									enableSkinEdit();
								break;
								case 3:
									if(user.autorized && (user.status == "Admin" ||  user.status == "Technical Admin" || user.status == "Game Master"))
										{
											main_menu.hide();
											adminpanel.show();									
										}
									else 
										{
											main_menu.label_field[2].update("�� �� �����");
											main_menu.label_field[2].timed_clean(2);
											//local t = setTimer(main_menu.label_field[1].clean, 1500, false);
										}
								break;
								case 4:
									main_menu.hide();
									help1.show();
								break;
								case 5:
									main_menu.hide();
									CallSettings();
								break;
								case 6:
									main_menu.hide();
									exit.show();
								break;
							}
					}
			if(register_menu.active == true)
					{                      
							local edb_id = register_menu.eb_collide();
							if(edb_id >= 0)
									register_menu.active_edb = edb_id;
							
							local button_id = register_menu.button_collide();
							switch(button_id)
							{
								case 0:
								if(register_menu.edit_box[0].text.len()>0)
									{
										local text = register_menu.edit_box[0].text;
										callServerFunc("UpdatePassword", getPlayerName(heroId), text);
										//help1.show();
									}
									else
									{
										register_menu.label_field[3].update("Password empty!");	
										register_menu.label_field[3].timed_clean(2);								
									}
								break;
								case 1:
									playerExit();
								break;
							}
					}
			if(login_menu.active == true)
					{
							local edb_id = login_menu.eb_collide();
							if(edb_id >= 0)
									login_menu.active_edb = edb_id;
							
							local button_id = login_menu.button_collide();
							switch(button_id)
							{
								case 0:
									callServerFunc("RespondLogin", getID(), login_menu.edit_box[0].text)									
								break;
								case 1:
									playerExit();
								break;
							}
					}
			 if(exit.active == true)
					{
							local button_id = exit.button_collide();
							switch(button_id)
							{
								case 0:
									exit.hide();
									playerExit();									
								break;
								case 1:
									exit.hide();
								break;
							}
					}
		}
		
	if(gui_data.button_id >= 0 && button == 0)
		GUI_Click_Sound();
})