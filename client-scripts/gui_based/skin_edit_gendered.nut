class Skin_Edit
{
	bModel = null;
	bModelId = null;
	bTexture = null;
	hModel = null;
	hModelId = null;
	hTexture = null;
	
	constructor()
	{
		bModel = "NULL";
		bModelId = 0;
		bTexture = 0;
		hModel = "NULL";
		hModelId = 0;
		hTexture = 0;
		
	}
	
	function setValues(bM,bT,hM,hT)
	{
		bModel = bM; bTexture = bT; hModel = hM; hTexture = hT;
		setIds();
	}
	
	function setIds()
	{
		switch(bModel)
		{
			case "HUM_BODY_NAKED0":
				bModelId = 0;
			break;
			case "HUM_BODY_BABE0":
				bModelId = 1;		
			break;
		}
		
		switch(hModel)
		{
			case "HUM_HEAD_BALD":
				hModelId = 0;
			break;
			case "HUM_HEAD_FATBALD":
				hModelId = 1;
			break;
			case "HUM_HEAD_FIGHTER":
				hModelId = 2;
			break;
			case "HUM_HEAD_PONY":
				hModelId = 3;
			break;
			case "HUM_HEAD_PSIONIC":
				hModelId = 4;
			break;
			case "HUM_HEAD_THIEF":
				hModelId = 5;
			break;
			case "HUM_HEAD_BABEHAIR":
				hModelId = 6;
			break;
			case "HUM_HEAD_BABE1":
				hModelId = 7;
			break;
			case "HUM_HEAD_BABE2":
				hModelId = 8;
			break;
			case "HUM_HEAD_BABE3":
				hModelId = 9;
			break;
			case "HUM_HEAD_BABE4":
				hModelId = 10;
			break;
			case "HUM_HEAD_BABE5":
				hModelId = 11;
			break;
			case "HUM_HEAD_BABE6":
				hModelId = 12;
			break;
			case "HUM_HEAD_BABE7":
				hModelId = 13;
			break;
			case "HUM_HEAD_BABE8":
				hModelId = 14;
			break;
		}
	
	}
	
	function setModels()
	{
		switch(bModelId)
		{
			case 0:
				bModel = "HUM_BODY_NAKED0";
			break;
			case 1:
				bModel = "HUM_BODY_BABE0";		
			break;
		}
		switch(hModelId)
		{
			case 0:
				hModel = "HUM_HEAD_BALD";
			break;
			case 1:
				hModel = "HUM_HEAD_FATBALD";
			break;
			case 2:
				hModel = "HUM_HEAD_FIGHTER";
			break;
			case 3:
				hModel = "HUM_HEAD_PONY";
			break;
			case 4:
				hModel = "HUM_HEAD_PSIONIC";
			break;
			case 5:
				hModel = "HUM_HEAD_THIEF";
			break;
			case 6:
				hModel = "HUM_HEAD_BABEHAIR";
			break;
			case 7:
				hModel = "HUM_HEAD_BABE1";
			break;
			case 8:
				hModel = "HUM_HEAD_BABE2";
			break;
			case 9:
				hModel = "HUM_HEAD_BABE3";
			break;
			case 10:
				hModel = "HUM_HEAD_BABE4";
			break;
			case 11:
				hModel = "HUM_HEAD_BABE5";
			break;
			case 12:
				hModel = "HUM_HEAD_BABE6";
			break;
			case 13:
				hModel = "HUM_HEAD_BABE7";
			break;
			case 14:
				hModel = "HUM_HEAD_BABE8";
			break;
		}
	}
	
	
	function showVision()
	{
		setAdditionalVisual(bModel, bTexture, hModel, hTexture);
	}
	
	function saveVision()
	{
		showVision();
		callServerFunc("SaveVisual", getName(), bModel, bTexture, hModel, hTexture);	
		sendVisual();
	}
	
	bTexturesMale = [0, 1, 2, 3, 8, 9, 10];
	bTexturesFemale = [4, 5, 6, 7, 11, 12];
}

o_SE <- Skin_Edit();
local camera = createVob("", 0, 0, 0, 0, 0, 0);
local camera_angle = 0;

function initSE(bModel, bTexture, hModel, hTexture)
{
	o_SE.setValues(bModel, bTexture, hModel, hTexture);
	UpdateSESliders();
}

function UpdateSESliders()
{
	skinedit_menu.slider[0].setValue(o_SE.bModelId);
	if(o_SE.bModelId == 0)
		{
			skinedit_menu.slider[1].update_range(0, 5);
			skinedit_menu.slider[2].update_range(0, 5);
			skinedit_menu.slider[3].update_range(0, 136);
			for(local i = 0; i<6; i++)
				if (o_SE.bTexturesMale[i] == o_SE.bTexture)
					skinedit_menu.slider[1].setValue(i);
		}
	if(o_SE.bModelId == 1)
		{
			skinedit_menu.slider[1].update_range(0, 6);
			skinedit_menu.slider[2].update_range(6, 14);
			skinedit_menu.slider[3].update_range(137, 158);	
			for(local i = 0; i<7; i++)
				if (o_SE.bTexturesFemale[i] == o_SE.bTexture)
					skinedit_menu.slider[1].setValue(i);	
		}
	skinedit_menu.slider[2].setValue(o_SE.hModelId);
	skinedit_menu.slider[3].setValue(o_SE.hTexture);
}

function enableSkinEdit()
{
	local pos = getPosition();
	camera_angle = getAngle();
	setVobPosition(camera, pos.x, pos.y-100, pos.z);
	setAngle(camera_angle);
	setCameraBehindVob(camera);
	skinedit_menu.show();
}

function disableSkinEdit()
{
	setDefaultCamera();
	user.toggleFreeze(false);
	skinedit_menu.hide();
}

addEvent("onRender", function()
{
	local cur = getCursorPosition();
	if (skinedit_menu.active)
	{
		user.toggleFreeze(true);
		setCameraBehindVob(camera);
	}
	if (skinedit_menu.active && skinedit_menu.active_slider > -1)
	{
		switch(skinedit_menu.active_slider)
		{
			case 0: 
				o_SE.bModelId = skinedit_menu.slider[skinedit_menu.active_slider].getValue(cur.x);
				if(o_SE.bModel == 0)
					{
						skinedit_menu.slider[1].update_range(0, 5);
						o_SE.bTexture = o_SE.bTexturesMale[0];
						skinedit_menu.slider[2].update_range(0, 5);
						o_SE.bModelId = 0;
						skinedit_menu.slider[3].update_range(0, 136);
						o_SE.hTexture = 0;			
					}
				if(o_SE.bModel == 1)
					{
						skinedit_menu.slider[1].update_range(0, 6);
						o_SE.bTexture = o_SE.bTexturesFemale[0];
						skinedit_menu.slider[2].update_range(6, 14);
						o_SE.bModelId = 6;
						skinedit_menu.slider[3].update_range(137, 158);
						o_SE.hTexture = 137;						
					}
				o_SE.setModels();
				o_SE.showVision();
				UpdateSESliders();
			break;
			case 1: 
				if (o_SE.bModel == 0)
					o_SE.bTexture = o_SE.bTexturesMale[skinedit_menu.slider[skinedit_menu.active_slider].getValue(cur.x)];
				else if(o_SE.bModel == 1)
					o_SE.bTexture = o_SE.bTexturesFemale[skinedit_menu.slider[skinedit_menu.active_slider].getValue(cur.x)];
					
			break;
			case 2: 
				o_SE.hModelId = skinedit_menu.slider[skinedit_menu.active_slider].getValue(cur.x);
			break;
			case 3: 
				o_SE.hTexture = skinedit_menu.slider[skinedit_menu.active_slider].getValue(cur.x);
			break;
		}
		o_SE.setModels();
		o_SE.showVision();   
	}
});

addEvent("onClick",function(button, x, y, wheel)
{
	if(button == "LEFT_DOWN")
	{
		if(skinedit_menu.active == true)
        {                    
            local slider_id = skinedit_menu.slider_collide();
            /*if(slider_id >= 0)
			{
				switch(slider_id)
				{
					case 0: 
						o_SE.bModelId = skinedit_menu.slider[slider_id].getValue(x);
					break;
					case 1: 
						o_SE.bTexture = skinedit_menu.slider[slider_id].getValue(x);
					break;
					case 2: 
						o_SE.hModelId = skinedit_menu.slider[slider_id].getValue(x);
					break;
					case 3: 
						o_SE.hTexture = skinedit_menu.slider[slider_id].getValue(x);
					break;
				}
				o_SE.setModels();
				o_SE.showVision();
			}*/
			local button_id = skinedit_menu.button_collide();
			switch(button_id)
			{
				case 0:
					camera_angle-=30;
					//print(camera_angle);
					local pos = getPosition();
					//local range = 100;
					//local x = pos.x + (range - pos.x) * cos(camera_angle) - (range - pos.z) * sin(camera_angle);
					//local z = pos.z + (range - pos.z) * cos(camera_angle) + (range - pos.x) * sin(camera_angle);
					//local dx = x - pos.x;
					//local dz = z - pos.z;
					//local angle = atan(dx / dz) * 180 / 3.14;
					//setVobPosition(camera, pos.x, pos.y-100, pos.z);
					//setVobRotation(camera, camera_angle, 0, 0);
					setAngle(camera_angle);
				break;
				case 1:
					camera_angle+=30;
					//print(camera_angle);
					local pos = getPosition();
					//local range = 100;
					//local x = pos.x + (range - pos.x) * cos(camera_angle) - (range - pos.z) * sin(camera_angle);
					//local z = pos.z + (range - pos.z) * cos(camera_angle) + (range - pos.x) * sin(camera_angle);
					//local dx = x - pos.x;
					//local dz = z - pos.z;
					//local angle = atan(dx / dz) * 180 / 3.14;
					//setVobPosition(camera, pos.x, pos.y-100, pos.z);
					//setVobRotation(camera, camera_angle, 0, 0);
					setAngle(camera_angle);
				break;
				case 2:
					if(o_SE.bModelId >=0 && o_SE.bTexture >=0  && o_SE.hModelId >=0  && o_SE.hTexture >=0)
					{
						o_SE.saveVision();
						disableSkinEdit();
					}
					else
					{
						local c = getCursorPosition()
						if(!o_SE.bModel)
						{
							o_SE.bModelId = skinedit_menu.slider[0].value;
						}
						if(!o_SE.bTexture)
						{
							o_SE.bTexture = skinedit_menu.slider[1].value;
						}
						if(!o_SE.hModel)
						{
							o_SE.hModelId = skinedit_menu.slider[2].value;
						}
						if(!o_SE.hTexture)
						{
							if (o_SE.bModel == 0)
								o_SE.bTexture = o_SE.bTexturesMale[skinedit_menu.slider[skinedit_menu.active_slider].value];
							else if(o_SE.bModel == 1)
								o_SE.bTexture = o_SE.bTexturesFemale[skinedit_menu.slider[skinedit_menu.active_slider].value];
						}
						
						o_SE.setModels();
						o_SE.saveVision();
						disableSkinEdit();
					}
				break;
			}
        }
	}
	if(button == "LEFT_UP" && skinedit_menu.active)
		skinedit_menu.active_slider = -1;
});