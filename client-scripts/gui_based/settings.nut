function CallSettings()
{
	settings.setChoices();
	settings_menu.show();
}

class Settings
{
	constructor()
	{
		highlight = 0;
		mouse_sen = 3;
		sfx = 100;
		music = 100;
		sfx_remain = sfx;
		music_remain = music;
	}
	
	function loadSettings()
	{
		/*local file_pointer = io.file("ors_settings.txt","r");
		if(!file_pointer)
		{
			file_pointer = io.file("ors_settings.txt","a+");
			file_pointer.close();
			
			file_pointer = io.file("ors_settings.txt","w+");
			file_pointer.write("ORSSettings");
			file_pointer.write(format("%s %d","ChatHighlight =", highlight));
			file_pointer.write(format("%s %d","MouseSen =", mouse_sen));
			file_pointer.write(format("%s %d","SFX =", sfx));
			file_pointer.write(format("%s %d","Music =", music));
		}
		else
		{
			local params = " ";
			while(params = file_pointer.read(io_type.LINE))
				{
					local result = sscanf("ssd", params);
					
					if(result)
					{
						if(result[0] == "ChatHighlight")
							highlight = result[2];
						else if(result[0] == "MouseSen")
							mouse_sen = result[2];
						else if(result[0] == "SFX")
							sfx = result[2];
						else if(result[0] == "Music")
							music = result[2];
					}
					
					result = null;
				}
			setChoices();
		}
		file_pointer.close();*/
	}
	
	function setChoices()
	{
		settings_menu.choice_group[0].setActive(highlight);
		settings_menu.slider[0].setValue(mouse_sen*10);
		settings_menu.label_field[2].update("���������������� ����: " + mouse_sen);
		setCursorSensitivity(mouse_sen);
	}
	
	function saveSettings()
	{
		/*highlight = settings_menu.choice_group[0].getActive();
		setCursorSensitivity(mouse_sen);
		
		local file_pointer = io.file("ors_settings.txt","w+");
		file_pointer.write("WorldOfMordragSettings");
		file_pointer.write(format("%s %d","ChatHighlight =", highlight));
		file_pointer.write(format("%s %d","MouseSen =", mouse_sen));
		file_pointer.write(format("%s %d","SFX =", sfx));
		file_pointer.write(format("%s %d","Music =", music));
		file_pointer.close();*/
	}
	
	highlight = 1;
	mouse_sen = 3;
	sfx = 100;
	music = 100;
	sfx_remain = 1;
	music_remain = 1;
}

settings_menu <- GUI_Menu(true, false, 0, 0, 8192.0, 8192.0);
settings <- Settings();

addEventHandler("onInit", function()
{
	print("Official Russian Server RP: Settings setup");
	createSettings();
	settings.loadSettings();
});

function ShowSettings()
{
	settings_menu.show();
}

function createSettings()
{
	settings_menu.createLabel(2000, 1000, 160, 2, 2, "���������", FONT_OLD_UPPER);
	settings_menu.createChoiceGroup();
	settings_menu.createLabel(1000, 1500, 249, 255, 144, "������ ���� ����", FONT_OLD_LOWER);
	settings_menu.choice_group[0].addChoice(3000, 1500, 1000, 500, "����");
	settings_menu.choice_group[0].addChoice(4300, 1500, 1000, 500, "���");
	settings_menu.choice_group[0].setActive(0);
	settings_menu.createLabel(1000, 2500, 249, 255, 144, "���������������� ����: ", FONT_OLD_LOWER);
    settings_menu.createSlider(3500, 2500, 2000, 10, 70);
	
    settings_menu.createButton(2000, 7000, 1000, 500, "������"); 
    settings_menu.createButton(4000, 7000, 1000, 500, "���������"); 
}

addEventHandler("onRender",function()
{
    if(settings_menu.active == true)
        {
				local cur = getCursorPosition();
				gui_data.button_id = settings_menu.button_collide(); 
				settings_menu.choice_collide();
				settings_menu.update();
				
				if (settings_menu.active_slider > -1)
					settings.mouse_sen = 1.0*(settings_menu.slider[settings_menu.active_slider].getValue(cur.x)/10);
				settings_menu.label_field[2].update("���������������� ����: " + settings.mouse_sen);
        }
});

addEventHandler("onMouseClick", function(button)
{
	if(button == 0)
		if(settings_menu.active)
                {                       
					local button_id = settings_menu.button_collide();
					settings_menu.active_slider = settings_menu.slider_collide();
					if(button_id == 0)
						{
							settings_menu.hide();
							main_menu.show();
						}
					else if (button_id == 1)
						{
							settings.saveSettings();
							settings_menu.hide();
							main_menu.show();						
						}
						
					local choice_id = settings_menu.choice_group[0].collide();
					if(choice_id >= 0)
						settings_menu.choice_group[0].setActive(choice_id);
						
				}
				
});

addEventHandler("onMouseRelease", function(button)
{
	if(button == 0 && settings_menu.active)
		settings_menu.active_slider = -1;
});

addEventHandler("onKey",function(key)
{
        switch(key)
        {
                case KEY_ESCAPE:
                        if(settings_menu.active)
						{
							settings_menu.hide();   
							main_menu.show();
						}            
                break;
        }
});