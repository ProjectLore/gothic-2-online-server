local CHAT_MAX_LINES = 15;
local FREEZE_STATUS = false;
local resolution = getResolution();

FONT_CHAT_LOWER <- "FONT_OLD_10_WHITE_HI.TGA";
//FONT_CHAT_LOWER <- "TRAJAN_10.TGA";

class ChatLinePlayer
{
	constructor(pid, message)
	{
		m_FadeOut = true;
		m_Alpha = 0;
		m_Color = getPlayerColor(pid);
		m_Position = { x = 0, y = 0 };
		
		local name = getPlayerName(pid);
		local resolution = getResolution();
		
		m_Position.x = (8192.0 / resolution.x) * drawGetWidth(drawCreate(0, 0, 0, 0, 0, FONT_CHAT_LOWER, name)) + 80;
		m_Position.y = 0;
		
		m_DrawName = drawCreate(100, 0, m_Color.r, m_Color.g, m_Color.b, 0, FONT_CHAT_LOWER, name + ":");
		m_DrawMessage = drawCreate(50 + m_Position.x, 0, 255, 255, 255, 0, FONT_CHAT_LOWER, message);
	}
	
	function destroy()
	{
		drawDestroy(m_DrawName);
		drawDestroy(m_DrawMessage);
	}
	
	function show()
	{
		drawSetVisible(m_DrawName, true);
		drawSetVisible(m_DrawMessage, true);
	}
	
	function hide()
	{
		drawSetVisible(m_DrawName, false);
		drawSetVisible(m_DrawMessage, false);
	}
	
	function update(y)
	{
		m_Position.y = y;
	
		drawSetPosition(m_DrawName, 100, y);
		drawSetPosition(m_DrawMessage, 50 + m_Position.x, y);
	}
	
	function alphaVisible(trigger)
	{
		if(trigger)
		{
			drawSetColor(m_DrawMessage, m_Color.r, m_Color.g, m_Color.b, 255);
			drawSetColor(m_DrawName, m_Color.r, m_Color.g, m_Color.b, 255);
		}
		else
		{
			drawSetColor(m_DrawMessage, m_Color.r, m_Color.g, m_Color.b, 0);
			drawSetColor(m_DrawName, m_Color.r, m_Color.g, m_Color.b, 0);
		}
	}
	
	function fadeOut()
	{
		if (m_FadeOut)
		{
			m_Alpha += 30;
			
			if (m_Alpha > 255)
			{
				m_Alpha = 255;
				m_FadeOut = false;
			}

			drawSetColor(m_DrawName, m_Color.r, m_Color.g, m_Color.b);
			drawSetColor(m_DrawMessage, 255, 255, 255);
			drawSetAlpha(m_DrawName, m_Alpha);
			drawSetAlpha(m_DrawMessage, m_Alpha);
		}
	}
	
	function move(offset)
	{
		m_Position.y -= offset;
	
		drawSetPosition(m_DrawName, 100, m_Position.y);
		drawSetPosition(m_DrawMessage, 50 + m_Position.x, m_Position.y);
	}
	
	m_Color = null;
	m_Position = null;
	m_FadeOut = false;
	m_Alpha = 0;
	m_DrawName = -1;
	m_DrawMessage = -1;
}

class ChatLineServer
{
	constructor(message, r, g, b)
	{
		m_FadeOut = true;
		m_Alpha = 0;
		m_Color = { r = r, g = g, b = b};
		m_PositionY = 0;
		
		m_DrawMessage = drawCreate(100, 0, r, g, b, FONT_CHAT_LOWER, message);
		drawSetAlpha(m_DrawMessage, 0);
	}
	
	function destroy()
	{
		drawDestroy(m_DrawMessage);
	}
	
	function show()
	{
		drawSetVisible(m_DrawMessage, true);
	}
	
	function hide()
	{
		drawSetVisible(m_DrawMessage, false);
	}
	
	function update(y)
	{
		m_PositionY = y;
	
		drawSetPosition(m_DrawMessage, 100, m_PositionY);
	}
	
	function alphaVisible(trigger)
	{
		if(trigger)
			drawSetColor(m_DrawMessage, m_Color.r, m_Color.g, m_Color.b, 255);
		else
			drawSetColor(m_DrawMessage, m_Color.r, m_Color.g, m_Color.b, 0);
	}
	
	function fadeOut()
	{
		if (m_FadeOut)
		{
			m_Alpha += 30;
			
			if (m_Alpha > 255)
			{
				m_Alpha = 255;
				m_FadeOut = false;
			}

			drawSetColor(m_DrawMessage, m_Color.r, m_Color.g, m_Color.b);
			drawSetAlpha(m_DrawMessage, m_Alpha);
		}
	}
	
	function move(offset)
	{
		m_PositionY -= offset;
	
		drawSetPosition(m_DrawMessage, 100, m_PositionY);
	}
	
	m_Color = null;
	m_PositionY = -1;
	m_FadeOut = false;
	m_Alpha = 0;
	m_DrawMessage = -1;
}


class ChatLineAdditional
{
	constructor(mtype, message, r, g, b)
	{
		m_FadeOut = true;
		m_Alpha = 0;
		m_Color = { r = r, g = g, b = b};
		m_PositionY = 0;
		
		m_DrawMessage = drawCreate(150, 0, r, g, b, FONT_CHAT_LOWER, message);
		drawSetAlpha(m_DrawMessage, 0);
		m_Texture = txtCreate(100, 0, 200.0*(1.0*resolution.y/resolution.x), 200.0, "NULL.TGA");
		
		switch(mtype)
		{
			case "arena":
				txtSetFilename(m_Texture, "ARENA.TGA");
			break;
			case "info":
				txtSetFilename(m_Texture, "INFO.TGA");
			break;
			case "dice":
				txtSetFilename(m_Texture, "DICE.TGA");
			break;
		}
	}
	
	function destroy()
	{
		drawDestroy(m_DrawMessage);
	}
	
	function show()
	{
		drawSetVisible(m_DrawMessage, true);
		if(m_PositionY >= 0)
			txtSetVisible(m_Texture, true);
	}
	
	function hide()
	{
		drawSetVisible(m_DrawMessage, false);
		txtSetVisible(m_Texture, false);
	}
	
	function update(y)
	{
		m_PositionY = y;
	
		drawSetPosition(m_DrawMessage, 220.0*(1.0*resolution.y/resolution.x)+100, m_PositionY);
		txtSetPosition(m_Texture, 100, m_PositionY);
	}
	
	function alphaVisible(trigger)
	{
		if(trigger)
			drawSetColor(m_DrawMessage, m_Color.r, m_Color.g, m_Color.b, 255);
		else
			drawSetColor(m_DrawMessage, m_Color.r, m_Color.g, m_Color.b, 0);
	}
	
	function fadeOut()
	{
		if (m_FadeOut)
		{
			m_Alpha += 30;
			
			if (m_Alpha > 255)
			{
				m_Alpha = 255;
				m_FadeOut = false;
			}

			drawSetColor(m_DrawMessage, m_Color.r, m_Color.g, m_Color.b);
			drawSetAlpha(m_DrawMessage, m_Alpha);
		}
	}
	
	function move(offset)
	{
		m_PositionY -= offset;
	
		drawSetPosition(m_DrawMessage, 220.0*(1.0*resolution.y/resolution.x)+100, m_PositionY);
		txtSetPosition(m_Texture, 100, m_PositionY);
		
	}
	
	m_Color = null;
	m_PositionY = -1;
	m_FadeOut = false;
	m_Alpha = 0;
	m_DrawMessage = -1;
	m_Texture = null;
}

class Chat
{
	constructor(lines)
	{
		m_MaxLines = lines;
		m_isShowed = true;
		m_Active = false;
		m_Update = false;
		m_OffsetMove = 0;
		m_TargetId = -1;
		m_Command = "None";
		m_CommandDraw = drawCreate(50, lines * 200 + 50, 230, 230, 230, FONT_CHAT_LOWER, "");
		m_AUDraw = drawCreate(7392, 300, 230, 230, 230, FONT_CHAT_LOWER, "PgUp");
		m_ADDraw = drawCreate(7392, lines * 200 - 200, 230, 230, 230, FONT_CHAT_LOWER, "PgDn");
		m_AUTex = txtCreate(7942, 300, 200.0*(1.0*resolution.y/resolution.x), 200.0, "AUP.TGA");
		m_ADTex = txtCreate(7942, lines * 200 - 200, 200.0*(1.0*resolution.y/resolution.x), 200.0, "ADN.TGA");
		m_Queue = [];
		m_Lines = [];
		m_History_Lines = [];
		m_Above = 0;
		
		m_Background = txtCreate(0, 0, 8192, m_MaxLines*200+50, "BG_CHAT2.TGA");
		setTimer(function(chat)
		{
			if (chat.m_OffsetMove)
			{
				chat.m_OffsetMove -= 40;
				if (chat.m_OffsetMove < 0) chat.m_OffsetMove = 0;

				foreach (line in chat.m_Lines)
					line.move(40);
			}
			else if (chat.m_Queue.len() > 0 && chat.m_Above == 0)
			{
				chat.m_Lines.append(chat.m_Queue[0]);
				chat.update();
				chat.m_Queue.remove(0);
			}
			
			foreach (line in chat.m_Lines)
				line.fadeOut();
		}, 50, 0, this);
		
		chatInputSetPosition(50, lines * 200 + 100);
	}
	
	function set_highlight()
	{
		if(m_Active)
			{
				//txtSetFilename(m_Background, "INV_BACK_SELL.TGA");
				//setTimer(Disable_highlight, 5000, false);
			}
	}
	
	function disable_highlight()
	{
		txtSetFilename(m_Background, "INV_BACK.TGA");
	}
	
	function open()
	{
		chatInputClear();
		chatInputOpen();
		
		FREEZE_STATUS = isFrozen();
		setFreeze(true);
		local angle = getPlayerAngle(heroId);
		setCursorPosition(angle*(8192/360), 4600);
		setCursorTxt("NONE.TGA");
		setCursorVisible(true);
	}
	
	function send()
	{
		close();
	}
	
	function close()
	{
		setCursorVisible(false);
		chatInputClose();
		
		if (!FREEZE_STATUS)
			setFreeze(false);
	}
	
	function setCommand(cmd)
	{
		m_Command = cmd;
	}
	
	function setCommandId(cmd, id)
	{
		m_Command = cmd;
		m_TargetId = id;
		local CommandText = "";
		
		if(getPlayerName(id) != "NULL")
			switch(cmd)
			{
				case "��":
					CommandText = format("�� ��� %s:", getPlayerName(id));
					drawSetText(m_CommandDraw, CommandText);
				break;
				case "���":
					CommandText = format("����� ��� %s:", getPlayerName(id));
					drawSetText(m_CommandDraw, CommandText);
				break;
			}
		else 
			clear_command();
			
			
		drawSetVisible(m_CommandDraw, true);
		chatInputSetPosition(50+drawGetWidth(drawCreate(0, 0, 0, 0, 0, FONT_OLD_LOWER, CommandText)), m_MaxLines * 200 + 100);
	}
	
	function clear_command()
	{
		m_Command = "None";
		m_TargetId = -1;
		drawSetVisible(m_CommandDraw, false);
		chatInputSetPosition(50, m_MaxLines * 200 + 100);
	}
	
	function show()
	{
		if(settings.highlight)
			txtSetVisible(m_Background, true);
			
		m_Active = true;
		if(m_Above > 0)
			{
				foreach(line in m_Lines)
					line.hide();
				foreach(line in m_History_Lines)
					line.hide();
					
				for(local x = 0; x < CHAT_MAX_LINES; x++)
				{
					m_History_Lines[(m_History_Lines.len() - 1) - (x + m_Above)].update((CHAT_MAX_LINES - x - 1)*200);
					m_History_Lines[(m_History_Lines.len() - 1) - (x + m_Above)].show();
					m_History_Lines[(m_History_Lines.len() - 1) - (x + m_Above)].alphaVisible(true);
				}
			}
		else
		{
			foreach(line in m_History_Lines)
				line.hide();
			foreach (line in m_Lines)
				line.show();
		}
		
		if(m_History_Lines.len() > CHAT_MAX_LINES + m_Above)
		{
			drawSetText(m_AUDraw, format("PgUp(%d)", m_History_Lines.len() - (CHAT_MAX_LINES + m_Above)));
			txtSetVisible(m_AUTex, true);
			drawSetVisible(m_AUDraw, true);
		}
		else
		{
			txtSetVisible(m_AUTex, false);
			drawSetVisible(m_AUDraw, false);		
		}
		
		if(m_Above > 0)
		{
			drawSetText(m_ADDraw, format("PgDn(%d)", m_Above));
			txtSetVisible(m_ADTex, true);
			drawSetVisible(m_ADDraw, true);
		}
		else
		{
			txtSetVisible(m_ADTex, false);
			drawSetVisible(m_ADDraw, false);		
		}
	}
	
	function hide()
	{
		m_Active = false;
		foreach (line in m_Lines)
			line.hide();
			
		foreach(line in m_History_Lines)
			line.hide();
			
		txtSetVisible(m_Background, false);
		txtSetVisible(m_AUTex, false);
		txtSetVisible(m_ADTex, false);
		drawSetVisible(m_AUDraw, false);
		drawSetVisible(m_ADDraw, false);
	}
	
	function update()
	{
		local size = m_Lines.len();
		if (size > m_MaxLines)
		{
			m_Lines[0].hide();
			m_Lines[0].destroy();
			m_Lines.remove(0);
			
			m_OffsetMove += 200;
		}
				
		m_Lines.top().update((size - 1) * 200);
		if (m_isShowed && m_Active) show();
	}
	
	function playerMessage(pid, message)
	{
		message.tostring();
		local cut = message.len();
		local text_width = drawGetWidth(drawCreate(0, 0, 0, 0, 0, FONT_OLD_LOWER, message))+1200;
		while(text_width > 8192.0)
		{
			cut--;
			text_width = drawGetWidth(drawCreate(0, 0, 0, 0, 0, FONT_OLD_LOWER, message.slice(0, cut)))+1200;
		}
		
		
		if(cut != message.len())
			{
				m_Queue.append(ChatLinePlayer(pid, message.slice(0, cut)+"-"));
				m_Queue.append(ChatLinePlayer(pid, "-"+message.slice(cut, message.len())));
				m_History_Lines.append(ChatLinePlayer(pid, message.slice(0, cut)+"-"));
				m_History_Lines.append(ChatLinePlayer(pid, "-"+message.slice(cut, message.len())));
			}
		else
		{
			m_Queue.append(ChatLinePlayer(pid, message));
			m_History_Lines.append(ChatLinePlayer(pid, message));
		}
		
		while(m_History_Lines.len() > 100)
		{
			m_History_Lines[0].hide();
			m_History_Lines[0].destroy();
			m_History_Lines.remove(0);
		}
		
		set_highlight();
		if(m_Active && m_Lines.len() > 0)
			update();
	}
	
	function serverMessage(message, r, g, b)
	{
		message.tostring();
		local cut = message.len();
		local text_width = drawGetWidth(drawCreate(0, 0, 0, 0, 0, FONT_OLD_LOWER, message));
		while(text_width > 8192.0)
		{
			cut--;
			text_width = drawGetWidth(drawCreate(0, 0, 0, 0, 0, FONT_OLD_LOWER, message.slice(0, cut)));
		}
		
		
		if(cut != message.len())
			{
				m_Queue.append(ChatLineServer(message.slice(0, cut)+"-", r, g, b));
				m_Queue.append(ChatLineServer("-"+message.slice(cut, message.len()), r, g, b));
				m_History_Lines.append(ChatLineServer(message.slice(0, cut)+"-", r, g, b));
				m_History_Lines.append(ChatLineServer("-"+message.slice(cut, message.len()), r, g, b));
			}
		else
			{
				m_Queue.append(ChatLineServer(message, r, g, b));
				m_History_Lines.append(ChatLineServer(message, r, g, b));
			}
			
		while(m_History_Lines.len() > 100)
		{
			m_History_Lines[0].hide();
			m_History_Lines[0].destroy();
			m_History_Lines.remove(0);
		}
			
		set_highlight();
		if(m_Active && m_Lines.len() > 0)
			update();
	}
	
	function additionalMessage(mtype, message, r, g, b)
	{
		m_Queue.append(ChatLineAdditional(mtype, message, r, g, b));
		m_History_Lines.append(ChatLineAdditional(mtype, message, r, g, b));
		
		while(m_History_Lines.len() > 100)
		{
			m_History_Lines[0].hide();
			m_History_Lines[0].destroy();
			m_History_Lines.remove(0);
		}
		
		set_highlight();
		if(m_Active && m_Lines.len() > 0)
			update();
	}
	
	function moveUp()
	{
		if(m_History_Lines.len() > m_Above + CHAT_MAX_LINES)
			m_Above++;
	}
	
	function moveDown()
	{
		if(m_Above > 0)
			m_Above--;
	}
	
	m_isShowed = false;
	m_Update = false;
	m_OffsetMove = -1;
	m_MaxLines = -1;
	//m_MessagesCount = -1;
	//m_MessagePosition = -1;
	m_Lines = [];
	m_History_Lines = [];
	m_AUDraw = null;
	m_ADDraw = null;
	m_AUTex = null;
	m_ADTex = null;
	m_Above = 0;
	m_Queue = [];
	m_Background = null;
	m_Active = false;
	m_Command = null;
	m_CommandDraw = null;
	m_TargetId = null;
}

class ChatMark
{
	constructor(x, text)
	{
		unread = false;
		unread_count = 0;
		selected = false;
		idle = false;
		label = text;
		shown = false;
		local labelx = x+(800-drawGetWidth(drawCreate(0, 0, 0, 0, 0, FONT_CHAT_LOWER, text)))/2;
		background = txtCreate(x, 0, 800, 300, "BG_CHAT2.TGA");
		select_texture = txtCreate(x, 0, 800, 300, "INV_SLOT_HIGHLIGHTED.TGA");
		draw = drawCreate(labelx.tointeger(), 50, 255, 255, 255, FONT_OLD_LOWER, text);
		
		setIdle();
	}
	
	function setActive()
	{
		unread = false; idle = false; selected = true;
		drawSetText(draw, label+"(~)");
		drawSetColor(draw, 241, 247, 151);
		txtSetVisible(select_texture, true);
	}
	
	function setUnread()
	{
		idle = true; selected = false;
		if(unread)
				unread_count++;
		else 
			{
				unread_count = 1;
				unread = true;
			}
		
		drawSetText(draw, format("%s(%d)", label, unread_count));
		drawSetColor(draw, 255, 255, 0);
	}
	
	function setIdle()
	{
		unread = false; idle = true; selected = false;
		drawSetText(draw, label);
		drawSetColor(draw, 255, 255, 255);
		txtSetVisible(select_texture, false);	
	}
	
	function show()
	{
	
		if(settings.highlight)
			txtSetFilename(background, "BG_CHAT2.TGA");
		else
			txtSetFilename(background, "BG_CHAT1.TGA");
			
			
			
		txtSetVisible(background, true);	
			
		shown = true;
		drawSetVisible(draw, true);
		if(selected)
			txtSetVisible(select_texture, true);	
		
	}
	
	function hide()
	{
		shown = false;
		drawSetVisible(draw, false);
		txtSetVisible(background, false);	
		txtSetVisible(select_texture, false);		
	}
	
	draw = null;
	label = null;
	background = null;
	select_texture = null;
	unread = null;
	unread_count = null;
	selected = null;
	idle = null;
	shown = null;
	
}

activeMark <- null;
activeChat <- null;

function Disable_highlight()
{
	activeChat.disable_highlight();
}

// old chat function
function addMessage(r, g, b, message)
{
	roleplayChat.serverMessage(message, r, g, b);
}

addEventHandler("onInit", function()
{
	print("ORS: Chat module");
	roleplayChat <- Chat(CHAT_MAX_LINES);
	roleplayChat.show();
	globalChat <- Chat(CHAT_MAX_LINES);
	roleplayChat.hide();
	adminChat <- Chat(CHAT_MAX_LINES);
	adminChat.hide();
	statusChat <- Chat(CHAT_MAX_LINES);
	statusChat.hide();
	roleplayMark <- ChatMark(6592,"��");
	globalMark <- ChatMark(7392,"���");
	adminMark <- ChatMark(4992,"���");
	statusMark <- ChatMark(5792,"����");
	
	activeChat = roleplayChat;
	activeMark = roleplayMark;
	activeMark.setActive();
	
	ReceiveAdditionalPrivate("info", "(��) ������� ��� > ������ ID > ������ \"������\" > ������ ����� � ��������� (�����)", 255, 255, 0);
});

addEventHandler("onPlayerMessage", function(pid, message, r, g, b)
{
	if (pid == -1)
	{
		globalChat.serverMessage(message, r, g, b);
		if(globalMark.idle)
			globalMark.setUnread();
	}
	/*else
		{
		roleplayChat.playerMessage(pid, message);
		if(pid == getID())
			{
				user.message_count++;
				user.message.append(message);
				user.message_current = user.message_count+1;
			}
		}*/

});

function saveInput(message)
{
	user.message_count++;
	user.message.append(message);
	user.message_current = user.message_count+1;
}

function SendAdmin(message)
{ 
	callServerFunc("AdminPlayerMessage", getID(), message);
}

function ReceiveAdmin(id, message)
{
	if(id != -1)
	{
		adminChat.playerMessage(id, message);
	}
	else
		adminChat.serverMessage(message, 255, 255, 0);
		
	if(adminMark.idle)
		adminMark.setUnread();
}

function SendStatistic(message)
{
	callServerFunc("StatisticMessage", message);
}

function ReceiveStatistic(message)
{
	statusChat.serverMessage(message,  241, 247, 151);
	if(statusMark.idle)
		statusMark.setUnread();
}

function ReceiveRP(message, r, g, b)
{
	roleplayChat.serverMessage(message, r, g, b);
	if(roleplayMark.idle)
		roleplayMark.setUnread();
}

function SendReply(id, message)
{
	//adminChat.serverMessage(format("���������� %s'�: %s", getPlayerName(id), message),  255, 255, 0);
	callServerFunc("ReplyMessage", getID(), id, message);
}

function SendPrivate(id, message)
{
	globalChat.serverMessage(format("���������� %s'�: %s", getPlayerName(id), message),  255, 255, 0);
	callServerFunc("PrivateMessage", getID(), id, message);
}

function ReceivePrivate(id, message)
{
	globalChat.serverMessage(format("�� %s: %s", getPlayerName(id), message),  255, 255, 0);
	if(globalMark.idle)
		globalMark.setUnread();
}

function ReceiveReply(id, message)
{
	globalChat.serverMessage(format("(�����) �� %s: %s", getPlayerName(id), message),  255, 255, 0);
	if(globalMark.idle)
		globalMark.setUnread();
}

function ReceiveAdditionalServer(mtype, text, r, g, b)
{
	roleplayChat.additionalMessage(mtype, text, r, g, b);
	if(roleplayMark.idle)
		roleplayMark.setUnread();
}

function ReceiveAdditionalPrivate(mtype, text, r, g, b)
{
	globalChat.additionalMessage(mtype, text, r, g, b);
	if(globalMark.idle)
		globalMark.setUnread();
}

function ReceiveOOC(text, r, g, b)
{
	globalChat.serverMessage(text, r, g, b);
	if(globalMark.idle)
		globalMark.setUnread();
}

addEventHandler("onKey", function(key)
{
	if (chatInputIsOpen())
	{
		local ctext = chatInputGetText();
		local slash_index = null;
		if((slash_index = ctext.find("\\")) != null)
			{
				if(slash_index == 0)
				{
					chatInputClear();
					if(ctext.len() - 1 > slash_index)
					{
						chatInputSetText(format("/", ctext.slice(slash_index + 1, ctext.len() - 1)));
					}
					else
					{
						chatInputSetText("/");	
					}						
				}		
			}
			
		playGesticulation(getID());
		if (key == KEY_UP)
			if(user.message_count > -1 && user.message_current > 0)
				{
					user.message_current--;
					chatInputClear();
					chatInputSetText(user.message[user.message_current]);
				}
		if (key == KEY_DOWN)
			if(user.message_count > -1 && user.message_current < user.message_count)
				{
					user.message_current++;
					chatInputClear();
					chatInputSetText(user.message[user.message_current]);				
				}

		
		
		if (key == KEY_RETURN)
			{
				saveInput(chatInputGetText());
				
				switch(activeChat)
				{
					case roleplayChat:
						local input_text = chatInputGetText();
						input_text = strip(input_text);
						if(input_text.find("/") != 0 && input_text.len() > 0)
							callServerFunc("SendNormalMessage", getID(), input_text);
					break;
					case globalChat:
						if(activeChat.m_Command == "��")
						{
							local input_text = chatInputGetText();
							input_text = strip(input_text);
							if(input_text.len() > 0)
								SendPrivate(activeChat.m_TargetId, input_text);
						}
						else
						{
							local input_text = chatInputGetText();
							input_text = strip(input_text);
							if(input_text.find("/") != 0 && input_text.len() > 0)
								callServerFunc("SendOOC", getID(), input_text);
						}
					break;
					case adminChat:
						if(activeChat.m_Command == "���")
						{
							local input_text = chatInputGetText();
							input_text = strip(input_text);
							if(input_text.len() > 0)
								SendReply(activeChat.m_TargetId, input_text);
						}
						else
						{
							local input_text = chatInputGetText();
							input_text = strip(input_text);
							if(input_text.find("/") != 0 && input_text.len() > 0)
								SendAdmin(input_text);
						}
					break;
				}
				chatInputSend();
				activeChat.send();
				activeChat.clear_command();
			}
		else if (key == KEY_ESCAPE)
			{
				activeChat.clear_command();
				activeChat.close();
			}
			
		if(key == KEY_SPACE)
		{
			switch(activeChat)
			{
				case globalChat:
					local result = sscanf("d", chatInputGetText());
					if (result)
					{
						activeChat.setCommandId("��", result[0]);
						chatInputClear();
					}
				break;
				case adminChat:
					local result = sscanf("d", chatInputGetText());
					if (result)
					{
						activeChat.setCommandId("���", result[0]);
						chatInputClear();
					}
				break;
			}
		}
	}
	else if ((key == KEY_RETURN || key == KEY_T) && !user.onGUI && user.autorized && user.inGame)
		{
			activeChat.open();
		}
	else if (key == KEY_Y && !user.onGUI && user.autorized && user.inGame)
		{
			activeChat.open();
			playAni(heroId, "S_WALKL");
		}
	else if (key == KEY_U && !user.onGUI && user.autorized && user.inGame)
		{
			activeChat.open();
			playAni(heroId, "S_RUNL");
		}
	
	if(!user.onGUI)
	switch(key)
	{
		case KEY_PRIOR:
			activeChat.moveUp();
			activeChat.update();
		break;
		case KEY_NEXT:
			activeChat.moveDown();
			activeChat.update();
		break;
		case KEY_TILDE:
			if(!chatInputIsOpen())
			switch(activeChat)
			{
				case roleplayChat:
					activeChat.hide();
					activeChat = globalChat;
					activeChat.show();
					
					activeMark.setIdle();
					activeMark = globalMark;
					activeMark.setActive();
				break;
				case globalChat:
					if(user.autorized && (user.status == "Admin" ||  user.status == "Technical Admin" || user.status == "Game Master" || user.status == "Moderator"))
					{
						activeChat.hide();
						activeChat = adminChat;
						activeChat.show();
						
						activeMark.setIdle();
						activeMark = adminMark;
						activeMark.setActive();
					}
					else
					{
						activeChat.hide();
						activeChat = roleplayChat;
						activeChat.show();
						
						activeMark.setIdle();
						activeMark = roleplayMark;
						activeMark.setActive();					
					}
				break;
				case adminChat:
					if(user.autorized && (user.status == "Admin" ||  user.status == "Technical Admin" || user.status == "Game Master" || user.status == "Moderator"))
					{
						activeChat.hide();
						activeChat = statusChat;
						activeChat.show();
							
						activeMark.setIdle();
						activeMark = statusMark;
						activeMark.setActive();
					}
					else
					{
						activeChat.hide();
						activeChat = roleplayChat;
						activeChat.show();
						
						activeMark.setIdle();
						activeMark = roleplayMark;
						activeMark.setActive();					
					}
				break;
				case statusChat:
					activeChat.hide();
					activeChat = roleplayChat;
					activeChat.show();
					
					activeMark.setIdle();
					activeMark = roleplayMark;
					activeMark.setActive();
				break;
			
			}
		break;
	}
	
});

onChatAngle <- null;

addEventHandler("onRender", function()
{
	local mouseP = getCursorPosition();
	if(chatInputIsOpen())
		setPlayerAngle(heroId, mouseP.x/(8192/360));

	if(user.onGUI)
		{
			roleplayMark.hide();
			globalMark.hide();
			adminMark.hide();
			statusMark.hide();
		}
	else if(user.inGame)
		{
			if(!roleplayMark.shown)
				roleplayMark.show();
			if(!globalMark.shown)
				globalMark.show();
			if(!adminMark.shown)
				if(user.autorized && (user.status == "Admin" ||  user.status == "Technical Admin" || user.status == "Game Master" || user.status == "Moderator"))
					adminMark.show();
			if(!statusMark.shown)
				if(user.autorized && (user.status == "Admin" ||  user.status == "Technical Admin" || user.status == "Game Master" || user.status == "Moderator"))
					statusMark.show();		
		}
});