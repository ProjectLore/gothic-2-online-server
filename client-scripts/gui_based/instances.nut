addEventHandler("onInit", function()
{
	print("ORS: Instances"); instances_menu.hide();
	createInstancesMenu();
});

local resolution = getResolution();

instances_menu <- GUI_Menu(true, false, 0, 0, 8192.0, 8192.0);

function ShowInstanceMenu()
{
	instances_menu.show();
}

function createInstancesMenu()
{
	instances_menu.createLabel(2000, 1000, 160, 2, 2, "����������� ������ ������", FONT_OLD_UPPER);
    instances_menu.createButton(2000, 7000, 1000, 500, "�����"); 
    instances_menu.createButton(1000, 2000, 1000, 500, "����");
    instances_menu.createButton(1000, 2500, 1000, 500, "����");
    instances_menu.createButton(1000, 3000, 1000, 500, "��������");
    instances_menu.createButton(1000, 3500, 1000, 500, "���������");
    instances_menu.createButton(1000, 4000, 1000, 500, "������");
    instances_menu.createButton(1000, 4500, 1000, 500, "���������");
    instances_menu.createButton(1000, 5000, 1000, 500, "�������������");
    instances_menu.createButton(1000, 5500, 1000, 500, "����");
    instances_menu.createButton(1000, 6000, 1000, 500, "�����");
    instances_menu.createButton(2000, 2000, 1000, 500, "�������� �����");
    instances_menu.createButton(2000, 2500, 1000, 500, "������");
    instances_menu.createButton(2000, 3000, 1000, 500, "������ ������");
    instances_menu.createButton(2000, 3500, 1000, 500, "�����");
    instances_menu.createButton(2000, 4000, 1000, 500, "���������");
    instances_menu.createButton(2000, 4500, 1000, 500, "����������");
    instances_menu.createButton(2000, 5000, 1000, 500, "����");
    instances_menu.createButton(2000, 5500, 1000, 500, "����������");
    instances_menu.createButton(2000, 6000, 1000, 500, "����");
    instances_menu.createButton(3000, 2000, 1000, 500, "����������");
    instances_menu.createButton(3000, 2500, 1000, 500, "�����");
    instances_menu.createButton(3000, 3000, 1000, 500, "����������");
    instances_menu.createButton(3000, 3500, 1000, 500, "��������");
    instances_menu.createButton(3000, 4000, 1000, 500, "�����������");
    instances_menu.createButton(3000, 4500, 1000, 500, "�����(�����)");
    instances_menu.createButton(3000, 5000, 1000, 500, "������");
    instances_menu.createButton(3000, 5500, 1000, 500, "�������");
    instances_menu.createButton(3000, 6000, 1000, 500, "�������");
    instances_menu.createButton(4000, 2000, 1000, 500, "���������");
    instances_menu.createButton(4000, 2500, 1000, 500, "���������");
    instances_menu.createButton(4000, 3000, 1000, 500, "���������");
    instances_menu.createButton(4000, 3500, 1000, 500, "���");
    instances_menu.createButton(4000, 4000, 1000, 500, "��������1");
    instances_menu.createButton(4000, 4500, 1000, 500, "��������2");
    instances_menu.createButton(4000, 5000, 1000, 500, "��������3");
    instances_menu.createButton(4000, 5500, 1000, 500, "��������4");
    instances_menu.createButton(4000, 6000, 1000, 500, "������");
    instances_menu.createButton(5000, 2000, 1000, 500, "�������");
    instances_menu.createButton(5000, 2500, 1000, 500, "����������");
    instances_menu.createButton(5000, 3000, 1000, 500, "������");
    instances_menu.createButton(5000, 3500, 1000, 500, "���������");
    instances_menu.createButton(5000, 4000, 1000, 500, "���������");
    instances_menu.createButton(5000, 4500, 1000, 500, "�����������");
    instances_menu.createButton(5000, 5000, 1000, 500, "�����");
    instances_menu.createButton(5000, 5500, 1000, 500, "�����������");
    instances_menu.createButton(5000, 6000, 1000, 500, "�����");
    instances_menu.createButton(6000, 2000, 1000, 500, "������!");
    instances_menu.createButton(6000, 2500, 1000, 500, "������!");
    instances_menu.createButton(6000, 3000, 1000, 500, "�����");
    instances_menu.createButton(6000, 3500, 1000, 500, "���������");
    instances_menu.createButton(3000, 7000, 1000, 500, "������ �����������"); 
}

addEventHandler("onRender",function()
{
    if(instances_menu.active == true)
        {
                gui_data.button_id = instances_menu.button_collide(); 
				instances_menu.update();
        }
		
	if(gui_data.button_id != gui_data.p_button && gui_data.button_id >= 0)
	{
		GUI_Collide_Sound();
	}
	gui_data.clean_collision();
});

addEventHandler("onMouseClick", function(button)
{
	if(button == 0)
		if(instances_menu.active)
                {   
                    local button_id = instances_menu.button_collide();
					switch(button_id)
					{
						case 0:
							instances_menu.hide();
						break;
						case 1:
							setInstance("WOLF"); instances_menu.hide();
						break;
						case 2:
							setInstance("WARG"); instances_menu.hide();
						break;
						case 3:
							setInstance("SHADOWBEAST"); instances_menu.hide();
						break;
						case 4:
							setInstance("FIRE_SHADOWBEAST"); instances_menu.hide();
						break;
						case 5:
							setInstance("WHISP"); instances_menu.hide();
						break;
						case 6:
							setInstance("SCAVENGER"); instances_menu.hide();
						break;
						case 7:
							setInstance("SCAVENGER_GL"); instances_menu.hide();
						break;
						case 8:
							setInstance("SHEEP"); instances_menu.hide();
						break;
						case 9:
							setInstance("SNAPPER"); instances_menu.hide();
						break;
						case 10:
							setInstance("DRAGONSNAPPER"); instances_menu.hide();
						break;
						case 11:
							setInstance("TROLL"); instances_menu.hide();
						break;
						case 12:
							setInstance("TROLL_BLACK"); instances_menu.hide();
						break;
						case 13:
							setInstance("WARAN"); instances_menu.hide();
						break;
						case 14:
							setInstance("BLOODFLY"); instances_menu.hide();
						break;
						case 15:
							setInstance("SWAMPDRONE"); instances_menu.hide();
						break;
						case 16:
							setInstance("GIANT_RAT"); instances_menu.hide();
						break;
						case 17:
							setInstance("SWAMPRAT"); instances_menu.hide();
						break;
						case 18:
							setInstance("STONEPUMA"); instances_menu.hide();
						break;
						case 19:
							setInstance("STONEGUARDIAN"); instances_menu.hide();
						break;
						case 20:
							setInstance("GOLEM"); instances_menu.hide();
						break;
						case 21:
							setInstance("GOLEM_FIREGOLEM"); instances_menu.hide();
						break;
						case 22:
							setInstance("GOLEM_ICEGOLEM"); instances_menu.hide();
						break;
						case 23:
							setInstance("SWAMPGOLEM"); instances_menu.hide();
						break;
						case 24:
							setInstance("SWAMPSHARK"); instances_menu.hide();
						break;
						case 25:
							setInstance("LURKER"); instances_menu.hide();
						break;
						case 26:
							setInstance("MEATBUG"); instances_menu.hide();
						break;
						case 27:
							setInstance("GIANT_BUG"); instances_menu.hide();
						break;
						case 28:
							setInstance("MOLERAT"); instances_menu.hide();
						break;
						case 29:
							setInstance("ALLIGATOR"); instances_menu.hide();
						break;
						case 30:
							setInstance("DRACONIAN"); instances_menu.hide();
						break;
						case 31:
							setInstance("ORC_WARRIOR"); instances_menu.hide();
						break;
						case 32:
							setInstance("GRAVE_ORC_1"); instances_menu.hide();
						break;
						case 33:
							setInstance("GRAVE_ORC_2"); instances_menu.hide();
						break;
						case 34:
							setInstance("GRAVE_ORC_3"); instances_menu.hide();
						break;
						case 35:
							setInstance("GRAVE_ORC_4"); instances_menu.hide();
						break;
						case 36:
							setInstance("HARPIE"); instances_menu.hide();
						break;
						case 37:
							setInstance("MINECRAWLER"); instances_menu.hide();
						break;
						case 38:
							setInstance("MINECRAWLERWARRIOR"); instances_menu.hide();
						break;
						case 39:
							setInstance("DRAGON"); instances_menu.hide();
						break;
						case 40:
							setInstance("DRAGON_ROCK"); instances_menu.hide();
						break;
						case 41:
							setInstance("DRAGON_ICE"); instances_menu.hide();
						break;
						case 42:
							setInstance("DRAGON_FIRE"); instances_menu.hide();
						break;
						case 43:
							setInstance("ZOMBIE"); instances_menu.hide();
						break;
						case 44:
							setInstance("SWAMPZOMBIE"); instances_menu.hide();
						break;
						case 45:
							setInstance("GOBBO"); instances_menu.hide();
						break;
						case 46:
							setInstance("IRRLICHT"); instances_menu.hide();
						break;
						case 47:
							setInstance("KEILER"); instances_menu.hide();
						break;
						case 48:
							setInstance("DEMON"); instances_menu.hide();
						break;
						case 49:
							setInstance("FIREWARAN"); instances_menu.hide();
						break;
						case 50:
							setInstance("PC_HERO");
							user.loadStats();
							user.loadVisual();
							user.loadItems();
							instances_menu.hide();
						break;
					}
				}
});

addEventHandler("onKey",function(key)
{
        switch(key)
        {
                case KEY_ESCAPE:
                        if(instances_menu.active)
                            {
								instances_menu.hide();
							}							
                break;
        }
		
});