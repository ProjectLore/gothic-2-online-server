addEventHandler("onInit", function()
{
	print("ORS: Animated messages");
});

local stroke_timer = null;
local fading_timer = null;
local block_timer = null;
local current = 0;
local resolution = getResolution();

class MessageProceed
{
	constructor()
	{
		amount = -1;
		query_mes = [];
		typed = [];
		running = false;
		back = txtCreate(0, 0, 8192.0, 800, "PANEL_01.TGA");
		draw = drawCreate(0, 300, 255, 188, 0, "FONT_OLD_20_WHITE_HI.TGA", " ");
		drawSetAlpha(draw, 200);
	}
	
	function add(txt, animt)
	{
		amount ++;
		query_mes.append(txt);
		typed.append(animt);
		proceed();
	}
	
	function proceed()
	{
		if(current<=amount && !running)
		{
			local x = (8192.0-drawGetWidth(drawCreate(0, 0, 0, 0, 0, "FONT_OLD_20_WHITE_HI.TGA", query_mes[current])))/2;
			drawSetText(draw, query_mes[current]);
			drawSetPosition(draw, x, 300);
			drawSetColor(globalAM.draw, 255, 188, 0);
			drawSetAlpha(globalAM.draw, 200);
			if(typed[current] == "fade")
				{
					MPFading();	
					show();
				}
			if(typed[current] == "stroke")
				{
					MPStroke();
					show();
				}
		}
	}
	
	function show()
	{
		txtSetVisible(back, true);
		drawSetVisible(draw, true);
	}
	
	function hide()
	{
		txtSetVisible(back, false);
		drawSetVisible(draw, false);
	
	}
	
	amount = -1;
	query_mes = [null];
	typed = [null];
	running = false;
	draw = null;
	back = null;
}

local in_first = false;
local first_line = 3000;
local in_second = false;
local second_line = 3700;
local in_third = false;
local third_line = 4400;
class ReceiveDraws
{
	constructor()
	{
		x = 0; y = 0;
		shown = false;
		busy = false;
		text_x = 0;
		text = " ";
		length = 5000.0;
		back = txtCreate( x, y, length, 300, "PANEL_01.TGA");
		draw = drawCreate(x+50, y+50, 241, 247, 151,"FONT_OLD_10_WHITE_HI.TGA", text);
		drawSetAlpha(draw, 200);
	}

	function setPos(new_x, new_y)
	{
		x = new_x; y = new_y;
		drawSetPosition(draw, x+50, y+100);
		txtSetPosition(back, x-(length-text_x)+50, y);		
	}
	
	function setText(new_text)
	{
		text = new_text;
		text_x = 50+drawGetWidth(drawCreate(0, 0, 0, 0, 0, "FONT_OLD_10_WHITE_HI.TGA", text));
		drawSetText(draw, text);
	}
	
	function show()
	{
		txtSetVisible(back, true);
		drawSetVisible(draw, true);
	}
	
	function hide()
	{
		txtSetVisible(back, false);
		drawSetVisible(draw, false);
	}
	
	draw = null;
	back = null;
	text = null;
	length = null;
	x = null; 
	y = null;
	text_x = null;
	shown = null;
	busy = null;
}

class ReceiveProceed
{
	constructor()
	{
		active = false;
		amount = -1;
		query_mes = [];
		proceeded = -1;
		in_process = -1;
		mes_draw = [];
		mes_draw.append(ReceiveDraws());
		mes_draw.append(ReceiveDraws());
		mes_draw.append(ReceiveDraws());
	}
	
	function add(txt)
	{
		amount ++;
		query_mes.append(txt);
		proceed();
		//print("Added line: "+txt);
	}
	
	function proceed()
	{
		foreach(object in mes_draw)
		{
			if(!object.busy && !in_third && amount > proceeded)
			{
				proceeded ++;
				local text_x = 50+drawGetWidth(drawCreate(0, 0, 0, 0, 0, "FONT_OLD_10_WHITE_HI.TGA", query_mes[proceeded]));
				object.setPos(0-text_x, third_line);
				object.setText(query_mes[proceeded]);
				object.show();
				object.busy = true;
				in_third = true;
				//print("Active "+active);
				if(!active || block_timer == null)
					LocalAMSystem();
			}
		}
		
	}
	
	active = false;
	amount = -1;
	proceeded = -1;
	proceeded = -1;
	query_mes = [null];
	mes_draw = [null];
	in_process = -1;
}

globalAM <- MessageProceed();
localAM <- ReceiveProceed();

function receiveMessageAM(msg, typed)
{
	globalAM.add(msg, typed);
}

local fade = 0;
local fade_in = true;
local fade_out = false;

function MPFading()
{
	globalAM.running = true;
	drawSetColor(globalAM.draw, 255, 188, 0);
	drawSetAlpha(globalAM.draw, fade)
	
	if(fading_timer == null)
		{
			fading_timer = setTimer(MPFading, 50, 0);
		}
		
	if (fade_in && fade < 200)
		{
			fade += 5;
		}
	else if(fade_in && fade >= 200)
		{
			fade_in = false;
			fade_out = true;
		}
	else if (fade_out && fade > 0)
		{
			fade -= 5;
		}
	else if(fade_out && fade <= 0)
		{
		
			killTimer(fading_timer);
			fading_timer = null;
			
			fade_out = false; fade_in = true;
			globalAM.hide();
			current++;
			globalAM.running = false;	
			globalAM.proceed();
		}	
}

local cut = 0;
local cut_in = true;
local cut_out = false;

function MPStroke()
{
	local text = globalAM.query_mes[current];
	drawSetText(globalAM.draw, text.slice(0, cut));
	drawSetAlpha(globalAM.draw, 200)
	globalAM.running = true;
	
	if(stroke_timer == null)
		stroke_timer = setTimer(MPStroke, 50, 0);
	
	if (cut_in && cut < globalAM.query_mes[current].len())
		{
			cut += 1;
			
		}
	else if(cut_in && cut >= globalAM.query_mes[current].len())
		{
			cut_in = false;
			cut_out = true;
			setTimerInterval(stroke_timer, 2000);
		}
	else if (cut_out && cut > 0)
		{
			cut -= 1;
			setTimerInterval(stroke_timer, 50);
		}
	else if(cut_out && cut <= 0)
		{
			killTimer(stroke_timer);
			stroke_timer = null;
			
			cut_out = false; cut_in = true;
			globalAM.hide();
			current++;
			globalAM.running = false;	
			globalAM.proceed();
		}	
}


function updateBlocks()
{
	in_first = false;
	in_second = false;
	in_third = false;
	foreach(local_draw in localAM.mes_draw)
	if(local_draw.busy)
		{
			if(local_draw.y == third_line || local_draw.y > second_line)
				in_third = true;
			else if(local_draw.y <= second_line && local_draw.y > third_line)
				in_second = true;
			else if(local_draw.y <= first_line && local_draw.y > second_line)
				in_first = true;
		}
}

//Antipattern: Down Syndrome
function LocalAMSystem()
{
	updateBlocks();
	//print("In_First :"+ in_first);
	//print("In_Second :"+ in_second);
	//print("In_Third :"+ in_third);
	local processes_active = 0;
	//Show block
	foreach(local_draw in localAM.mes_draw)
	{
		if(local_draw.busy && !local_draw.shown && !in_second)
		{
			processes_active++;
			local text_x = 50+drawGetWidth(drawCreate(0, 0, 0, 0, 0, "FONT_OLD_20_WHITE_HI.TGA", local_draw.text));
			if(local_draw.x < 0)
				{
					local_draw.setPos(local_draw.x+text_x/25, third_line);
				}
			else
				{
					local_draw.shown = true;
				}
		}
	//Push Block Up
	if(local_draw.shown && local_draw.busy)
	{
			processes_active ++;
			//if(local_draw.y == second_line)
			//	{
			//		in_second = true;
			//		in_third = false;
			//		localAM.proceed();
			// }
			if(second_line < local_draw.y && local_draw.y <= third_line && !in_second)
				{
					local_draw.setPos(local_draw.x, local_draw.y-10);
				}
			//if(local_draw.y == first_line)
			//	{
			//		in_first = true;
			//		in_second = false;
			//	}
			if(first_line < local_draw.y && local_draw.y <= second_line && !in_first)
				{
					local_draw.setPos(local_draw.x, local_draw.y-10);
					localAM.proceed();
				}
		}	
		//Hide block
		if(local_draw.busy && local_draw.shown && local_draw.y == first_line)
		{
			processes_active++;
			local text_x = 50+drawGetWidth(drawCreate(0, 0, 0, 0, 0, "FONT_OLD_20_WHITE_HI.TGA", local_draw.text));
			if(local_draw.x > 0-text_x)
				{
					local_draw.setPos(local_draw.x-text_x/25, first_line);
				}
			else
				{
					in_first = false;
					local_draw.text = " ";
					local_draw.busy = false;
					local_draw.shown = false;
					local_draw.hide();
					localAM.proceed();
				}
		}
	}
	
	if(processes_active > 0)
		{
			localAM.active = true;
			if(!block_timer)
				block_timer = setTimer(LocalAMSystem, 50, 0);
		}
	else	
		{
			localAM.active = false;
			killTimer(block_timer); block_timer = null;
		}
}

addEventHandler("onEquip",  function(instance, synchronized)
{
	localAM.add(format("%s %s","������ ", getItemName(instance)));
});

addEventHandler("onTakeItem",  function(instance, amount, synchronized)
{
	if(user.autorized && user.fully_loaded)
	{
		localAM.add(format("%dx%s",amount, getItemName(instance)));
	}
});

function AddLocalAM(message)
{
	localAM.add(message);
}


addEventHandler("onRender", function()
{
	foreach(draw in localAM.mes_draw)
	{
		if(!user.onGUI && draw.busy)
			draw.show();
		else
			draw.hide();
	}
});