addEventHandler("onInit", function()
{
	print("Vexillum: Help documentation");
	createHelp();
});

local resolution = getResolution();

help1 <- GUI_Menu(true, false, 0, 0, 8192.0, 8192.0);
help2 <- GUI_Menu(true, false, 0, 0, 8192.0, 8192.0);
help3 <- GUI_Menu(true, false, 0, 0, 8192.0, 8192.0);
help4 <- GUI_Menu(true, false, 0, 0, 8192.0, 8192.0);

function ShowHelp()
{
	help1.show();
}

function createHelp()
{
	help1.createLabel(2000, 1000, 160, 2, 2, "������� �������", FONT_OLD_UPPER);
	help1.createPic(1000, 1200, 500, 500*(1.0*resolution.x/resolution.y), "ESC.TGA");
	help1.createLabel(1600, 1450, 249, 255, 144, " <- ����/�������", FONT_OLD_LOWER);
	help1.createPic(1000, 2000, 500, 500*(1.0*resolution.x/resolution.y), "F4.TGA");
	help1.createLabel(1600, 2250, 249, 255, 144, " <- ������ �������", FONT_OLD_LOWER);
	help1.createPic(1000, 2800, 500, 500*(1.0*resolution.x/resolution.y), "F10.TGA");
	help1.createLabel(1600, 3050, 249, 255, 144, " <- �������� ��������� (�������� ��) (� �������� ����)", FONT_OLD_LOWER);
	help1.createPic(1000, 3600, 500, 500*(1.0*resolution.x/resolution.y), "NUM5.TGA");
	help1.createLabel(1600, 3850, 249, 255, 144, " <- �������� (� �������� ����)", FONT_OLD_LOWER);
	help1.createPic(1000, 4400, 500, 500*(1.0*resolution.x/resolution.y), "NUM6.TGA");
	help1.createLabel(1600, 4650, 249, 255, 144, " <- ������� (� �������� ����)", FONT_OLD_LOWER);
	help1.createPic(1000, 5200, 500, 500*(1.0*resolution.x/resolution.y), "NUM7.TGA");
	help1.createLabel(1600, 5450, 249, 255, 144, " <- ������ (��������) ���� (� �������� ����)", FONT_OLD_LOWER);
	help1.createPic(1000, 6000, 500, 500*(1.0*resolution.x/resolution.y), "M.TGA");
	help1.createLabel(1600, 6250, 249, 255, 144, " <- �������������� ������ (������ ������ ����) (� �������� ����)", FONT_OLD_LOWER);
    help1.createButton(4000, 7000, 1000, 500, "����. ��������"); 
	
	help2.createLabel(2000, 1000, 160, 2, 2, "����", FONT_OLD_UPPER);
	help2.createLabel(1000, 1500, 249, 255, 144, "(��) ������� ������ ���� \"���\" > ������� ��� > ������� ID + ������ ������ > ���������", FONT_OLD_LOWER);
	help2.createLabel(1000, 2000, 160, 2, 2, "���:", FONT_OLD_LOWER);
	help2.createLabel(1000, 2500, 249, 255, 144, "(����) /�(s) (�����) /�(w) (��) /��(do) (1 ����) /�(me)", FONT_OLD_LOWER);
	help2.createLabel(1000, 3000, 249, 255, 144, "(OOC) /o (G-OOC) /��(og) ", FONT_OLD_LOWER);
	help2.createLabel(1000, 4000, 160, 2, 2, "������ ������� �������", FONT_OLD_LOWER);
	help2.createLabel(1000, 4500, 249, 255, 144, "������ - ������������ ����� ������", FONT_OLD_LOWER);
	help2.createLabel(1000, 5000, 249, 255, 144, "L(�) - ���� �����", FONT_OLD_LOWER);
	help2.createLabel(1000, 5500, 249, 255, 144, "N - �������� ������ (� ������ ��������) �� ���������", FONT_OLD_LOWER);
	help2.createLabel(1000, 6000, 249, 255, 144, "- Read rules", FONT_OLD_LOWER);
    help2.createButton(2000, 7000, 1000, 500, "����. ��������"); 
    help2.createButton(4000, 7000, 1000, 500, "����. ��������"); 
	
	
	help3.createLabel(2000, 1000, 160, 2, 2, "���", FONT_OLD_UPPER);
	help3.createLabel(1000, 1500, 249, 255, 144, "��� ����� ��� �������� �����", FONT_OLD_LOWER);
	help3.createLabel(1000, 2000, 249, 255, 144, "����������, ��� �� ������ ������� � ����, � ������� ��� ������, ����� ���", FONT_OLD_LOWER);
    help3.createButton(2000, 7000, 1000, 500, "����. ��������"); 
    help3.createButton(4000, 7000, 1000, 500, "����. ��������"); 
	
	help4.createLabel(2000, 1000, 160, 2, 2, "��������", FONT_OLD_UPPER);
	help4.createLabel(1000, 2500,  249, 255, 144, "��� ����� ���������� ��� �������������", FONT_OLD_LOWER);
    help4.createButton(2000, 7000, 1000, 500, "����. ��������"); 
    help4.createButton(4000, 7000, 1000, 500, "���������� ����"); 
    help4.createButton(1200, 1500, 1750, 500, "�������� � ��");
    help4.createButton(3050, 1500, 1750, 500, "����������� � ��");
    help4.createButton(5000, 1500, 1750, 500, "����� �������");
}

addEventHandler("onRender",function()
{
    if(help1.active == true)
        {
                gui_data.button_id = help1.button_collide(); 
				help1.update();
        }
    if(help2.active == true)
        {
                gui_data.button_id = help2.button_collide(); 
				help2.update();
        }
    if(help3.active == true)
        {
				gui_data.button_id = help3.button_collide(); 
				help3.update();
        }
    if(help4.active == true)
        {
                gui_data.button_id = help4.button_collide(); 
				help4.update();
        }
		
	if(gui_data.button_id != gui_data.p_button && gui_data.button_id >= 0)
	{
		GUI_Collide_Sound();
	}
	gui_data.clean_collision();
});

addEventHandler("onMouseClick", function(button)
{
	if(button == 0)
		if(help1.active)
                {   
                    local button_id = help1.button_collide();
					if(button_id == 0)
						{
							help1.hide();
							help2.show();
						}
				}
		else if(help2.active)
                {   
                    local button_id = help2.button_collide();
					switch(button_id)
					{
						case 0:
							help2.hide();
							help1.show();
						break;
						case 1:
							help2.hide();
							help3.show();
						break;
					}
				}
		else if(help3.active)
                {   
                    local button_id = help3.button_collide();
					switch(button_id)
					{
						case 0:
							help3.hide();
							help2.show();
						break;
						case 1:
							help3.hide();
							help4.show();
						break;
					}
				}
		else if(help4.active)
                {   
                    local button_id = help4.button_collide();
					switch(button_id)
					{
						case 0:
							help4.hide();
							help3.show();
						break;
						case 1:
							help4.hide();
						break;
						case 2:
							//system("start https://vk.com/vexillum_games");
						break;
						case 3:
							//system("start https://vk.com/vexillum_project_tournament");
						break;
						case 4:
							//
						break;
					}
				}
	
});

addEventHandler("onKey",function(key)
{
        switch(key)
        {
                case KEY_ESCAPE:
                        if(help1.active)
                            {
								help1.hide(); 
								main_menu.show();
							}				
                        if(help2.active)
                            {
								help2.hide(); 
								main_menu.show();
							}									
                        if(help3.active)
							{
                                help3.hide();  
								main_menu.show();  
							}
                        if(help4.active)
							{
                                help4.hide();     
								main_menu.show();         
							}								
                break;
        }
		
});