local local_date = date();
local resolution = getResolution();

class StatusPanel
{
	constructor()
	{
		visible = false;
		first_text = format("%d:%d:%d", local_date.hour, local_date.min, local_date.sec);
		second_text = "";
		third_text  = "Official Russian Server RP";
		color_r = 241; color_g = 247; color_b = 151;
		slide_texture = txtCreate( 8192, 7192, 4000, 1000, "PANEL_01.TGA");
		corner_banner = txtCreate( 7192.0, 8192.0-(1000.0*(1.0*resolution.x/resolution.y)), 1000.0, 1000.0*(1.0*resolution.x/resolution.y), "CORNER_METAL.TGA");
		corner_left_banner = txtCreate(-50, 8192.0-(1000.0*(1.0*resolution.x/resolution.y)), 1000.0, 1000.0*(1.0*resolution.x/resolution.y), "CORNER_METAL_REVERSE.TGA");
		first_row_draw = drawCreate(4500, 7292, color_r, color_g, color_b, FONT_OLD_LOWER, first_text);
		drawSetAlpha(first_row_draw, 0);
		second_row_draw = drawCreate(4500, 7592, color_r, color_g, color_b, FONT_OLD_LOWER, second_text);
		drawSetAlpha(second_row_draw, 0);
		third_row_draw = drawCreate(4500, 7892, color_r, color_g, color_b, FONT_OLD_LOWER, third_text);
		drawSetAlpha(third_row_draw, 0);
		
	}
	
	function setText(labe1, labe2, labe3)
	{
		first_text = labe1; second_text = labe2; third_text = labe3;
		drawSetText(first_row_draw, first_text);
		drawSetText(second_row_draw, second_text);
		drawSetText(third_row_draw, third_text);
	}
	
	function setFirstRowText(label)
	{
		first_text = label;
		drawSetText(first_row_draw, first_text);
	}
	
	function setSecondRowText(label)
	{
		second_text = label;
		drawSetText(second_row_draw, second_text);
	}
	
	function setThirdRowText(label)
	{
		third_text = label;
		drawSetText(third_row_draw, third_text);
	}
	
	function show()
	{
		visible = true;
		txtSetVisible(slide_texture, true);
		//txtSetVisible(corner_banner, true);
		//txtSetVisible(corner_left_banner, true);
		drawSetVisible(first_row_draw, true);
		drawSetVisible(second_row_draw, true);
		drawSetVisible(third_row_draw, true);
	}
	
	function hide()
	{
		visible = false;
		txtSetVisible(slide_texture, false);
		//txtSetVisible(corner_banner, false);
		//txtSetVisible(corner_left_banner, false);
		drawSetVisible(first_row_draw, false);
		drawSetVisible(second_row_draw, false);
		drawSetVisible(third_row_draw, false);	
	}
	color_r = null;
	color_g = null;
	color_b = null;
	first_row_draw = null;
	second_row_draw = null;
	third_row_draw = null;
	first_text = null;
	second_text = null;
	third_text = null;
	slide_texture = null;
	corner_banner = null;
	corner_left_banner = null;
	visible = false;
}

status_panel <- StatusPanel();
addEventHandler("onInit", function()
{
	print("ORS: Status panel");
	status_panel.setText("DateTime","","Official Russian Server RP");
});

local open = false;

function OpenPanel()
{
	open = true;
	ManipulatePanel();
}

function setPanelText(txt1, txt2, txt3)
{
	status_panel.setText(txt1, txt2, txt3);
}

function setPanelText1(txt)
{
	status_panel.setFirstRowText(txt);
}

function setPanelText2(txt)
{
	status_panel.setSecondRowText(txt);
}

function setPanelText3(txt)
{
	status_panel.setThirdRowText(txt);
}

function ClosePanel()
{
	open = false;
	PanelTextFade();
}


function ManipulatePanel()
{
	local text_x;
	if(status_panel.first_text.len() >= status_panel.second_text.len() && status_panel.first_text.len() >= status_panel.third_text.len())
		text_x = 80 + drawGetWidth(drawCreate(0, 0, 0, 0, 0, FONT_OLD_LOWER, status_panel.first_text));
	else if (status_panel.second_text.len() >= status_panel.first_text.len() && status_panel.second_text.len() >= status_panel.third_text.len())
		text_x = 80 + drawGetWidth(drawCreate(0, 0, 0, 0, 0, FONT_OLD_LOWER, status_panel.second_text));
	else if (status_panel.third_text.len() >= status_panel.second_text.len() && status_panel.third_text.len() >= status_panel.first_text.len())
		text_x = 80 + drawGetWidth(drawCreate(0, 0, 0, 0, 0, FONT_OLD_LOWER, status_panel.third_text));
	local panel_pos = txtGetPosition(status_panel.slide_texture);
	if(open)
	{
		//print("opening panel "+ panel_pos.x)
		if(panel_pos.x > 8192.0-text_x)
			{
				txtSetPosition(status_panel.slide_texture, panel_pos.x-50, panel_pos.y);
				setTimer(ManipulatePanel, 50, 1);
			}
		else
			PanelTextFade();
	}
	else
		if(panel_pos.x < 8192)
			{
				//print("closing panel "+ panel_pos.x)
				txtSetPosition(status_panel.slide_texture, panel_pos.x+50, panel_pos.y);
				setTimer(ManipulatePanel, 50, 1);
			}
}

local fade = 0;
function PanelTextFade()
{
	
	drawSetColor(status_panel.first_row_draw, status_panel.color_r, status_panel.color_g, status_panel.color_b);
	drawSetAlpha(status_panel.first_row_draw, fade);
	drawSetColor(status_panel.second_row_draw, status_panel.color_r, status_panel.color_g, status_panel.color_b);
	drawSetAlpha(status_panel.second_row_draw, fade);
	drawSetColor(status_panel.third_row_draw, status_panel.color_r, status_panel.color_g, status_panel.color_b);
	drawSetAlpha(status_panel.third_row_draw, fade);
	//print("fade text"+fade);
	if (open && fade < 200)
		{
			fade += 20;
			setTimer(PanelTextFade, 50, 1);
		}
	else if (!open && fade > 0)
		{
			fade -= 20;
			setTimer(PanelTextFade, 50, 1);
		}
	else if(!open && fade <= 0)
		{
			ManipulatePanel();
		}	
}

addEventHandler("onRender", function()
{
	if(!user.onGUI && !status_panel.visible && user.autorized)
		status_panel.show();
	else if(user.onGUI && status_panel.visible && user.autorized)
		status_panel.hide();
		
	if(status_panel.visible)
	{	
		local game_time = getTime();
		local local_date = date();
		local hour = format("%d", local_date.hour);
		local minutes;
		if(local_date.min < 10)
			minutes = format("0%d", local_date.min);
		else 
			minutes = format("%d", local_date.min);
		local seconds;
		if(local_date.sec < 10)
			seconds = format("0%d", local_date.sec);
		else 
			seconds = format("%d", local_date.sec);
		local daytime = format("%s:%s:%s", hour, minutes, seconds);
		local daydate = format("%d/%d/%d", local_date.day, local_date.month+1, local_date.year);
		if(status_panel.first_text = "DateTime")
			status_panel.setFirstRowText("GT " + game_time.hour + ":" + game_time.min + " RT " + daytime + " " + daydate);
		
		// first row
		local fit = 8192.0-drawGetWidth(drawCreate(0, 0, 0, 0, 0, FONT_OLD_LOWER, status_panel.first_text));
		local static_pos = drawGetPosition(status_panel.first_row_draw);
		drawSetPosition(status_panel.first_row_draw, fit, static_pos.y);
		// second row
		fit = 8192.0-drawGetWidth(drawCreate(0, 0, 0, 0, 0, FONT_OLD_LOWER, status_panel.second_text));
		static_pos = drawGetPosition(status_panel.second_row_draw);
		drawSetPosition(status_panel.second_row_draw, fit, static_pos.y);
		// third row
		fit = 8192.0-drawGetWidth(drawCreate(0, 0, 0, 0, 0, FONT_OLD_LOWER, status_panel.third_text));
		static_pos = drawGetPosition(status_panel.third_row_draw);
		drawSetPosition(status_panel.third_row_draw, fit, static_pos.y);
	}
});

addEventHandler("onKey",function(key)
{
		if (!chatInputIsOpen())
        switch(key)
        {
			case KEY_COMMA:
				if(!user.onGUI)
					if(!open)
						{
						//	print("opening");
							OpenPanel();
						}
					else
						{
						//	print("closing");
							ClosePanel();
						}
			break;
		}
});