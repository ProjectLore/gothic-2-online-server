class Skin_Edit
{
	bModel = null;
	bModelId = null;
	bTexture = null;
	hModel = null;
	hModelId = null;
	hTexture = null;
	
	constructor()
	{
		bModel = "NULL";
		bModelId = 0;
		bTexture = 0;
		hModel = "NULL";
		hModelId = 0;
		hTexture = 0;
		
	}
	
	function setValues(bM,bT,hM,hT)
	{
		bModel = bM; bTexture = bT; hModel = hM; hTexture = hT;
		setIds();
	}
	
	function setIds()
	{
		switch(bModel)
		{
			case "HUM_BODY_NAKED0":
				bModelId = 0;
			break;
			case "HUM_BODY_BABE0":
				bModelId = 1;		
			break;
		}
		switch(hModel)
		{
			case "HUM_HEAD_BALD":
				hModelId = 0;
			break;
			case "HUM_HEAD_FATBALD":
				hModelId = 1;
			break;
			case "HUM_HEAD_FIGHTER":
				hModelId = 2;
			break;
			case "HUM_HEAD_PONY":
				hModelId = 3;
			break;
			case "HUM_HEAD_PSIONIC":
				hModelId = 4;
			break;
			case "HUM_HEAD_THIEF":
				hModelId = 5;
			break;
			case "HUM_HEAD_BABEHAIR":
				hModelId = 6;
			break;
			case "HUM_HEAD_BABE1":
				hModelId = 7;
			break;
			case "HUM_HEAD_BABE2":
				hModelId = 8;
			break;
			case "HUM_HEAD_BABE3":
				hModelId = 9;
			break;
			case "HUM_HEAD_BABE4":
				hModelId = 10;
			break;
			case "HUM_HEAD_BABE5":
				hModelId = 11;
			break;
			case "HUM_HEAD_BABE6":
				hModelId = 12;
			break;
			case "HUM_HEAD_BABE7":
				hModelId = 13;
			break;
			case "HUM_HEAD_BABE8":
				hModelId = 14;
			break;
		}
	
	}
	
	function setModels()
	{
		switch(bModelId)
		{
			case 0:
				bModel = "HUM_BODY_NAKED0";
			break;
			case 1:
				bModel = "HUM_BODY_BABE0";		
			break;
		}
		switch(hModelId)
		{
			case 0:
				hModel = "HUM_HEAD_BALD";
			break;
			case 1:
				hModel = "HUM_HEAD_FATBALD";
			break;
			case 2:
				hModel = "HUM_HEAD_FIGHTER";
			break;
			case 3:
				hModel = "HUM_HEAD_PONY";
			break;
			case 4:
				hModel = "HUM_HEAD_PSIONIC";
			break;
			case 5:
				hModel = "HUM_HEAD_THIEF";
			break;
			case 6:
				hModel = "HUM_HEAD_BABEHAIR";
			break;
			case 7:
				hModel = "HUM_HEAD_BABE1";
			break;
			case 8:
				hModel = "HUM_HEAD_BABE2";
			break;
			case 9:
				hModel = "HUM_HEAD_BABE3";
			break;
			case 10:
				hModel = "HUM_HEAD_BABE4";
			break;
			case 11:
				hModel = "HUM_HEAD_BABE5";
			break;
			case 12:
				hModel = "HUM_HEAD_BABE6";
			break;
			case 13:
				hModel = "HUM_HEAD_BABE7";
			break;
			case 14:
				hModel = "HUM_HEAD_BABE8";
			break;
		}
	}
	
	
	function showVision()
	{
		setModels();
		setPlayerVisual(getID(), bModel, bTexture, hModel, hTexture);
	}
	
	function saveVision()
	{
		showVision();
		callServerFunc("SaveVisual", user.character, bModel, bTexture, hModel, hTexture);
			
		callServerFunc("setPlayerVisual", heroId, bModel, bTexture, hModel, hTexture);
	}
	
}

o_SE <- Skin_Edit();
//local camera = createVob("", 0, 0, 0, 0, 0, 0);
local camera_angle = 0;

function initSE(bModel, bTexture, hModel, hTexture)
{
	o_SE.setValues(bModel, bTexture, hModel, hTexture);
	skinedit_menu.slider[0].setValue(o_SE.bModelId);
	skinedit_menu.slider[1].setValue(o_SE.bTexture);
	skinedit_menu.slider[2].setValue(o_SE.hModelId);
	skinedit_menu.slider[3].setValue(o_SE.hTexture);
}

function enableSkinEdit()
{
	local pos = getPlayerPosition(heroId)
	camera_angle = getPlayerAngle(heroId);
	//setVobPosition(camera, pos.x, pos.y-100, pos.z);
	setPlayerAngle(heroId, camera_angle);
	skinedit_menu.show();
	//enableCameraMovement(true);
	//setCameraBehindVob(camera);
}

function disableSkinEdit()
{
	//setDefaultCamera();
	user.toggleFreeze(false);
	skinedit_menu.hide();
	if(!user.inGame)
		ShowCharacterListMenu();
}

addEventHandler("onRender", function()
{
	local cur = getCursorPosition();
	if (skinedit_menu.active)
	{
		user.toggleFreeze(true);
	}
	if (skinedit_menu.active && skinedit_menu.active_slider > -1)
	{
		switch(skinedit_menu.active_slider)
		{
			case 0: 
				o_SE.bModelId = skinedit_menu.slider[skinedit_menu.active_slider].getValue(cur.x);
			break;
			case 1: 
				o_SE.bTexture = skinedit_menu.slider[skinedit_menu.active_slider].getValue(cur.x);
			break;
			case 2: 
				o_SE.hModelId = skinedit_menu.slider[skinedit_menu.active_slider].getValue(cur.x);
			break;
			case 3: 
				o_SE.hTexture = skinedit_menu.slider[skinedit_menu.active_slider].getValue(cur.x);
			break;
		}
		o_SE.setModels();
		o_SE.showVision();   
		
        skinedit_menu.createLabel(6000, 1700, 188, 188, 188, "Пол", FONT_OLD_LOWER);		
        skinedit_menu.createSlider(6000, 2000, 2000, 0, 1);
        skinedit_menu.createLabel(6000, 2300, 188, 188, 188, "Текстура тела", FONT_OLD_LOWER);		
        skinedit_menu.createSlider(6000, 2600, 2000, 0, 40);
        skinedit_menu.createLabel(6000, 2900, 188, 188, 188, "Модель головы", FONT_OLD_LOWER);		
        skinedit_menu.createSlider(6000, 3200, 2000, 0, 14);
        skinedit_menu.createLabel(1500, 1200, 188, 188, 188, "Текстура головы", FONT_OLD_LOWER);
		if(skinedit_menu.slider[0].value == 0)
			skinedit_menu.label_field[0].setText("Пол: Муж.")
		else
			skinedit_menu.label_field[0].setText("Пол: Жен.")
			
		skinedit_menu.label_field[1].setText("Текстура тела №" + skinedit_menu.slider[1].value)
		skinedit_menu.label_field[2].setText("Модель головы №" + skinedit_menu.slider[2].value)
		skinedit_menu.label_field[3].setText("Текстура головы №" + skinedit_menu.slider[3].value)
		
	}
});

addEventHandler("onMouseClick", function(button)
{
	if(button == 0)
	{
		if(skinedit_menu.active == true)
        {                    
            local slider_id = skinedit_menu.slider_collide();
            /*if(slider_id >= 0)
			{
				switch(slider_id)
				{
					case 0: 
						o_SE.bModelId = skinedit_menu.slider[slider_id].getValue(x);
					break;
					case 1: 
						o_SE.bTexture = skinedit_menu.slider[slider_id].getValue(x);
					break;
					case 2: 
						o_SE.hModelId = skinedit_menu.slider[slider_id].getValue(x);
					break;
					case 3: 
						o_SE.hTexture = skinedit_menu.slider[slider_id].getValue(x);
					break;
				}
				o_SE.setModels();
				o_SE.showVision();
			}*/
			local button_id = skinedit_menu.button_collide();
			switch(button_id)
			{
				case 0:
					camera_angle-=30;
					//print(camera_angle);
					local pos = getPlayerPosition(heroId)
					//local range = 100;
					//local x = pos.x + (range - pos.x) * cos(camera_angle) - (range - pos.z) * sin(camera_angle);
					//local z = pos.z + (range - pos.z) * cos(camera_angle) + (range - pos.x) * sin(camera_angle);
					//local dx = x - pos.x;
					//local dz = z - pos.z;
					//local angle = atan(dx / dz) * 180 / 3.14;
					//setVobPosition(camera, pos.x, pos.y-100, pos.z);
					//setVobRotation(camera, 0, camera_angle, 0);
					setPlayerAngle(heroId, camera_angle);
				break;
				case 1:
					camera_angle+=30;
					//print(camera_angle);
					local pos = getPlayerPosition(heroId)
					//local range = 100;
					//local x = pos.x + (range - pos.x) * cos(camera_angle) - (range - pos.z) * sin(camera_angle);
					//local z = pos.z + (range - pos.z) * cos(camera_angle) + (range - pos.x) * sin(camera_angle);
					//local dx = x - pos.x;
					//local dz = z - pos.z;
					//local angle = atan(dx / dz) * 180 / 3.14;
					//setVobPosition(camera, pos.x, pos.y-100, pos.z);
					//setVobRotation(camera, 0, camera_angle, 0);
					setPlayerAngle(heroId, camera_angle);
				break;
				case 2:
					if(o_SE.bModelId >=0 && o_SE.bTexture >=0  && o_SE.hModelId >=0  && o_SE.hTexture >=0)
					{
						o_SE.saveVision();
						disableSkinEdit();
					}
					else
					{
						if(!o_SE.bModel)
						{
							o_SE.bModelId = skinedit_menu.slider[0].getValue(x);
							o_SE.setModels();
						}
						if(!o_SE.bTexture)
						{
							o_SE.bTexture = skinedit_menu.slider[1].getValue(x);
							o_SE.setModels();
						}
						if(!o_SE.hModel)
						{
							o_SE.hModelId = skinedit_menu.slider[2].getValue(x);
							o_SE.setModels();
						}
						if(!o_SE.hTexture)
						{
							o_SE.hTexture = skinedit_menu.slider[3].getValue(x);
							o_SE.setModels();
						}
						o_SE.saveVision();
						disableSkinEdit();
					}
				break;
			}
        }
	}
});

addEventHandler("onMouseRelease", function(button)
{
	if(button == 0 && skinedit_menu.active)
		skinedit_menu.active_slider = -1;
});
