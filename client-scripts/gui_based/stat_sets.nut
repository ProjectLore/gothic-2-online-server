addEventHandler("onInit", function()
{
	print("ORS: Characteristics incrustration");
});

local labels = ["��������", "����", "������������", "����", "����������", "���������", "���", "�������"];
class SoulSets
{
	remain_dex = null;
	remain_str = null;
	remain_health = null;
	remain_mana = null;
	remain_oh = null;
	remain_th = null;
	remain_bow = null;
	remain_xbow = null;
	chosen_field = null;
	constructor()
	{
		remain_dex = 0;
		remain_str = 0;
		remain_health = 0;
		remain_mana = 0;
		remain_oh = 0;
		remain_th = 0;
		remain_bow = 0;
		remain_xbow = 0;
		chosen_field = 0;
		
	}
	
	function reset()
	{
		remain_dex = 0;
		remain_str = 0;
		remain_health = 0;
		remain_mana = 0;
		remain_oh = 0;
		remain_th = 0;
		remain_bow = 0;
		remain_xbow = 0;
		chosen_field = 0;	
	}
	
	
}

souldata <- SoulSets();
soulmenu <- GUI_Menu(true, false, 0, 0, 8192.0, 8192.0);

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////Soulset////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

local soul_draw;
addEventHandler("onInit",function()
{
	createSoulset();
	//soul_draw = drawCreate("� ��� ���� ��������������� ���� �������������! ���-��: ", FONT_OLD_LOWER, 50, 7600, 0, 255, 255);
});

function createSoulset()
{
	soulmenu.createLabel(2000, 1000, 0, 200, 200, "��������� �������������", FONT_OLD_UPPER);
	soulmenu.createLabel(1000, 1500, 100, 100, 100, "���� �������������: ", FONT_OLD_UPPER);
	soulmenu.createLabel(1200, 2000, 100, 100, 100, " ", FONT_OLD_UPPER);
	soulmenu.createLabel(3200, 2000, 0, 255, 255, "�������� : ", FONT_OLD_UPPER);
	soulmenu.createLabel(5200, 2000, 100, 100, 100, "���� : ", FONT_OLD_UPPER);
    soulmenu.createSlider(3000, 2500, 2000, 0, 7);
    soulmenu.createButton(3000, 2800, 1000, 500, "-");
    soulmenu.createButton(4000, 2800, 1000, 500, "+");
    soulmenu.createButton(3000, 3800, 2000, 500, "���������");
        
}




addEventHandler("onRender", function()
{
	/*if(user.stats_souls >= 2 && !user.onGUI && user.autorized)
	{
		drawSetText(soul_draw, format("� ��� ���� ��������������� ���� �������������! ���-��: %d", user.stats_souls));
		drawSetVisible(soul_draw, true);
	}
	else
	{
		drawSetVisible(soul_draw, false);	
	}*/
	
	if (soulmenu.active)
	{
		local field_1;
		local field_2;
		local field_3;
		local cur = getCursorPosition();
		gui_data.button_id = soulmenu.button_collide();
		if (soulmenu.active_slider == 0)
		{
			souldata.chosen_field = soulmenu.slider[soulmenu.active_slider].getValue(cur.x);
		}
		
		switch (souldata.chosen_field)
		{
			case 0:
				field_2 = user.stats_dex + souldata.remain_dex;
				field_3 = user.stats_str + souldata.remain_str;
			break;
			case 1:
				field_1 = user.stats_dex + souldata.remain_dex;
				field_2 = user.stats_str + souldata.remain_str;
				field_3 = user.stats_health + souldata.remain_health;
			break;
			case 2:
				field_1 = user.stats_str + souldata.remain_str;
				field_2 = user.stats_health + souldata.remain_health;
				field_3 = user.stats_mana + souldata.remain_mana;
			break;
			case 3:
				field_1 = user.stats_health + souldata.remain_health;
				field_2 = user.stats_mana + souldata.remain_mana;
				field_3 = user.stats_oh + souldata.remain_oh;
			break;
			case 4:
				field_1 = user.stats_mana + souldata.remain_mana;
				field_2 = user.stats_oh + souldata.remain_oh;
				field_3 = user.stats_th + souldata.remain_th;
			break;
			case 5:
				field_1 = user.stats_oh + souldata.remain_oh;
				field_2 = user.stats_th + souldata.remain_th;
				field_3 = user.stats_bow + souldata.remain_bow;
			break;
			case 6:
				field_1 = user.stats_th + souldata.remain_th;
				field_2 = user.stats_bow + souldata.remain_bow;
				field_3 = user.stats_xbow + souldata.remain_xbow;
			break;
			case 7:
				field_1 = user.stats_bow + souldata.remain_bow;
				field_2 = user.stats_xbow + souldata.remain_xbow;
			break;
		}
		if(souldata.chosen_field == 0)
			soulmenu.label_field[2].update("");
		else
			soulmenu.label_field[2].update(format("%s : %d",labels[souldata.chosen_field-1], field_1));
		
		if(souldata.chosen_field == 7)
			soulmenu.label_field[4].update("");
		else
			soulmenu.label_field[4].update(format("%s : %d",labels[souldata.chosen_field+1], field_3));
		
		soulmenu.label_field[3].update(format("%s : %d",labels[souldata.chosen_field], field_2));
		soulmenu.label_field[1].update(format("����� ������������� (LP) : %d", user.stats_souls));
		
		if(user.stats_souls >= 2)
			soulmenu.label_field[1].update_color(0,255,255);
		else 
			soulmenu.label_field[1].update_color(100,100,100);
	}
	
});

addEventHandler("onMouseClick",function(button)
{
	if(button == 0)
	{
		if(soulmenu.active == true)
        {     
            local slider_id = soulmenu.slider_collide();
			local button_id = soulmenu.button_collide();
			if (button_id == 1)
				switch(souldata.chosen_field)
				{
					case 0:
						if(user.stats_souls >= 2)
							{
								user.stats_souls -= 2;;
								souldata.remain_dex = souldata.remain_dex + 1;
							}
					break;
					case 1:
						if(user.stats_souls >= 2)
							{
								user.stats_souls -= 2;;
								souldata.remain_str = souldata.remain_str + 1;
							}
					break;
					case 2:
						if(user.stats_souls >= 2)
							{
								user.stats_souls -= 2;;
								souldata.remain_health = souldata.remain_health + 25;
							}
					break;
					case 3:
						if(user.stats_souls >= 2)
							{
								user.stats_souls -= 2;;
								souldata.remain_mana = souldata.remain_mana + 25;
							}
					break;
					case 4:
						if(user.stats_souls >= 2)
							{
								user.stats_souls -= 2;;
								souldata.remain_oh = souldata.remain_oh + 1;
							}
					break;
					case 5:
						if(user.stats_souls >= 2)
							{
								user.stats_souls -= 2;;
								souldata.remain_th = souldata.remain_th + 1;
							}
					break;
					case 6:
						if(user.stats_souls >= 2)
							{
								user.stats_souls -= 2;;
								souldata.remain_bow = souldata.remain_bow + 1;
							}
					break;
					case 7:
						if(user.stats_souls >= 2)
							{
								user.stats_souls -= 2;;
								souldata.remain_xbow = souldata.remain_xbow + 1;
							}
					break;
				}
			else if(button_id == 0)
				switch(souldata.chosen_field)
				{
					case 0:
						if(souldata.remain_dex > 0)
							{
								user.stats_souls += 2;
								souldata.remain_dex = souldata.remain_dex - 1;
							}
					break;
					case 1:
						if(souldata.remain_str > 0)
							{
								user.stats_souls += 2;
								souldata.remain_str = souldata.remain_str - 1;
							}
					break;
					case 2:
						if(souldata.remain_health > 0)
							{
								user.stats_souls += 2;
								souldata.remain_health = souldata.remain_health - 25;
							}
					break;
					case 3:
						if(souldata.remain_mana > 0)
							{
								user.stats_souls += 2;
								souldata.remain_mana = souldata.remain_mana - 25;
							}
					break;
					case 4:
						if(souldata.remain_oh > 0)
							{
								user.stats_souls += 2;
								souldata.remain_oh = souldata.remain_oh - 1;
							}
					break;
					case 5:
						if(souldata.remain_th > 0)
							{
								user.stats_souls += 2;
								souldata.remain_th = souldata.remain_th - 1;
							}
					break;
					case 6:
						if(souldata.remain_bow > 0)
							{
								user.stats_souls += 2;
								souldata.remain_bow = souldata.remain_bow - 1;
							}
					break;
					case 7:
						if(souldata.remain_xbow > 0)
							{
								user.stats_souls += 2;
								souldata.remain_xbow = souldata.remain_xbow - 1;
							}
					break;
				}
			if (button_id == 2)
				{
					user.updateStatsList(user.stats_dex+souldata.remain_dex, user.stats_str+souldata.remain_str, user.stats_mana+souldata.remain_mana, user.stats_health+souldata.remain_health, user.stats_oh+souldata.remain_oh, user.stats_th+souldata.remain_th, user.stats_bow+souldata.remain_bow, user.stats_xbow+souldata.remain_xbow, user.stats_souls);
					user.saveStats();
					user.loadStats();
					souldata.reset();
					soulmenu.hide(); 
					ShowMainMenu()
				}
		}
	}

});

addEventHandler("onMouseClick", function(button)
{
	if(button == 0 && soulmenu.active)
		soulmenu.active_slider = -1;
});

addEventHandler("onKey",function(key)
{
	if(soulmenu.active && key == KEY_ESCAPE)
		{
			soulmenu.hide(); 
			ShowMainMenu();
		}
});