/*function onPlayerCommand(cmd, params)
{
	if(!user.autorized)
	{
		if(cmd == "log" && user.password == params)
		{
			setAutorized(); 
			callServerFunc("sendMessageToAll", 0, 255, 0, getPlayerName(heroId)+" succesfuly autorized!");
		}
		else if(user.password != params)
		{
			callServerFunc("sendMessageToAll", 0, 255, 0, "Wrong password!");		
		}
	}
	else if(user.autorized)
		switch (cmd)
		{
			//Animations
			case "stand":
				local id = sscanf("d",params);
				switch(id[0])
				{
					case 0:
						playAni("S_HGUARD");
					break;
					case 1:
						playAni("S_LGUARD");
					break;
				}
			break;
			case "pmagic":
				local id = sscanf("d",params);
				switch(id[0])
				{
					case 0:
						playAni("T_PRACTICEMAGIC");
					break;
					case 1:
						playAni("T_PRACTICEMAGIC2");
					break;
					case 2:
						playAni("T_PRACTICEMAGIC3");
					break;
					case 3:
						playAni("T_PRACTICEMAGIC4");
					break;
					case 4:
						playAni("T_PRACTICEMAGIC5");
					break;
				}
			break;
			case "dance":
				local id = sscanf("d",params);
				switch(id[0])
				{
					case 0:
						playAni("T_DANCE_01");
					break;
					case 1:
						playAni("T_DANCE_02");
					break;
					case 2:
						playAni("T_DANCE_03");
					break;
					case 3:
						playAni("T_DANCE_04");
					break;
					case 4:
						playAni("T_DANCE_05");
					break;
					case 5:
						playAni("T_DANCE_06");
					break;
					case 6:
						playAni("T_DANCE_07");
					break;
					case 7:
						playAni("T_DANCE_08");
					break;
					case 8:
						playAni("T_DANCE_09");
					break;
				}
			break;
			case "legshake":
				playAni("R_LEGSHAKE");
			break;
			case "scratchh":
				playAni("R_SCRATCHHEAD");
			break;
			case "scratche":
				playAni("R_SCRATCHEGG");
			break;
			case "scratchls":
				playAni("R_SCRATCHLSHOULDER");
			break;
			case "scratchrs":
				playAni("R_SCRATCHRSHOULDER");
			break;
			case "yes":
				playAni("T_YES");
			break;
			case "no":
				playAni("T_NO");
			break;
			case "dunno":
				playAni("T_DONTKNOW");
			break;
			case "greetg":
				playAni("T_GREETGRD");
			break;
			case "smoke":
				playAni("t_JOINT_Random_1");
			break;
			case "playdead":
				playAni("T_DEAD");
			break;
			case "pray":
				playAni("S_PRAY");
			break;
			case "sit":
				playAni("S_SIT");
			break;
			
			//Def
			case "d":
			case "drop":
			case "д":
			case "дроп":
			local par;
				if(par = sscanf("dd", params))
					user.dropItemBySlot(par[0], par[1]);
				else if(par = sscanf("sd", params))
					user.dropItemByName(par[0], par[1]);
			break;
			case "skin":
			case "скин":
			case "внешность":
				callClientFunc(pid, "enableSkinEdit");
			break;
		}
	if(user.autorized && (user.status == "Admin" ||  user.status == "Technical Admin" || user.status == "Game Master"))
	switch(cmd)
	{
			case "exp":
				local souls = sscanf("d", params);
				user.stats_souls = souls[0];
				user.saveStats();
				break;
			case "dex":
				local dex = sscanf("d", params);
				user.setDextery(dex[0]);
				break;
			case "str":
				local str = sscanf("d", params);
				user.setStr(str[0]);
				break;
			case "maxmana":
				local mana = sscanf("d", params);
				user.setMMana(mana[0]);
				break;
			case "maxhealth":
				local health = sscanf("d", params);
				user.setMHealth(health[0]);
				break;
			case "oh":
				local oh = sscanf("d", params);
				user.setMastery1H(oh[0]);
				break;
			case "th":
				local th = sscanf("d", params);
				user.setMastery2H(th[0]);
				break;
			case "bow":
				local bow = sscanf("d", params);
				user.setMasteryBow(bow[0]);
				break;
			case "xbow":
				local xbow = sscanf("d", params);
				user.setMasteryXbow(xbow[0]);
				break;
			case "magic":
				local magic = sscanf("d", params);
				setMagicLevel(magic[0]);
				break;
		case "g":
		case "г":
			local txt = sscanf("s", params);
			callServerFunc("spreadGlobalMessage", txt[0], "fade");
		break;
		case "gs":
		case "гс":
			local txt = sscanf("s", params);
			callServerFunc("spreadGlobalMessage", txt[0], "stroke");
		break;
		case "fly":
		case "вполет":
			local id = sscanf("d", params);
			local pos = getPlayerPosition(heroId)
			callServerFunc("callClientFunc", id[0], "setPosition",  pos.x,  pos.y+2000,  pos.z);
		break;
		case "kill":
		case "убить":
			local id = sscanf("d", params);
			callServerFunc("callClientFunc", id[0], "setHealth", 0);
		break;
		case "give":
			local item = sscanf("dsd", params);
			callServerFunc("callClientFunc", item[0], "giveItem", item[1], item[2]);
		break;
		case "ban":
		case "бан":
			local id = sscanf("d", params);
			callServerFunc("ban", id[0]);
		break;
		case "spectate":
		case "спектатор":
			local id = sscanf("d", params);
			Spectate(id[0]);
		break;					
		case "cname":	
		case "симя":
			local id_name = sscanf("ds", params);
			if(id_name)
				callServerFunc("ChangeName", id_name[0], id_name[1]);
		break;
		case "marvin":
		case "марвин":
			local par = sscanf("d", params)
			if(par)
				if(par[0] == 0)
				{
					AddLocalAM("Марвин: Выкл");
					enableMarvin(false);
				}
				else
				{
					AddLocalAM("Марвин: Вкл");
					enableMarvin(true);
				}
		break;
	}
	
	if(user.autorized && (user.status == "Admin" ||  user.status == "Technical Admin" || user.status == "Game Master" || user.status == "Moderator"))
	switch(cmd)
	{			
		case "goh":
		case "сюды":
			local id = sscanf("d", params);
			local pos = getPlayerPosition(heroId)
			callServerFunc("callClientFunc", id[0], "setPosition",  pos.x,  pos.y,  pos.z);
		break;
		case "news":
		case "новости":
			local txt = sscanf("s", params);
			if(txt)
				callServerFunc("SendNews", getID(), txt[0]);
		break;
		case "ado":
		case "адо":
			local txt = sscanf("s", params);
			if(txt)
				callServerFunc("SendAdminTHPMessage", getID(), txt[0]);
		break;
		case "invis":
		case "инвиз":
			local par = sscanf("d", params)
			if(par)
				if(par[0] == 0)
				{
					AddLocalAM("Инвиз: Выкл");
					//callServerFunc("setPlayerInvisible", getID(), false);
				}
				else
				{
					AddLocalAM("Инвиз: Вкл");
					//callServerFunc("setPlayerInvisible", getID(), true);
				}
		break;
		case "addname":
		case "допимя":
			local id_name = sscanf("ds", params);
			if(id_name)
				callServerFunc("ChangeAdditionalName", id_name[0], id_name[1])
		break;
		case "kick":
		case "кик":
			local id = sscanf("d", params);
			callServerFunc("kick", id[0]);
		break;		
		case "instance":
		case "облик":
			ShowInstanceMenu();
		break;	
		case "freeze":
		case "фриз":
			local id = sscanf("d", params);
			callServerFunc("callClientFunc", id[0], "Freeze", true);
			callServerFunc("callClientFunc", id[0], "AddLocalAM", "Холодно?");
		break;	
		case "antifreeze":
		case "антифриз":
			local id = sscanf("d", params);
			callServerFunc("callClientFunc", id[0], "Freeze", false);
			callServerFunc("callClientFunc", id[0], "AddLocalAM", "Вот тебе антифриз. Держи");
		break;			
	}
}*/
