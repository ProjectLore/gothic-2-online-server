addEventHandler("onInit",  function()
{
	mixer <- StereoMixer();
	//sound_stream <- BASS_StreamCreateFile("VexillumFiles/battle.mp3", 0, 0);	
	//sound_collide <- BASS_StreamCreateFile("VexillumFiles/collide.wav", 0, 0);	
	//sound_click <- BASS_StreamCreateFile("VexillumFiles/click.wav", 0, 0);	
});

class StereoMixer
{
	constructor()
	{
		sfx = [];
		music = [];
		
		music.append(Sound("battle", "VexillumFiles/battle.mp3"));
		
		sfx.append(Sound("collide", "VexillumFiles/collide.wav"));
		sfx.append(Sound("click", "VexillumFiles/click.wav"));
	}
	
	sfx = null;
	music = null;
}

class Sound
{
	constructor(st, sd)
	{
		s_Type = st;
		s_Directory = sd;
		
		//s_Stream = BASS_StreamCreateFile(s_Directory, 0, 0);
	}
	
	function getStream()
	{
		return s_Stream;
	}
	
	function changeDirectory(sd)
	{
		//BASS_StreamFree(s_Stream);
		s_Directory = sd;
		//s_Stream = BASS_StreamCreateFile(s_Directory, 0, 0);
	}
	
	function Play()
	{
		//BASS_StreamFree(s_Stream);
		//s_Stream = BASS_StreamCreateFile(s_Directory, 0, 0);
		//BASS_ChannelPlay(s_Stream, false);
	}
	
	function Stop()
	{
		//BASS_ChannelStop(s_Stream, false);	
	}
	
	s_Directory = null
	s_Type = null;
	s_Stream = null;
}

function setMusicVolume(volume)
{
	//foreach(music in mixer.music)
		//BASS_SetVolume(music.getStream(), volume);
		
}

function setSfxVolume(volume)
{

	//BASS_SetVolume(volume);
	
	
	//foreach(sfx in mixer.sfx)
		//BASS_SetVolume(sfx.getStream(), volume);
		
	//Alpha
	
	//foreach(music in mixer.music)
		//BASS_SetVolume(music.getStream(), volume);
}

function BattleSongPlay()
{
	//foreach(music in mixer.music)
		//if(music.s_Type == "battle")
			//music.Play();
}

function BattleSongPause()
{
	//foreach(music in mixer.music)
		//if(music.s_Type == "battle")
			//music.Stop();
}

function SwitchBattleSong(id)
{
	switch(id)
	{
		case 0:
			foreach(music in mixer.music)
				if(music.s_Type == "battle")
					music.changeDirectory("VexillumFiles/battle.mp3");	
		break;
		case 1:
			foreach(music in mixer.music)
				if(music.s_Type == "battle")
					music.changeDirectory("VexillumFiles/battle2.mp3");	
		break;
		case 2:
			foreach(music in mixer.music)
				if(music.s_Type == "battle")
					music.changeDirectory("VexillumFiles/battle3.mp3");	
		break;
	
	}
}

function GUI_Collide_Sound()
{
	//foreach(sfx in mixer.sfx)
		//if(sfx.s_Type == "collide")
			//sfx.Play();
}

function GUI_Click_Sound()
{
	//foreach(sfx in mixer.sfx)
		//if(sfx.s_Type == "click")
			//sfx.Play();
}