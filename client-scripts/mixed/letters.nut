class LetterList
{
	constructor()
	{
		list = [];
		current = 0;
		to_give = -1;
	}
	
	function add(p1, p2, p3, p4)
	{
		list.append(Letter(p1, p2, p3, p4));
	}
	
	function remove(index)
	{
		list.remove(index);
		callServerFunc("ClearPlayerLetters", user.character);
		foreach(letter in letters.list)
			callServerFunc("AddPlayerLetter", user.character, letter.text, letter.sealed, letter.seal_removed, letter.author);
	}
	
	function nextLetter()
	{
		if(current + 1 <= list.len() - 1)
			current++;
		else
		{
			current++;
			return false;
		}		
		
		return true;
	}
	
	function prevLetter()
	{
		if(current != 0)
			current--
		else
			return false;
			
		return true;
	}
	
	function give(id)
	{
		callServerFunc("callClientFunc", id, "ReceiveLetter", list[to_give].text, list[to_give].sealed, list[to_give].seal_removed, list[to_give].author)
		remove(to_give);
	}
	
	function choseLetter(index)
	{
		if(index <= list.len() - 1)
			current = index;
	}
	
	list = null;	
	current = null;
	to_give = null;
}

class Letter
{
	constructor(p1, p2, p3, p4)
	{
		text = p1;
		sealed = p2;
		seal_removed = p3;
		author = p4;
	}
	
	text = null;
	sealed = null;
	seal_removed = null;
	author = null;
}

letters_menu <- GUI_Menu(true, true, 0, 0, 8192.0, 8192.0);
letters <- LetterList();

addEventHandler("onInit", function()
{
	print("ORS: Letters setup");
	createLettersMenu();
});

function AddLetter(text, sealed, seal_removed, author)
{
	letters.add(text, sealed, seal_removed, author);
}

function ReceiveLetter(text, sealed, seal_removed, author)
{
	letters.add(text, sealed, seal_removed, author);
	callServerFunc("ClearPlayerLetters", user.character);
	foreach(letter in letters.list)
		callServerFunc("AddPlayerLetter", user.character, letter.text, letter.sealed, letter.seal_removed, letter.author);
		
}

function createLettersMenu()
{
	letters_menu.createLabel(2000, 1000, 160, 2, 2, "�������� �����", FONT_OLD_UPPER);
	
	
	letters_menu.createChoiceGroup();
	letters_menu.choice_group[0].addChoice(4592, 4592, 1000, 500, "�������");
	letters_menu.choice_group[0].addChoice(5692, 4592, 1000, 500, "��������");
	letters_menu.choice_group[0].setActive(1);
	letters_menu.createChoiceGroup();
	letters_menu.choice_group[1].addChoice(4592, 5592, 1000, 500, "������");
	letters_menu.choice_group[1].addChoice(5692, 5592, 1000, 500, "�������");
	letters_menu.choice_group[1].setActive(1);
	
	letters_menu.createPic(4096, 100, 3996, 7092, "LETTER1.TGA");
	letters_menu.createPic(4096, 2000, 4000, 4000, "SEALED_SCROLL.TGA");
	letters_menu.createPic(6692, 6192, 600, 600, "WAX_SEAL.TGA");
	
    letters_menu.createLabel(6192, 5992, 0, 0, 0, "", FONT_OLD_LOWER);	
    letters_menu.createEditBlock("",4496, 600, 3296, 4400, FONT_OLD_LOWER, true);
    letters_menu.createEditBlock("",4496, 600, 3296, 4400, FONT_OLD_LOWER, false);
    letters_menu.createButton(2000, 2000, 1000, 500, "����� ������");
    letters_menu.createButton(2000, 3000, 1000, 500, "��������");
    letters_menu.createButton(2000, 4000, 1000, 500, "�������");
    letters_menu.createButton(2000, 2000, 1000, 500, "���������"); 
    letters_menu.createButton(2000, 6000, 1000, 500, "����������"); 
    letters_menu.createButton(2000, 7000, 1000, 500, "���������"); 
}

function ShowLetterMenu()
{
	letters_menu.show();
	UpdateLetterMenu();
	callServerFunc("SendFPMessage", getID(), "������ � �������")
}

function UpdateLetterMenu()
{
	if(letters.current > letters.list.len() - 1)
	{
		letters_menu.button[5].hide();
		if(letters.current != 0)
			letters_menu.button[4].show();
		else
			letters_menu.button[4].hide();
		ShowNewLetter();
	}
	else
	{
		letters_menu.button[5].show();
		if(letters.current != 0)
			letters_menu.button[4].show();
		else
			letters_menu.button[4].hide();
		ShowExistLetter();
	}
}

function ShowNewLetter()
{
	letters_menu.button[0].hide();
	letters_menu.button[1].hide();
	letters_menu.button[2].hide();
	letters_menu.button[3].show();
		
	letters_menu.pic[0].show();
	letters_menu.pic[1].hide();
	letters_menu.pic[2].hide();

	letters_menu.label_field[1].hide();
	letters_menu.edit_block[0].show();
	letters_menu.edit_block[1].hide();
		
	letters_menu.choice_group[0].show();
	letters_menu.choice_group[1].show();
}

function ShowExistLetter()
{
	letters_menu.edit_block[1].setText(letters.list[letters.current].text);
	letters_menu.label_field[1].update(letters.list[letters.current].author);
	
	if(letters.list[letters.current].sealed && !letters.list[letters.current].seal_removed)
	{
		letters_menu.button[0].show();
		letters_menu.button[1].show();
		letters_menu.button[2].show();
		letters_menu.button[3].hide();
		
		letters_menu.pic[0].hide();
		letters_menu.pic[1].show();
		letters_menu.pic[2].hide();
		
		letters_menu.label_field[1].hide();
		letters_menu.edit_block[0].hide();
		letters_menu.edit_block[1].hide();
		
		letters_menu.choice_group[0].hide();
		letters_menu.choice_group[1].hide();
	}
	else if(letters.list[letters.current].sealed && letters.list[letters.current].seal_removed)
	{
		letters_menu.button[0].hide();
		letters_menu.button[1].show();
		letters_menu.button[2].show();
		letters_menu.button[3].hide();
		
		letters_menu.pic[0].show();
		letters_menu.pic[1].hide();
		letters_menu.pic[2].show();
		
		letters_menu.label_field[1].show();
		letters_menu.edit_block[0].hide();
		letters_menu.edit_block[1].show();
		
		letters_menu.choice_group[0].hide();
		letters_menu.choice_group[1].hide();	
	}
	else if(!letters.list[letters.current].sealed)
	{
		letters_menu.button[0].hide();
		letters_menu.button[1].show();
		letters_menu.button[2].show();
		letters_menu.button[3].hide();
		
		letters_menu.pic[0].show();
		letters_menu.pic[1].hide();
		letters_menu.pic[2].hide();
		
		letters_menu.label_field[1].show();
		letters_menu.edit_block[0].hide();
		letters_menu.edit_block[1].show();
		
		letters_menu.choice_group[0].hide();
		letters_menu.choice_group[1].hide();	
	}
}

addEventHandler("onRender",function()
{
    if(letters_menu.active == true)
        {
			gui_data.button_id = letters_menu.button_collide(); 
			letters_menu.choice_collide();
        }
});


addEventHandler("onMouseClick", function(button)
{
	if(button == 0)
		if(letters_menu.active)
                {   
					local slider_id = letters_menu.slider_collide();
                    
					local button_id = letters_menu.button_collide();
					switch(button_id)
					{
						case 0:
							callServerFunc("SendFPMessage", getID(), "������ ������ � ������")
							letters.list[letters.current].seal_removed = true;
							UpdateLetterMenu();
							callServerFunc("ClearPlayerLetters", user.character);
							foreach(letter in letters.list)
								callServerFunc("AddPlayerLetter", user.character, letter.text, letter.sealed, letter.seal_removed, letter.author);
						break;
						case 1:
							callServerFunc("SendFPMessage", getID(), "���� ������ � ����")
							setPanelText3("N - �������� ������ (�������� - ��� ������)");
							OpenPanel();
							letters.to_give = letters.current;
							letters_menu.hide();
						break;
						case 2:
							callServerFunc("SendFPMessage", getID(), "�������� ������ � ������")
							letters.remove(letters.current);
							UpdateLetterMenu();
						break;
						case 3:
							callServerFunc("SendFPMessage", getID(), "���������� ���-�� �� ����� ����������")
							local sealed;
							local author;
							switch(letters_menu.choice_group[0].getActive())
							{
								case 0:
									author = user.character;
								break;
								case 1:
									author = "";
								break;
							}
							
							switch(letters_menu.choice_group[1].getActive())
							{
								case 0:
									sealed = true;
								break;
								case 1:
									sealed = false;
								break;
							}
							
							ReceiveLetter(letters_menu.edit_block[0].text, sealed, false, author);
							letters_menu.edit_block[0].setText("");
							UpdateLetterMenu();
						break;
						case 4:
							letters.prevLetter();
							UpdateLetterMenu();
						break;
						case 5:
							letters.nextLetter();
							UpdateLetterMenu();
						break;						
					}
						
					local choice_id = letters_menu.choice_group[0].collide();
					if(choice_id >= 0)
						letters_menu.choice_group[0].setActive(choice_id);
						
					local choice_id = letters_menu.choice_group[1].collide();
					if(choice_id >= 0)
						letters_menu.choice_group[1].setActive(choice_id);
						
                    local edblock_id = -1;
					edblock_id = letters_menu.eblock_collide();
                    if(edblock_id >= 0)
                        letters_menu.active_edblock = edblock_id;
				}
});


addEventHandler("onMouseClick", function(button)
{
	
});

addEventHandler("onKey",function(key)
{
        switch(key)
        {
                case KEY_ESCAPE:
                        if(letters_menu.active && letters_menu.active_edblock == -1)
						{
							letters_menu.hide();
						}  
						if(letters.to_give != -1)
						{
							letters.to_give = -1;
							setPanelText3("Official Russian Server RP");
						}        
                break;
                case KEY_L:
                        if(!user.onGUI && !chatInputIsOpen())
						{
							ShowLetterMenu();
						}            
                break;
                case KEY_N:
                        if(getFocusID() != -1 && letters.to_give != -1 && !chatInputIsOpen() && !user.onGUI)
						{
							letters.give(getFocusID());
							setPanelText3("Official Russian Server RP");
							callServerFunc("SendFPMessage", getID(), "������� ������ "+ getFocusName() +"'�")
						}
						else if(letters.to_give != -1 && !chatInputIsOpen() && !user.onGUI)
						{
							letters.to_give = -1;
							setPanelText3("Official Russian Server RP");
						}
                break;
        }
		
        if (letters_menu.active == true && letters_menu.active_edblock >= 0)
                        if(key == KEY_BACK)
                                letters_menu.edit_block[letters_menu.active_edblock].deleteLitera();
                        else if (key == KEY_ESCAPE || key == KEY_RETURN)
                                letters_menu.active_edblock = -1;
                        else
                                letters_menu.edit_block[letters_menu.active_edblock].addLitera(getKeyLetter(key));
});