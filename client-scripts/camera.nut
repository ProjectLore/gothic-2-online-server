addEventHandler("onInit",  function()
{
	
});

local spectated_id = -1;

class Camera
{
	
	constructor()
	{
		position =
		{
			x = 0
			y = 0
			z = 0
		};
		
		rotation = 
		{
			x = 0
			y = 0
			z = 0
		};
		
		//objectCamera = createVob("", position.x, position.y, position.z, rotation.x, rotation.y, rotation.z);
		
		render = 0;
		
		active = false;
	}
	
	
	function setPosition(x, y, z)
	{
		position.x = x;
		position.y = y;
		position.z = z;
		
		setVobPosition(objectCamera, position.x, position.y, position.z);
	}
	
	function setRotation(x, y, z)
	{
		rotation.x = x;
		rotation.y = y;
		rotation.z = z;
		
		setVobRotation(objectCamera, rotation.x, rotation.y, rotation.z);
		
	}
	
	function setActive(toggle)
	{
		if(toggle)
			{
				setCameraBehindVob(objectCamera);
				setFreeze(true);
				active = true;
			}
		else 
			{
				setDefaultCamera();
				if(!user.freezed)
					setFreeze(false);
				active = false;
			}
	}
	
	function setAction(func)
	{
		render = setTimer(func, 50, 0);
	}
	
	function clearAction()
	{
		killTimer(render);
	}
	
	objectCamera = null;
	position = null;
	rotation = null;
	render = null;
	active = null;
}

local actionCamera = Camera();

function overlook()
{
	local pos = getPlayerPosition(heroId)
	local cpos = getCursorPosition();
	actionCamera.setPosition(pos.x, pos.y, pos.z);
	
	if(cpos.x <= 5)
		setCursorPosition(8186, cpos.y);
	else if(cpos.x >=8187)
		setCursorPosition(6, cpos.y);
		
	actionCamera.setRotation(0, getAngle()-(cpos.x/(8192.0/360))-180, 0);
}

function spectate_camera()
{
	local pos = getPlayerPosition(spectated_id);
	local cpos = getCursorPosition();
	actionCamera.setPosition(pos.x, pos.y, pos.z);
	
	if(cpos.x <= 5)
		setCursorPosition(8186, cpos.y);
	else if(cpos.x >=8187)
		setCursorPosition(6, cpos.y);
	
	actionCamera.setRotation(0, getPlayerAngle(spectated_id)-(cpos.x/(8192.0/360))-180, 0);
}

function Spectate(pid)
{
	
	actionCamera.clearAction();
	
	setFreeze(true);
	spectated_id = pid;
	setCursorPosition(8192.0/2, 8192.0/2);
	setCursorTxt("NONE.TGA");
	setCursorVisible(true);
	
	actionCamera.setActive(true);
	actionCamera.setAction(spectate_camera);
	
	local spec_name = format("Spectating %s",getPlayerName(pid));
	setPanelText3(spec_name);
	OpenPanel();
	
}


function DisableCamera()
{
	actionCamera.setActive(false);
	actionCamera.clearAction();
	setCursorVisible(false);
	
	
	setPanelText3("Official Russian Server RP");
	ClosePanel();
	
	if(!user.freezed)
		setFreeze(false);
}


addEventHandler("onMouseClick",function(button)
{
	
	/*if(button == "RIGHT_DOWN" && !actionCamera.active && !user.onGUI)
	{
		actionCamera.setActive(true);
		actionCamera.setAction(overlook);
		
	}
	else if(button == "RIGHT_DOWN" && actionCamera.active && !user.onGUI)
	{
		actionCamera.setActive(false);
		actionCamera.clearAction();
		setCursorVisible(false);
	}*/

});

	
addEventHandler("onKey",function(key)
{
	if(key == 525 && !actionCamera.active && (!user.onGUI || freecam.active))
	{
		setCursorPosition(8192.0/2, 8192.0/2);
		setCursorTxt("NONE.TGA");
		setCursorVisible(true);
		
		
		actionCamera.setActive(true);
		actionCamera.setAction(overlook);
	}
	else if((key == 525 || key == KEY_X) && !user.onGUI && actionCamera.active)
	{
		DisableCamera();
	}
});