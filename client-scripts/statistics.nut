class MultiplayerStatistics
{
	constructor()
	{
		local netStats = getNetworkStats();
		
		m_DrawTitle = drawCreate("Multiplayer debug", "FONT_OLD_20_WHITE_HI.TGA", 50, 50, 255, 255, 255);
		m_DrawPing = drawCreate(format("Ping: %i ms", getPing()), "FONT_OLD_10_WHITE_HI.TGA", 50, 400, 255, 255, 255);
		m_DrawFPS = drawCreate(format("FPS: %i", getFPSRate()), "FONT_OLD_10_WHITE_HI.TGA", 50, 600, 255, 255, 255);
		m_DrawReceivecPackets = drawCreate(format("Received packet count: %i", netStats.packetReceived), "FONT_OLD_10_WHITE_HI.TGA", 50, 800, 255, 255, 255);
		m_DrawLostPackets = drawCreate(format("Lost packet count: %i", netStats.packetlossTotal), "FONT_OLD_10_WHITE_HI.TGA", 50, 1000, 255, 255, 255);
		m_DrawLostPacketsLastSec = drawCreate(format("Lost packet in last second count: %i", netStats.packetlossLastSecond), "FONT_OLD_10_WHITE_HI.TGA", 50, 1200, 255, 255, 255);
		m_DrawMessageBuffer = drawCreate(format("Message buffer: %i", netStats.messagesInResendBuffer), "FONT_OLD_10_WHITE_HI.TGA", 50, 1400, 255, 255, 255);
		m_DrawBufferBytesToSend = drawCreate(format("Buffer bytes to send: %i", netStats.bytesInResendBuffer), "FONT_OLD_10_WHITE_HI.TGA", 50, 1600, 255, 255, 255);
		m_DrawStreamedPlayers = drawCreate(format("Streamed players: %i", getStreamedPlayers()), "FONT_OLD_10_WHITE_HI.TGA", 50, 1800, 255, 255, 255);
		m_DrawStreamedItems = drawCreate(format("Streamend items: %i", getStreamedItems()), "FONT_OLD_10_WHITE_HI.TGA", 50, 2000, 255, 255, 255);
		
		setTimer(function(stats)
		{
			local netStats = getNetworkStats();
		
			drawSetText(stats.m_DrawPing, format("Ping: %i ms", getPing()));
			drawSetText(stats.m_DrawFPS, format("FPS: %i", getFPSRate()));
			drawSetText(stats.m_DrawReceivecPackets, format("Received packet count: %i", netStats.packetReceived));
			drawSetText(stats.m_DrawLostPackets, format("Lost packet count: %i", netStats.packetlossTotal));
			drawSetText(stats.m_DrawLostPacketsLastSec, format("Lost packet in last second count: %i", netStats.packetlossLastSecond));
			drawSetText(stats.m_DrawMessageBuffer, format("Message buffer: %i", netStats.messagesInResendBuffer));
			drawSetText(stats.m_DrawBufferBytesToSend, format("Buffer bytes to send: %i", netStats.bytesInResendBuffer));
			drawSetText(stats.m_DrawStreamedPlayers, format("Streamed players: %i", getStreamedPlayers()));
			drawSetText(stats.m_DrawStreamedItems, format("Streamend items: %i", getStreamedItems()));
		}, 200, true, this);
		
		m_bVisible = false;
	}
	
	function show()
	{
		if (!m_bVisible)
		{
			drawSetVisible(m_DrawTitle, true);
			drawSetVisible(m_DrawPing, true);
			drawSetVisible(m_DrawFPS, true);
			drawSetVisible(m_DrawReceivecPackets, true);
			drawSetVisible(m_DrawLostPackets, true);
			drawSetVisible(m_DrawLostPacketsLastSec, true);
			drawSetVisible(m_DrawMessageBuffer, true);
			drawSetVisible(m_DrawBufferBytesToSend, true);
			drawSetVisible(m_DrawStreamedPlayers, true);
			drawSetVisible(m_DrawStreamedItems, true);
			
			globalChat.hide();
			
			m_bVisible = true;
		}
	}
	
	function hide()
	{
		if (m_bVisible)
		{
			drawSetVisible(m_DrawTitle, false);
			drawSetVisible(m_DrawPing, false);
			drawSetVisible(m_DrawFPS, false);
			drawSetVisible(m_DrawReceivecPackets, false);
			drawSetVisible(m_DrawLostPackets, false);
			drawSetVisible(m_DrawLostPacketsLastSec, false);
			drawSetVisible(m_DrawMessageBuffer, false);
			drawSetVisible(m_DrawBufferBytesToSend, false);
			drawSetVisible(m_DrawStreamedPlayers, false);
			drawSetVisible(m_DrawStreamedItems, false);
			
			globalChat.show();
			
			m_bVisible = false;
		}
	}
	
	m_bVisible							= false;
	m_DrawTitle 							= null;
	m_DrawPing 							= null;
	m_DrawFPS							= null;
	m_DrawReceivecPackets 		= null;
	m_DrawLostPackets 				= null;
	m_DrawLostPacketsLastSec	= null;
	m_DrawMessageBuffer 			= null;
	m_DrawBufferBytesToSend	= null;
	m_DrawStreamedPlayers		= null;
	m_DrawStreamedItems 			= null;
};

globalMultiStats <- MultiplayerStatistics();

addEventHandler("onKey", function(key)
{
	if (key == KEY_F6)
	{
		if (globalMultiStats.m_bVisible)
			globalMultiStats.hide();
		else if (!chatInputIsOpen())
			globalMultiStats.show();
	}
});
//DLG_CONVERSATION.TGA