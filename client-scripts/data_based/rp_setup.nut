HEALTH_BAR <- 0;
FOCUS_BAR <- 1;
MANA_BAR <- 3;


addEventHandler("onInit", function()
{
	print("ORS RP setup");
	print("Interface hidden");
	enableHud(HEALTH_BAR, false);
	enableHud(FOCUS_BAR, false);
	enableHud(MANA_BAR, false);
	createPregenerationMenus();
});

function ShowCharacterListMenu()
{
	dropCharacters();
	callServerFunc("setPlayerName", heroId, user.name);
	callServerFunc("LoadPlayerCharacters", heroId);
	cc_menu.show();
}

function UpdateCChoices()
{
	for(local i = 0; i < 2; i++)
		cc_menu.choice_group[0].choices[i].setText("������ ����");
	
	for(local i = 0; i < 2 && i < user.characters.len(); i++)
		cc_menu.choice_group[0].choices[i].setText(user.characters[i]);
	CheckCharacter(cc_menu.choice_group[0].getActive());
}

function ProceedCreation()
{
	user.character = creation_menu.edit_box[0].text;
	creation_menu.hide();
	enableSkinEdit();
}

function CheckCharacter(slot)
{
	if(user.characters.len() - 1 >= slot)
	{		
		if(cc_menu.active)
		{
			cc_menu.button[0].enable(true);
			cc_menu.button[1].enable(false);
			cc_menu.button[2].enable(true);
			cc_menu.button[3].enable(true);
			
			cc_menu.edit_block[0].setText(user.description[slot]);
			
			cc_menu.pic[1].show();
			cc_menu.edit_block[0].show();
			cc_menu.label_field[2].show();
		}
		
		setPlayerCharacter(slot);
		callServerFunc("setPlayerName", heroId, user.character);
		
		user.loadVisual();
	}
	else
	{
		if(cc_menu.active)
		{
			cc_menu.button[0].enable(false);
			cc_menu.button[1].enable(true);
			cc_menu.button[2].enable(false);
			cc_menu.button[3].enable(false);	
			
			cc_menu.edit_block[0].setText("");
			
			cc_menu.pic[1].hide();
			cc_menu.edit_block[0].hide();
			cc_menu.label_field[2].hide();
		}
		
		callServerFunc("setPlayerName", heroId, user.name);
	}
}

function ShowCreationMenu()
{
	creation_menu.show();
}

function createPregenerationMenus()
{
	cc_menu <- GUI_Menu(true, true, 0, 0, 0, 0);
	creation_menu <- GUI_Menu(true, true, 0, 0, 0, 0);
	
	
	cc_menu.createLabel(300, 300, 160, 2, 2, "����� ����������, " + getPlayerName(heroId), FONT_OLD_UPPER);
	cc_menu.createPic(50, 1600, 1100, 1500, "BACKGROUND_02.TGA");
	cc_menu.createLabel(300, 1450, 249, 255, 144, "���������", FONT_OLD_LOWER);
	cc_menu.createChoiceGroup();
	cc_menu.choice_group[0].addChoice(100, 1900, 1000, 500, "������ ����");
	cc_menu.choice_group[0].addChoice(100, 2500, 1000, 500, "������ ����");
	cc_menu.choice_group[0].setActive(0);
	
	cc_menu.createPic(50, 3300, 2100, 4500, "BACKGROUND_02.TGA");
	cc_menu.createLabel(300, 3150, 249, 255, 144, "��������", FONT_OLD_LOWER);
	cc_menu.createEditBlock("", 100, 3350, 2000, 4200, FONT_OLD_LOWER, false);
	cc_menu.pic[1].hide();
	cc_menu.edit_block[0].hide();
	cc_menu.label_field[2].hide();
    
	
	cc_menu.createPic(6950, 4500, 1100, 3050, "BACKGROUND_02.TGA");
    cc_menu.createButton((8192/2)-(1000/2), 7200, 1000, 350, "� ����"); 
    cc_menu.createButton((8192/2)-(1000/2), 7650, 1000, 350, "�������"); 
	cc_menu.button[0].enable(false);
    cc_menu.createButton(7000, 4600, 1000, 350, "�������"); 
	cc_menu.button[2].enable(false);
    cc_menu.createButton(7000, 5200, 1000, 350, "���������"); 
	cc_menu.button[3].enable(false);
	
    cc_menu.createButton(7000, 5800, 1000, 350, "����������"); 
    cc_menu.createButton(7000, 6400, 1000, 350, "���������"); 
    cc_menu.createButton(7000, 7000, 1000, 350, "�����"); 
	
	creation_menu.createPic((8192/2)/2, 1000, (8192/2), 6000, "BACKGROUND_02.TGA");
	creation_menu.createLabel((8192/2)/2 + ((8192/2) - drawGetWidth(drawCreate(0, 0, 0, 0, 0, FONT_OLD_LOWER, "��� ���������")))/2, 1500, 249, 255, 144, "��� ���������", FONT_OLD_LOWER);
	creation_menu.createLabel((8192/2)/2 + ((8192/2) - drawGetWidth(drawCreate(0, 0, 0, 0, 0, FONT_OLD_LOWER, "������������ ���")))/2, 1500, 255, 100, 0, "", FONT_OLD_LOWER);
	creation_menu.createEditBox((8192/2)-(2200/2), 2000, 2200, 500, FONT_OLD_LOWER);
	creation_menu.createLabel((8192/2)/2 + ((8192/2) - drawGetWidth(drawCreate(0, 0, 0, 0, 0, FONT_OLD_LOWER, "�������� ���������")))/2, 2800, 249, 255, 144, "�������� ���������", FONT_OLD_LOWER);
    creation_menu.createEditBlock("", (8192/2)/2 + 100, 3100, (8192/2)-200, 3300, FONT_OLD_LOWER, true);
    creation_menu.createButton((8192/2)-(1000/2), 6600, 1000, 500, "�����");
	creation_menu.button[0].enable(false);
}

class uidList
{
	constructor()
	{
		_uid = [];
		_uid.resize(120);
		for(local i = 0; i < 120; i++)
			_uid[i] = -1;
	}
	
	function add(pid, uid)
	{
		_uid[pid] = uid;
	}
	
	function del(pid)
	{
		_uid[pid] = -1;
	}
	_uid = null;
}

class RPData
{
	constructor()
	{
		additional_name = "";
		class_name = "";
		class_id = 1;
		people_i_know = [];
	}
	
	function setAdditionalName(name)
	{
		additional_name = name;
	}
	
	function addKnownPersone(uid, name)
	{
		people_i_know.append(KnownNames(uid, name));
	}
	
	people_i_know = null;
	additional_name = null;
	class_name = null;
	class_id = null;
}

class KnownNames
{
	constructor(new_uid, new_name)
	{
		uid = new_uid; name = new_name;
	}
	
	uid = null;
	name = null;
}

rp_data <- RPData();
uids <- uidList();

function ReplaceUID(pid, uid)
{
	uids.add(pid, uid);
}

function RequireUIDs()
{
	callServerFunc("SendUIDs", getID());
}

function AddKnown(uid, name)
{
	rp_data.addKnownPersone(uid, name);
}

function ReceiveAdditionalName(name)
{
	rp_data.setAdditionalName(name);
	//UpdateClassName();
	LostTargetName();
}

function ReceiveClassData(c_id, c_name)
{
	rp_data.class_id = c_id;
	rp_data.class_name = c_name;
	LostTargetName();
}

function MathClassName()
{
	/*if((user.stats_dex >= 15 && user.stats_dex <= 30) && (user.stats_str >= 40 && user.stats_str <= 50) &&( user.stats_health <= 150) && (user.stats_mana < 50) && ((user.stats_oh > 30) || (user.stats_th > 30)) (user.stats_bow >= 10) && (user.stats_xbow >= 10))
		rp_data.class_name = "(����-�������)";
	else 
	if((user.stats_dex >= 25 && user.stats_dex <= 40) && (user.stats_str >= 50 && user.stats_str <= 70) &&( user.stats_health <= 250) && (user.stats_mana < 50) && ((user.stats_oh > 45) || (user.stats_th > 45)) (user.stats_bow >= 10) && (user.stats_xbow >= 10))
		rp_data.class_name = "(����)";
	else 
	if((user.stats_dex >= 35 && user.stats_dex <= 50) && (user.stats_str >= 60 && user.stats_str <= 80) &&( user.stats_health <= 400) && (user.stats_mana < 50) && ((user.stats_oh > 60) || (user.stats_th > 60)) (user.stats_bow >= 10) && (user.stats_xbow >= 10))
		rp_data.class_name = "(������� ����)";
	else
	if((user.stats_dex >= 45 && user.stats_dex <= 60) && (user.stats_str >= 80 && user.stats_str <= 90) &&( user.stats_health > 400) && (user.stats_mana < 50) && ((user.stats_oh > 70) || (user.stats_th > 70)) (user.stats_bow >= 10) && (user.stats_xbow >= 10))
		rp_data.class_name = "(������ ����)";
	*/
}

function SendAdditionalName(id)
{
	//callServerFunc("callClientFunc", id, "ReceiveTargetName", "(" + rp_data.class_name + ") " + rp_data.additional_name + " " + getPlayerName(heroId));
}

function ReceiveTargetName(name)
{
	//setPanelText2(name);
	//ClosePanel();
	//OpenPanel();
}

function LostTargetName()
{
	ClosePanel();
	OpenPanel();
	setPanelText2("(" + rp_data.class_name + ") " + rp_data.additional_name + " " + getPlayerName(heroId));
	if(status_panel.third_text != "Official Russian Server RP")
		ClosePanel();
}


addEventHandler("onTakeFocus", function(id, name)
{
	callServerFunc("callClientFunc", id, "SendAdditionalName", getID());
});

addEventHandler("onLostFocus", function(pid, name)
{
	LostTargetName();
});

addEventHandler("onPlayerCreated", function(id, ping)
{
	RequireUIDs();
});

addEventHandler("onPlayerCreated", function(id, ping)
{
	RequireUIDs();
	//setPlayerName(pid, "����������");
});

function onPlayerHit(pid)
{
	if(main_menu.active && pid == getID())
		return 1;
}

addEventHandler("onPlayerHit", onPlayerHit);

function onRenderFocus(type, id, x, y, name)
{
	if(id > -1)
		return 1;
};

addEventHandler("onRenderFocus", onRenderFocus);


addEventHandler("onMouseClick", function(button)
{
	if(button == 0)
		if(cc_menu.active)
                {   
					local button_id = cc_menu.button_collide();
					switch(button_id)
					{
						case 0:
							cc_menu.hide();
							setInGame(true);
						break;
						case 1:
							cc_menu.hide();
							ShowCreationMenu();
						break;
						case 2:
							callServerFunc("DeleteCharacter", user.characters[cc_menu.choice_group[0].getActive()]);
							dropCharacters();
						break;
						case 3:
							user.character = user.characters[cc_menu.choice_group[0].getActive()];
							cc_menu.hide();
							enableSkinEdit();
						break;
						case 4:
						
						break;
						case 5:
						
						break;
						case 6:
							Exit();
						break;
					}
						
					local choice_id = cc_menu.choice_group[0].collide();
					if(choice_id >= 0)
						cc_menu.choice_group[0].setActive(choice_id);
					
					CheckCharacter(cc_menu.choice_group[0].getActive());
				}
		else if(creation_menu.active)
                {   
                    
					local edb_id = -1;
					edb_id = creation_menu.eb_collide();
							creation_menu.active_edb = edb_id;
									
					local button_id = creation_menu.button_collide();
					switch(button_id)
					{
						case 0:
							//
							//
							//
							callServerFunc("IntegrateCharacterCheck", heroId, creation_menu.active && creation_menu.edit_box[0].text, creation_menu.active && creation_menu.edit_block[0].text)
						break;
					}
						
                    local edblock_id = -1;
					edblock_id = creation_menu.eblock_collide();
                        creation_menu.active_edblock = edblock_id;
				}
});


addEventHandler("onRender",function()
{
    if(cc_menu.active == true)
        {
			gui_data.button_id = cc_menu.button_collide(); 
			cc_menu.choice_collide();
        }
		
    if(creation_menu.active == true)
        {
			gui_data.button_id = creation_menu.button_collide();
        }
});

addEventHandler("onKey",function(key)
{
	if (creation_menu.active == true && creation_menu.active_edb >= 0)
                        if(key == KEY_BACK)
                                creation_menu.edit_box[creation_menu.active_edb].deleteLitera();
                        else if (key == KEY_ESCAPE)
                                creation_menu.active_edb = -1;					
                        else
                                creation_menu.edit_box[creation_menu.active_edb].addLitera(getKeyLetter(key));
								
    if (creation_menu.active == true && creation_menu.active_edblock >= 0)
                    if(key == KEY_BACK)
                            creation_menu.edit_block[creation_menu.active_edblock].deleteLitera();
                    else if (key == KEY_ESCAPE || key == KEY_RETURN)
                            creation_menu.active_edblock = -1;
                    else
                            creation_menu.edit_block[creation_menu.active_edblock].addLitera(getKeyLetter(key));
							
	if (creation_menu.active && creation_menu.edit_box[0].text.len() > 2 && creation_menu.edit_block[0].text.len() > 10)
		creation_menu.button[0].enable(true);
	else
		creation_menu.button[0].enable(false);
	
});