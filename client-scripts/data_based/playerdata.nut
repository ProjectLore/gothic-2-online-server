local resolution;

addEventHandler("onInit", function()
{
	resolution = getResolution();
	print(resolution.x + " " + resolution.y);
	//callConsole(true);
	//if (md5File("Data/Worlds/ARENA2.3.ZEN")){print("MALACA");}else{exitGame();}
});

class Player
{
	constructor()
	{
		message_count = -1;
		message = [];
		message_current = -1;
		id = -1;
		freezed = false;
		
		inGame = false;
		name = getPlayerName(heroId);
		character = null;
		characters = [];
		description = [];
	
	}

	function savePosition()
	{
		callServerFunc("SavePosition",  user.character);
	}
	
	function saveItems()
	{
		callServerFunc("ClearItemBase",  user.character);
		local items = getEq();
		if(getPlayerInstance(getID()) == "PC_HERO")
			foreach (item in items)
			{
				//callServerFunc("Test", item.instance +" "+ item.amount);
				if(getArmorInstance() == item.instance || getMeleeInstance() == item.instance || getRangedInstance() == item.instance)
					callServerFunc("SavePlayerItems",  user.character,  item.instance, true,  item.amount);
				else
					callServerFunc("SavePlayerItems",  user.character,  item.instance, false,  item.amount);
			}
	}
	
	function dropItemBySlot(slot, amount)
	{
		local pPos = getPlayerPosition(heroId);
		local items = getEq();
		local cslot = 1;
		if(amount > 0)
			if(getPlayerInstance(getID()) == "PC_HERO")
				foreach (item in items)
				{
					if(cslot == slot)
						if(item.amount >= amount)
						{
							removeItem(item.instance, amount);
							callServerFunc("createItem", item.instance, amount, pPos.x, pPos.y+50, pPos.z, getWorld());
						}
					cslot ++;
				}
		saveItems();
	}
	
	
	function dropItemByName(name, amount)
	{
		local pPos = getPlayerPosition(heroId)
		local items = getEq();
		if(amount > 0)
			if(getPlayerInstance(getID()) == "PC_HERO")
				foreach (item in items)
					if(getItemName(item.instance) == name && item.amount >= amount)
						{
							removeItem(item.instance, amount);
							callServerFunc("createItem", item.instance, amount, pPos.x, pPos.y+50, pPos.z, getWorld());
						}
		saveItems();
	}
	
	function loadBodyState()
	{
		callServerFunc("LoadBodyStatus", user.character);
	}
	
	function loadItems()
	{
		callServerFunc("loadPlayerItems", user.character);
	}
	
	function loadVisual()
	{
		callServerFunc("LoadVisual", user.character);
	}
	
	function setPos(x, y, z)
	{
		//setPosition(x, y, z);
	}
	
	function toggleFreeze()
	{
		if(isFrozen())
			setFreeze(false);
		else
			setFreeze(true);
	}
	
	function setDextery(dex)
	{
		setDexterity(dex);
		stats_dex = dex;	
		saveStats();
	}
	
	function setStr(str)
	{
		setStrength(str);
		stats_str = str;
		saveStats();
	
	}
	
	function setMMana(mana)
	{
		setMaxMana(mana);
		setMana(getMaxMana());
		stats_mana = mana;
		saveStats();
	
	}
	
	function setMHealth(health)
	{
		setMaxHealth(health);
		setHealth(getMaxHealth());
		stats_health = health;
		saveStats();
	
	}
	
	function setMastery1H(oh)
	{
		setWeaponSkill(1, oh);
		stats_oh = oh;
		saveStats();
	
	}
	
	function setMastery2H(th)
	{
		setWeaponSkill(2, th);
		stats_th = th;
		saveStats();
	
	}
	
	function setMasteryBow(bow)
	{
		setWeaponSkill(3, bow);
		stats_bow = bow;
		saveStats();
	
	}
	
	function setMasteryXbow(xbow)
	{
		setWeaponSkill(4, xbow);
		stats_xbow = xbow;
		saveStats();
	
	}
	
	function saveBodyStatus()
	{
		callServerFunc("SaveBodyStatus", getID());
	}
	
	function saveStats()
	{
		callServerFunc("SaveStats", user.character, 	stats_dex, 	stats_str, 	stats_mana, 	stats_health, 	stats_oh, 	stats_th, 	stats_bow, 	stats_xbow, stats_souls);
	}
	
	function loadStats()
	{
		setDexterity(stats_dex);
		setStrength(stats_str);
		setMaxMana(stats_mana);
		setMana(getMaxMana());
		setMaxHealth(stats_health);
		setHealth(getMaxHealth());
		setWeaponSkill(1, stats_oh);
		setWeaponSkill(2, stats_th);
		setWeaponSkill(3, stats_bow);
		setWeaponSkill(4, stats_xbow);
		callServerFunc("LoadStats",  user.character);
	}
	
	function loadClass()
	{
		callServerFunc("LoadClass", user.character);
	}
	
	function updateStatsList(dex, str, mana, health, oh, th, bow, xbow, souls)
	{
		stats_dex = dex;
		stats_str = str;
		stats_mana = mana;
		stats_health = health;
		stats_oh = oh;
		stats_th = th;
		stats_bow = bow;
		stats_xbow = xbow;
		stats_souls = souls;
	}
	
	function switchStatus(st)
	{
		status = st;
	}

	function toggleFreeze(trigger)
	{
		setFreeze(trigger);
	}
	
	password = null;
	autorized = false;
	status = null;
	fully_loaded = false;
	onGUI = false;
	freezed = false;
	id = -1;
	
	inGame = null;
	name = null;
	character = null;
	characters = null;
	description = null;
	
	message_count = null;
	message = [null, null, null, null, null, null, null, null, null, null, null];
	message_current = null;
	
	stats_dex = 10;
	stats_str = 10;
	stats_mana = 0;
	stats_health = 100;
	stats_oh = 10;
	stats_th = 10;
	stats_bow = 10;
	stats_xbow = 10;
	stats_souls = 10;
}

user <- Player();

function RequireLogin()
{
	//user.password = password;
	user.toggleFreeze(true);
	callLogin();
}

function RequireRegister()
{
	user.toggleFreeze(true);
}

function ReceiveID(pid)
{
	user.id = pid;
}

function dropCharacters()
{
	user.characters.clear();
	user.description.clear();
	UpdateCChoices();
}

function addExistingCharacter(name, description)
{
	user.characters.append(name);
	user.description.append(description);
	UpdateCChoices();
}

function setPlayerCharacter(slot)
{
	user.character = user.characters[slot];
}

function setInGame(toggle)
{
	if(user.character && !user.inGame && toggle)
		{
			user.inGame = true;
			callServerFunc("LoadLetters", getID());
			callServerFunc("LoadRPData", getID());
			callServerFunc("LoadPlayerPosition", getID());
			SetFunctionsActive();
		}
	else
	{
		user.inGame = false;
		callServerFunc("setPlayerWorld", heroId, "LOGIN.ZEN");
		ShowCharacterListMenu();
	}
}



function getName()
{
	if(user.character)
		return user.character;
	else
		return 0;
}

function getID()
{
	return user.id;
}

function RespondLogin(success)
{
	if(success)
	{
		login_menu.hide();
		setAutorized(); 
		callServerFunc("sendMessageToAll", 0, 255, 0, getPlayerName(heroId)+" ������� ���������������!");
	}
	else 
	{
		login_menu.label_field[5].update("Wrong password!");
		login_menu.label_field[5].timed_clean(2);
		login_menu.edit_box[0].text = "";		
		login_menu.edit_box[0].mask = "";		
		login_menu.edit_box[0].update(false);								
	}	
}

function setAutorized()
{
	if(!user.autorized)
	{
		user.autorized = true;
		//user.toggleFreeze(false);
		if (register_menu.active)
		{
			register_menu.hide();
		}
		else if (login_menu.active)
		{
			login_menu.hide();
		}
		
		ShowCharacterListMenu();
		//callServerFunc("setPlayerInvisible", getID(), false);
	}
}

function spawnPlayer()
{
	callServerFunc("spawnPlayer", getID());
}

function SetFunctionsActive()
{
	spawnPlayer();
	//user.loadStats();
	user.loadClass();
	user.loadVisual();
	user.loadItems();
	user.loadBodyState();
	setTimer(user.saveItems, 13*60*1000, 0);
	setTimer(user.savePosition, 60*1000, 0);
	setTimer(user.saveBodyStatus, 60*1000, 0)
}

function loadPos(name,  x,  y,  z)
{
	user.setPos(x, y, z);
}

function itemsLoaded()
{
	user.fully_loaded = true;
}


function setRightsStatus(st)
{
	user.switchStatus(st);
}

function receiveLoadedStats(dex, str, mana, health, oh, th, bow, xbow, souls)
{
	user.updateStatsList(dex, str, mana, health, oh, th, bow, xbow, souls);
}

function Freeze(boolean)
{
	playAni(heroId, "S_RUN");
	enableHeroMovement(!boolean);	
	user.freezed = boolean;
}

function DropItemBySlot(id, amount)
{
	user.dropItemBySlot(id, amount);
}

function DropItemByName(name, amount)
{
	user.dropItemByName(name, amount);
}

function playerExit()
{
	if(user.autorized && user.inGame && getPlayerInstance(getID()) == "PC_HERO")
	{
		user.saveItems();
		user.savePosition();
		user.saveBodyStatus();
	}
	
	receiveMessageAM("����������...", "stroke");
	local draw_shade =  drawCreate(3970, 5970, 0, 0, 0, FONT_OLD_UPPER, "��������");
	local draw = drawCreate(4000, 6000, 255, 255, 0, FONT_OLD_UPPER, "��������");
	drawSetVisible(draw, true); drawSetVisible(draw_shade, true); 
	setTimer(Exit, 1500, 1);
}

function playerLogout()
{
	if(user.autorized && user.inGame && getPlayerInstance(getID()) == "PC_HERO")
	{
		user.saveItems();
		user.savePosition();
		user.saveBodyStatus();
	}
	
	receiveMessageAM("����������...", "stroke");
	setTimer(setInGame, 1500, 1, false);
}

local playstatus_draw = drawCreate(6000, 6500, 255, 255, 0, FONT_OLD_UPPER, "Playstatus");
local playstatus_bg = txtCreate(5192, 6300, 3000, 400, BG_TEXTURE);
local playstatus_timer;
local playstatus_id = -1;

function receiveStatus(playstatus)
{
	local text = "";
	if(playstatus == "menu")
		{
			text = "� ����(AFK)";
		}
	else if(playstatus == "stats")
		{
			text = "�����. ���.";		
		}
	else if(playstatus == "skinedit")
		{
			text = "����������� ����";		
		}
	else if(playstatus == "animations")
		{
			text = "��. ��������";		
		}
	else if(playstatus == "login")
		{
			text = "������ � ����";		
		}
	else if(playstatus == "register")
		{
			text = "��������������";		
		}
	else if(playstatus == "adminpanel")
		{
			text = "���. (AFK)";		
		}
	if(playstatus == "none" || playstatus_id == -1)
		{
			drawSetVisible(playstatus_draw, false);
			txtSetVisible(playstatus_bg, false);
		}
	else
		{
			local x = 5192+(3000-drawGetWidth(drawCreate(0, 0, 0, 0, 0, "FONT_OLD_20_WHITE_HI.TGA", text)))/2;
			drawSetPosition(playstatus_draw, x, 6500);
			drawSetText(playstatus_draw, text);
			drawSetVisible(playstatus_draw, true);
			txtSetVisible(playstatus_bg, true);		
		}
}

function sendStatus(player_id)
{
	if(main_menu.active)
	{
		callServerFunc("callClientFunc", player_id, "receiveStatus", "menu");
	}
	else if(register_menu.active)
	{
		callServerFunc("callClientFunc", player_id, "receiveStatus", "register");
	}
	else if(login_menu.active)
	{
		callServerFunc("callClientFunc", player_id, "receiveStatus", "login");
	}
	else if(skinedit_menu.active)
	{
		callServerFunc("callClientFunc", player_id, "receiveStatus", "skinedit");
	}
	else if(adminpanel.active)
	{
		callServerFunc("callClientFunc", player_id, "receiveStatus", "adminpanel");
	}
	else if(anim1.active || anim2.active || anim3.active || anim4.active || anim5.active || anim6.active || anim7.active)
	{
		callServerFunc("callClientFunc", player_id, "receiveStatus", "animations");
	}
	else if(soulmenu.active)
	{
		callServerFunc("callClientFunc", player_id, "receiveStatus", "stats");
	}
	else
		callServerFunc("callClientFunc", player_id, "receiveStatus", "none");
}


function statusRequest()
{
	callServerFunc("callClientFunc", playstatus_id, "sendStatus", getID());
}

addEventHandler("onTakeFocus", function(id, name)
{
	//print(name);
	playstatus_id = id;
	statusRequest();
	playstatus_timer = setTimer(statusRequest, 5000, 0);
});

addEventHandler("onLostFocus", function(pid, name)
{
	//print(name);
	killTimer(playstatus_timer);
	playstatus_id = -1;
	receiveStatus("none");
});

function Exit()
{
	exitGame();
}

/*addEventHandler("onExit",  function()
{

});*/

addEventHandler("onKey",  function(key)
{
	//if(user.autorized && user.fully_loaded)
	//{
	//	user.savePosition();
	//}
	switch(key)
	{
		case KEY_F9:
			//playerExit();
			local pos = getPlayerPosition(heroId)
			//print(pos.x+"--"+pos.y+"--"+pos.z+"--"+getAngle());
		break;
	}
});

addEventHandler("onTakeItem",  function(instance, amount, synchronized)
{
	if(user.autorized && user.inGame && user.fully_loaded && getPlayerInstance(getID()) == "PC_HERO")
	{
		setTimer(user.saveItems, 1000, 1);
	}
});

addEventHandler("onDropItem",  function(instance, amount, synchronized)
{
	if(user.autorized && user.inGame && user.fully_loaded && getPlayerInstance(getID()) == "PC_HERO")
	{
		setTimer(user.saveItems, 1000, 1);
	}
});

addEventHandler("onDie",  function()
{
	if(user.autorized && user.inGame && user.fully_loaded && getPlayerInstance(getID()) == "PC_HERO")
	{
		user.saveItems();
		user.fully_loaded = false;
	}
	
	setTimer(spawnPlayer, 5000, 1);
});

addEventHandler("onPlayerRespawn",  function()
{
	if(user.autorized && !user.fully_loaded && user.inGame)
	{
		setInstance("PC_HERO");
		//user.loadStats();
		user.loadClass();
		user.loadVisual();
		user.loadItems();
		user.loadBodyState();
	}
		sendVisual();
});



//addEventHandler("onDie",  function(instance, amount)
//{
//	if(user.autorized && user.fully_loaded)
//	{
//		setTimer(user.saveItems, 1000, false);
//	}
//});

addEventHandler("onUseItem",  function(instance, amount, hand)
{
	if(user.autorized && user.inGame && user.fully_loaded && getPlayerInstance(getID()) == "PC_HERO")
	{
		setTimer(user.saveItems, 1000, 1);
	}
});
