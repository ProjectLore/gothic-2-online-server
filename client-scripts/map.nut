class MapPlayer
{
	constructor(id)
	{
		local color = getPlayerColor(id);
		
		m_Id = id;
		
		m_DrawName = drawCreate(0, 0, color.r, color.g, color.b, "FONT_OLD_10_WHITE_HI.TGA", "+ " + getPlayerName(id));
	}
	
	function destroy()
	{
		drawDestroy(m_DrawName);
	}
	
	function show()
	{
		drawSetVisible(m_DrawName, true);
	}
	
	function hide()
	{
		drawSetVisible(m_DrawName, false);
	}
	
	function updatePosition(x, y)
	{
		drawSetPosition(m_DrawName, x, y);
	}

	function updateName(name)
	{
		drawSetText(m_DrawName, "+ " + name);
	}
	
	function updateColor(r, g, b)
	{
		drawSetColor(m_DrawName, r, g, b);
	}
	
	m_Id = -1;
	m_DrawName = -1;
};

class Map
{
	constructor()
	{
		m_TexMap = -1;
		m_isShowed = false;
		m_List = [];
		
		changeMap(getWorld());
		
		setTimer(function(map)
		{
			foreach (player in map.m_List)
			{
				local position = getPlayerPosition(player.m_Id);
				local mapPos = calc2DMapPosition(position.x, position.z);
				
				player.updatePosition(mapPos.x, mapPos.y);
			}
			
		}, 500, true, this);
	}
	
	function toggle()
	{
		m_isShowed ? hide() : show();
	}
	
	function show()
	{
		if (m_TexMap != -1) txtSetVisible(m_TexMap, true);
		
		foreach (player in m_List)
			player.show();

		m_isShowed = true;
	}
	
	function hide()
	{
		if (m_TexMap != -1) txtSetVisible(m_TexMap, false);
	
		foreach (player in m_List)
			player.hide();
			
		m_isShowed = false;
	}

	function insert(pid)
	{
		m_List.append(MapPlayer(pid));
	}
	
	function remove(pid)
	{
		local index = null;
		foreach (id, player in m_List)
			if (player.m_Id == pid)
				index = id;
				
		if (index != null) m_List.remove(index);
	}
	
	function changeMap(world)
	{
		if (m_TexMap == -1)
		{
			switch (world)
			{
			case "NEWWORLD\\NEWWORLD.ZEN":
				m_TexMap = txtCreate(0, 0, 8192, 8192, "MAP_NEWWORLD.TGA");
				setMapLevelCoords(-28000, 50500, 95500, -42500);
				break;
				
			case "OLDWORLD\\OLDWORLD.ZEN":
				m_TexMap = txtCreate(0, 0, 8192, 8192, "MAP_OLDWORLD.TGA");
				setMapLevelCoords(-78500, 47500, 54000, -53000);
				break;
				
			case "ADDON\\ADDONWORLD.ZEN":
				m_TexMap = txtCreate(0, 0, 8192, 8192, "MAP_ADDONWORLD.TGA");
				setMapLevelCoords(-47783, 36300, 43949, -32300);
				break;
			}
		}
		else
		{
			switch (world)
			{
			case "NEWWORLD\\NEWWORLD.ZEN":
				txtSetFilename(m_TexMap, "MAP_NEWWORLD.TGA");
				setMapLevelCoords(-28000, 50500, 95500, -42500);
				break;
				
			case "OLDWORLD\\OLDWORLD.ZEN":
				txtSetFilename(m_TexMap, "MAP_OLDWORLD.TGA");
				setMapLevelCoords(-78500, 47500, 54000, -53000);
				break;
				
			case "ADDON\\ADDONWORLD.ZEN":
				txtSetFilename(m_TexMap, "MAP_ADDONWORLD.TGA");
				setMapLevelCoords(-47783, 36300, 43949, -32300);
				break;
			}
		}
	}
	
	function updateName(pid, name)
	{
		foreach (player in m_List)
			if (player.m_Id == pid)
				player.updateName(name);
	}
	
	function updateColor(pid, r, g, b)
	{
		foreach (player in m_List)
			if (player.m_Id == pid)
				player.updateColor(r, g, b);
	}
	
	m_isShowed = false;
	m_TexMap = -1;
	m_List = null;
};

local map = Map();

addEventHandler("onPlayerCreated", function(pid, ping)
{
	map.insert(pid);
});

addEventHandler("onPlayerDestroyed", function(pid)
{
	map.remove(pid);
});

addEventHandler("onPlayerChangeName", function(pid, name)
{
	map.updateName(pid, name);
});

addEventHandler("onPlayerChangeColor", function(pid, r, g, b)
{
	map.updateColor(pid, r, g, b);
});

addEventHandler("onWorldEnter", function(world)
{
	map.changeMap(world);
});

addEventHandler("onKey", function(key)
{
	if (!chatInputIsOpen() && key == KEY_M)
		map.toggle();
});