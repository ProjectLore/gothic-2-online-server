//import ("modules/GO_lFileLoader");

class Log
{
	constructor(label, dir)
	{
		name = label;
		directory = dir;
		text = [];
	}
	
	function addLog(txt)
	{
		text.append(txt);
	}
	
	function addTimedLog(txt)
	{
		local time = date();
		local additional_txt = format("[%d:%d:%d] %s", time.hour, time.min, time.sec, txt);
		text.append(additional_txt);
	}
	
	function saveLog()
	{
		local sum_dir = format("%s/%s.txt", directory, name);
		local file_pointer = io.file(sum_dir,"a+");
		if(text.len() > 0)
		{
			text.reverse();
			file_pointer.write(text.pop());
		}
		file_pointer.close();
	}
	
	name = null;
	text = null;
	directory = null;
}

local cd = date();
globalChatLog <- Log(format("%d_%d_%d_timed_%d_%d_%d_chatlog", cd.year, cd.month, cd.day, cd.hour, cd.min, cd.sec), "logs/chat");
whisperChatLog <- Log(format("%d_%d_%d_timed_%d_%d_%d_whisperlog", cd.year, cd.month, cd.day, cd.hour, cd.min, cd.sec), "logs/chat");
adminChatLog <- Log(format("%d_%d_%d_timed_%d_%d_%d_adminlog", cd.year, cd.month, cd.day, cd.hour, cd.min, cd.sec), "logs/chat");
globalChatLog.addTimedLog("Server started");
//globalChatLog.saveLog();
whisperChatLog.addTimedLog("Server started");
//whisperChatLog.saveLog();
adminChatLog.addTimedLog("Server started");
//adminChatLog.saveLog();