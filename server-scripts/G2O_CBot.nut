//By Sative
local LIMIT_BOTS = 32;

enum WeaponMode
{
	NONE,
	FIST,
	DAG,
	ONEH,
	TWOH,
	BOW, 
	CBOW,
	MAG
};

local botStructure = {};
for(local i = 0; i < LIMIT_BOTS; ++i)
{
	botStructure[i] <- {};
	botStructure[i].isSpawned <- false;
	botStructure[i].bot <- 0;
}

addEvent("onJoin", function(pid)
{
	for(local i = 0; i < LIMIT_BOTS; ++i)
		if(botStructure[i].isSpawned == true)
			botStructure[i].bot.createForPlayer(pid);
	
	callClientFunc(pid, "bot_setPlayerId", pid);
});

function getBot(botID) //Globalna funkcja, nie wiadomo czy inne pliki beda mialy dostep do struktury botow
{
	if(botStructure[i].isSpawned == true)
		return botStructure[i].bot;
	else return NULL;
}

function __botFreeID()
{
	for(local i = 0; i < LIMIT_BOTS; ++i)
	{
		if(botStructure[i].isSpawned == false)
			return i;
	};
	print("[Bots Module] Fatal error! TOO MANY BOTS");
	return -1;
};

class Bot
{
	constructor(id, name)
	{
		m_Id = id;
		m_Name = name;
		
		//Default events
		onDamage = function(killerID, damage){print("Player " + getPlayerName(killerID) + " punched bot " + m_Name + " with damage " + damage);};
		onDie = function(killerID){print("Player " + getPlayerName(killerID) + " killed bot " + m_Name);};
		onPosition = function(x, y, z){};
		onUpdate = function(){};
		
		//Create for all
		for(local i = 0; i < getMaxSlots(); ++i)
			if(isPlayerConnected(i) == true)
				createForPlayer(i);
				
		print("Bot " + m_Name + " with id: " + m_Id + " has been spawned.");
	}
	
	function createForPlayer(playerID)
	{
		//ID, instance, world, x, y, z, angle, strength, dexterity, health, maxhealth, mana, maxmana, weaponmode, immortal, animation, armor, melee, ranged, name
		local packet = format("%d %s %s %f %f %f %f %d %d %d %d %d %d %d %d %s %s %s %s %s",
		m_Id, m_Instance, m_World, m_PosX, m_PosY, m_PosZ, m_Angle, m_Strength, m_Dexterity, m_Health, m_HealthMax, m_Mana, m_ManaMax, m_WeaponMode, m_Immortal ? 1 : 0, m_Animation, m_Armor, m_MeleeWeapon, m_RangedWeapon, m_Name);
		callClientFunc(playerID, "bot_onCreate", packet);
	}
	
	function destroyForPlayer(playerID)
	{
		callClientFunc(playerID, "bot_onDestroy", m_Id);
	}
	
	function setWorld(world)
	{
		m_World = world;
		
		for(local i = 0; i < getMaxSlots(); ++i)
			if(isPlayerConnected(i) == true)
				callClientFunc(i, "bot_onSetWorld", m_Id, world);
	}
	
	function setPosition(x, y, z)
	{
		m_PosX = x;
		m_PosY = y;
		m_PosZ = z;
		//Update for all
		for(local i = 0; i < getMaxSlots(); ++i)
			if(isPlayerConnected(i) == true)
				callClientFunc(i, "bot_onSetPosition", format("%d %f %f %f", m_Id, x, y, z));
	}
	
	function setAngle(angle)
	{
		m_Angle = angle;
		//Update for all
		for(local i = 0; i < getMaxSlots(); ++i)
			if(isPlayerConnected(i) == true)
				callClientFunc(i, "bot_onSetAngle", format("%d %f", m_Id, angle));
	}
	
	function setHealth(health)
	{
		m_Health = health;
		
		for(local i = 0; i < getMaxSlots(); ++i)
			if(isPlayerConnected(i) == true)
				callClientFunc(i, "bot_onSetHealth", m_Id, health);
	}
	
	function setMana(mana)
	{
		m_Mana = mana;
		
		for(local i = 0; i < getMaxSlots(); ++i)
			if(isPlayerConnected(i) == true)
				callClientFunc(i, "bot_onSetMana", m_Id, mana);
	}
	
	function setMaxMana(mana)
	{
		m_ManaMax = mana;
		
		for(local i = 0; i < getMaxSlots(); ++i)
			if(isPlayerConnected(i) == true)
				callClientFunc(i, "bot_onSetMaxMana", m_Id, mana);
	}
	
	function setMaxHealth(health)
	{
		m_HealthMax = health;
		
		for(local i = 0; i < getMaxSlots(); ++i)
			if(isPlayerConnected(i) == true)
				callClientFunc(i, "bot_onSetMaxHealth", m_Id, health);
	}
	
	function setStrength(strength)
	{
		m_Strength = strength;
		
		for(local i = 0; i < getMaxSlots(); ++i)
			if(isPlayerConnected(i) == true)
				callClientFunc(i, "bot_onSetStrength", m_Id, strength);
	}
	
	function setDexterity(dexterity)
	{
		m_Dexterity = dexterity;
		
		for(local i = 0; i < getMaxSlots(); ++i)
			if(isPlayerConnected(i) == true)
				callClientFunc(i, "bot_onSetDexterity", m_Id, dexterity);
	}
	
	function setWeaponMode(weaponMode)
	{
		m_WeaponMode = weaponMode;
		for(local i = 0; i < getMaxSlots(); ++i)
			if(isPlayerConnected(i) == true)
				callClientFunc(i, "bot_onSetWeaponMode", m_Id, weaponMode);
	}
	
	function setImmortal(enable)
	{
		m_Immortal = enable;
		for(local i = 0; i < getMaxSlots(); ++i)
			if(isPlayerConnected(i) == true)
				callClientFunc(i, "bot_onSetImmortal", m_Id, enable ? 1 : 0);
	}
	function playAnimation(aniname)
	{
		m_Animation = aniname;
		for(local i = 0; i < getMaxSlots(); ++i)
			if(isPlayerConnected(i) == true)
				callClientFunc(i, "bot_onPlayAni", format("%d %s", m_Id, aniname));
	}
	
	function stopAnimation()
	{
		m_Animation = "S_RUNS"; //Standart ani
		for(local i = 0; i < getMaxSlots(); ++i)
			if(isPlayerConnected(i) == true)
				callClientFunc(i, "bot_onStopAni", m_Id);
	}
	
	function setInstance(instance)
	{
		m_Instance = instance;
		for(local i = 0; i < getMaxSlots(); ++i)
			if(isPlayerConnected(i) == true)
				callClientFunc(i, "bot_onSetInstance", m_Id, instance);
	}
	
	function wearArmor(instance)
	{
		m_Armor = instance;
		for(local i = 0; i < getMaxSlots(); ++i)
			if(isPlayerConnected(i) == true)
				callClientFunc(i, "bot_onWearArmor", m_Id, instance);
	}
	
	function wearMeleeWeapon(instance)
	{
		m_MeleeWeapon = instance;
		for(local i = 0; i < getMaxSlots(); ++i)
			if(isPlayerConnected(i) == true)
				callClientFunc(i, "bot_onWearMeleeWeapon", m_Id, instance);
	}
	
	function wearRangedWeapon(instance)
	{
		m_MeleeWeapon = instance;
		for(local i = 0; i < getMaxSlots(); ++i)
			if(isPlayerConnected(i) == true)
				callClientFunc(i, "bot_onWearRangedWeapon", m_Id, instance);
	}
	
	function removeArmor()
	{
		m_Armor = "NULL";
		for(local i = 0; i < getMaxSlots(); ++i)
			if(isPlayerConnected(i) == true)
				callClientFunc(i, "bot_onRemoveArmor", m_Id);
	}
	
	function removeMeleeWeapon()
	{
		m_MeleeWeapon = "NULL";
		for(local i = 0; i < getMaxSlots(); ++i)
			if(isPlayerConnected(i) == true)
				callClientFunc(i, "bot_onRemoveMeleeWeapon", m_Id);
	}

	function removeRangedWeapon()
	{
		m_RangedWeapon = "NULL";
		for(local i = 0; i < getMaxSlots(); ++i)
			if(isPlayerConnected(i) == true)
				callClientFunc(i, "bot_onRemoveRangedWeapon", m_Id);
	}
	
	function isDead()
	{
		return m_Health == 0;
	}
	
	function hitPlayer(playerID)
	{
		callClientFunc(playerID, "bot_onHitPlayer", m_Id);
	}
	
	function reSpawn()
	{
		setHealth(m_HealthMax);
	}
	
	function setAngleToPos(x, z)
	{
		if(m_PosX == x && m_PosZ == z)
			m_Angle = 0;
		else
		{
			x = x - m_PosX;
			z = z - m_PosZ;
			m_Angle = atan(x/z) * 180.0 / 3.14;
			if(z < 0)
				m_Angle = m_Angle >= 180 ? m_Angle - 180 : m_Angle + 180;
		}
		setAngle(m_Angle);
	}
	
	function update()
	{
		//Update position
		//Find nearest player
		if(TIME_BOT_POS < getTickCount())
		{
			for(local i = 0; i < getMaxSlots(); ++i)
			{
				if(isPlayerConnected(i) == true)
				{
					local pos = getPlayerPosition(i);
					if( getDistance3D(pos.x, pos.y, pos.z, m_PosX, m_PosY, m_PosZ) < 3000 )
					{
						callClientFunc(i, "secret_gimmeBotPos", m_Id);
						break;
					}
				}
			}
			TIME_BOT_POS = getTickCount() + 300;
		}
		//Callback
		onUpdate();
	}
	
	//EVENTS!
	onDamage = 0;
	onDie = 0;
	onPosition = 0;
	onUpdate = 0;
	
	m_Id = -1;
	m_Name = "Me";
	m_Instance = "PC_HERO";
	m_Animation = "S_RUNS";
	m_World = "NEWWORLD\\NEWWORLD.ZEN";
	m_Armor = "NULL";
	m_MeleeWeapon = "NULL";
	m_RangedWeapon = "NULL";
	m_WeaponMode = WeaponMode.NONE;
	m_PosX = 0.0;
	m_PosY = 0.0;
	m_PosZ = 0.0;
	m_Angle = 0;
	m_Health = 40;
	m_HealthMax = 40;
	m_Mana = 10;
	m_ManaMax = 10;
	m_Strength = 10;
	m_Dexterity = 10;
	m_Immortal = false;
	
	TIME_BOT_POS = 0;
};

function createBot(name)
{
	local id = __botFreeID();
	botStructure[id].bot = Bot(id, name);
	botStructure[id].isSpawned = true;
	
	return botStructure[id].bot;
};

function destroyBot(id)
{
	botStructure[id].isSpawned = false;
	for(local i = 0; i < getMaxSlots(); ++i)
		botStructure[id].bot.destroyForPlayer(i);
	botStructure[id].bot = 0;
};

function _onBotPosition(params)
{
	local args = sscanf("dfff", params);
	if(botStructure[args[0]].isSpawned == true)
	{
		if(getDistance3D(args[1], args[2], args[3], botStructure[args[0]].bot.m_PosX, botStructure[args[0]].bot.m_PosY, botStructure[args[0]].bot.m_PosZ) < 400)
		{
			botStructure[args[0]].bot.setPosition(args[1], args[2], args[3]);
			botStructure[args[0]].bot.onPosition(args[1], args[2], args[3]);
		}
	}
};

function _onBotDamage(playerID, botID, minushealth)
{
	if( botStructure[botID].isSpawned == true )
	{
		local __bot = botStructure[botID].bot;
		if( minushealth >= __bot.m_Health || __bot.m_Health == 1 || (__bot.m_Health - minushealth) == 1 )
		{
			__bot.setHealth(0);
			__bot.onDie(playerID);
		}
		else
		{		
			__bot.setHealth(__bot.m_Health - minushealth);
			__bot.onDamage(playerID, minushealth);
		}

	}
};

addEvent("onTick", function()
{
	//Update bots
	for(local i = 0; i < LIMIT_BOTS; ++i)
		if(botStructure[i].isSpawned == true)
			botStructure[i].bot.update();
});