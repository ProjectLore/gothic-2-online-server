
class ChatSavings
{
	constructor()
	{
		messages = [];
		color_r = [];
		color_g = [];
		color_b = [];
	}
	
	function add(r, g, b, text)
	{
		messages.append(text);
		color_r.append(r);
		color_g.append(g);
		color_b.append(b);
	}
	
	messages = [];
	color_r = [];
	color_g = [];
	color_b = [];
}

global <- ChatSavings();
admin <- ChatSavings();
s_status <- ChatSavings();

function saveChatLogs()
{
	globalChatLog.saveLog();
	whisperChatLog.saveLog();
	adminChatLog.saveLog();
}
local savingTimer = setTimer(saveChatLogs, 25*1000, 0);

oldSendMessage <- sendMessageToPlayer;
function sendMessageToPlayer(id, r, g, b, text)
{
	//globalChatLog.addTimedLog("To player "+getPlayerName(id)+" > "+text);
	oldSendMessage(id, r, g, b, text);
}


readress_smta <- sendMessageToAll;
function sendMessageToAll(r, g, b, text)
{
	globalChatLog.addTimedLog(text);
	
	local local_date = date();
	local hour = format("%d", local_date.hour);
	local minutes;
	if(local_date.min < 10)
		minutes = format("0%d", local_date.min);
	else 
		minutes = format("%d", local_date.min);
	local seconds;
	if(local_date.sec < 10)
		seconds = format("0%d", local_date.sec);
	else 
		seconds = format("%d", local_date.sec);
		
	global.add(r, g, b, format("|%s:%s:%s| %s", hour, minutes, seconds, text));
	readress_smta(r, g, b, text);
}

readress_ss <- StatisticMessage;
function StatisticMessage(text)
{
	local local_date = date();
	local hour = format("%d", local_date.hour);
	local minutes;
	if(local_date.min < 10)
		minutes = format("0%d", local_date.min);
	else 
		minutes = format("%d", local_date.min);
	local seconds;
	if(local_date.sec < 10)
		seconds = format("0%d", local_date.sec);
	else 
		seconds = format("%d", local_date.sec);
		
	s_status.add(241, 247, 151, format("|%s:%s:%s| %s", hour, minutes, seconds, text));
	readress_ss(text);
}

function AdminMessage(text)
{
	for (local i = 0; i < getMaxSlots (); ++ i)
		if (isPlayerConnected (i))
			callClientFunc(i, "ReceiveAdmin", -1, text);
}

function AdminPlayerMessage(id, text)
{
	adminChatLog.addTimedLog(getPlayerName(id)+" : "+text);
	for (local i = 0; i < getMaxSlots (); ++ i)
		if (isPlayerConnected (i))
			callClientFunc(i, "ReceiveAdmin", id, text);
}

function ReplyMessage(sender_id, receiver_id, message)
{
	adminChatLog.addTimedLog("(���. �����) �� "+getPlayerName(sender_id)+" � "+getPlayerName(receiver_id)+" : "+message);
	StatisticMessage("(���. �����) �� "+getPlayerName(sender_id)+" � "+getPlayerName(receiver_id)+" : "+message);
	AdminMessage("(�����) �� " + getPlayerName(sender_id) + " � " + getPlayerName(receiver_id) + " : " + message);
	callClientFunc(receiver_id, "ReceiveReply", sender_id, message);
}

function PrivateMessage(sender_id, receiver_id, message)
{
	whisperChatLog.addTimedLog("�� "+getPlayerName(sender_id)+" � "+getPlayerName(receiver_id)+" : "+message);
	StatisticMessage("�� "+getPlayerName(sender_id)+" � "+getPlayerName(receiver_id)+" : "+message);
	callClientFunc(receiver_id, "ReceivePrivate", sender_id, message);
}

function ArenaMessage(text)
{
	for (local i = 0; i < getMaxSlots (); ++ i)
		if (isPlayerConnected (i))
			callClientFunc(i, "ReceiveAdditionalServer", "arena", text, 241, 247, 151);
}

function InfoMessage(id, text)
{
	if (isPlayerConnected (id))
		callClientFunc(id, "ReceiveAdditionalServer", "info", text, 255, 255, 0);
}

addEventHandler("onPlayerJoin", function(pid)
{

})