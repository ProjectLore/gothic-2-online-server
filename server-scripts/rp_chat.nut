addEventHandler("onInit", function()
{
	print("ORS: RP chat")
	print("build test v00000000000000000000x1")
});


function SendNormalMessage(object, text)
{
	local objectP = getPlayerPosition(object);
	for (local i = 0; i < getMaxSlots (); ++ i)
		if(i != object)
		{
			local P = getPlayerPosition(i);
			if(getPlayerWorld(object) == getPlayerWorld(i))
				if(getPlayerVirtualWorld(object) == getPlayerVirtualWorld(i))
					if(getDistance3D(objectP.x, objectP.y, objectP.z, P.x, P.y, P.z) <= 800)
						callClientFunc(i, "ReceiveRP", format("%s: %s", getPlayerName(object), text), 255, 255, 255);
		}
		else if (i == object)
			callClientFunc(i, "ReceiveRP", format("%s: %s", getPlayerName(object), text), 255, 255, 255);
			//callClientFunc(i, "ReceiveRP", format("��: %s", text), 255, 255, 255
	
	globalChatLog.addTimedLog(format("%s: %s", getPlayerName(object), text));
}

function SendShoutMessage(object, text)
{
	local objectP = getPlayerPosition(object);
	for (local i = 0; i < getMaxSlots (); ++ i)
		if(i != object)
		{
			local P = getPlayerPosition(i);
			if(getPlayerWorld(object) == getPlayerWorld(i))
				if(getPlayerVirtualWorld(object) == getPlayerVirtualWorld(i))
					if(getDistance3D(objectP.x, objectP.y, objectP.z, P.x, P.y, P.z) <= 2750)
						callClientFunc(i, "ReceiveRP", format("%s ���������: %s", getPlayerName(object), text), 255, 255, 100);
		}
		else if (i == object)
			callClientFunc(i, "ReceiveRP", format("%s ���������: %s", getPlayerName(object), text), 255, 255, 100);
			//callClientFunc(i, "ReceiveRP", format("�� ��������: %s", text), 255, 255, 100);
			
	globalChatLog.addTimedLog(format("%s ���������: %s", getPlayerName(object), text));
}

function SendWhisperMessage(object, text)
{
	local objectP = getPlayerPosition(object);
	for (local i = 0; i < getMaxSlots (); ++ i)
		if(i != object)
		{
			local P = getPlayerPosition(i);
			if(getPlayerWorld(object) == getPlayerWorld(i))
				if(getPlayerVirtualWorld(object) == getPlayerVirtualWorld(i))
					if(getDistance3D(objectP.x, objectP.y, objectP.z, P.x, P.y, P.z) <= 250)
						callClientFunc(i, "ReceiveRP", format("%s �����: %s", getPlayerName(object), text), 180, 180, 180);
		}
		else if (i == object)
			callClientFunc(i, "ReceiveRP", format("%s �����: %s", getPlayerName(object), text), 180, 180, 180);
			//callClientFunc(i, "ReceiveRP", format("�� �������: %s", text), 180, 180, 180
			
	globalChatLog.addTimedLog(format("%s �����: %s", getPlayerName(object), text));
}

function SendFPMessage(object, text)
{
	local objectP = getPlayerPosition(object);
	for (local i = 0; i < getMaxSlots (); ++ i)
		{
			local P = getPlayerPosition(i);
			if(getPlayerWorld(object) == getPlayerWorld(i))
				if(getPlayerVirtualWorld(object) == getPlayerVirtualWorld(i))
					if(getDistance3D(objectP.x, objectP.y, objectP.z, P.x, P.y, P.z) <= 1000)
						callClientFunc(i, "ReceiveRP", format("%s %s", getPlayerName(object), text), 255, 255, 0);
		}
		
	globalChatLog.addTimedLog(format("%s %s", getPlayerName(object), text));
}

function SendTHPMessage(object, text)
{
	local objectP = getPlayerPosition(object);
	for (local i = 0; i < getMaxSlots (); ++ i)
		if(i != object)
		{
			local P = getPlayerPosition(i);
			if(getPlayerWorld(object) == getPlayerWorld(i))
				if(getPlayerVirtualWorld(object) == getPlayerVirtualWorld(i))
					if(getDistance3D(objectP.x, objectP.y, objectP.z, P.x, P.y, P.z) <= 1000)
						callClientFunc(i, "ReceiveRP", format("DO(%s): %s", getPlayerName(object), text), 0, 255, 255);
		}
		else if (i == object)
			callClientFunc(i, "ReceiveRP", format("DO(%s): %s", getPlayerName(object), text), 0, 255, 255);
			//callClientFunc(i, "ReceiveRP", format("DO(��): %s", text), 0, 255, 255);
			
	globalChatLog.addTimedLog(format("DO(%s): %s", getPlayerName(object), text));
}

function SendAdminTHPMessage(object, text)
{
	local objectP = getPlayerPosition(object);
	for (local i = 0; i < getMaxSlots (); ++ i)
		{
		local P = getPlayerPosition(i);
		if(getPlayerWorld(object) == getPlayerWorld(i))
			if(getPlayerVirtualWorld(object) == getPlayerVirtualWorld(i))
				if(getDistance3D(objectP.x, objectP.y, objectP.z, P.x, P.y, P.z) <= 1500)
					callClientFunc(i, "ReceiveRP", text, 100, 255, 255);
		}
}

function SendOOC(object, text)
{
	local objectP = getPlayerPosition(object);
	for (local i = 0; i < getMaxSlots (); ++ i)
		if(i != object)
		{
			local P = getPlayerPosition(i);
			if(getPlayerWorld(object) == getPlayerWorld(i))
				if(getPlayerVirtualWorld(object) == getPlayerVirtualWorld(i))
					if(getDistance3D(objectP.x, objectP.y, objectP.z, P.x, P.y, P.z) <= 1750)
						callClientFunc(i, "ReceiveOOC", format("(OOC %s): %s", getPlayerName(object), text), 150, 150, 150);
		}
		else if (i == object)
			callClientFunc(i, "ReceiveOOC", format("(OOC %s): %s", getPlayerName(object), text), 150, 150, 150);
			//callClientFunc(i, "ReceiveOOC", format("(OOC ��): %s", text), 150, 150, 150);
			
	globalChatLog.addTimedLog(format("(OOC %s): %s", getPlayerName(object), text));
}

function SendGlobalOOC(object, text)
{
	for (local i = 0; i < getMaxSlots (); ++ i)
		//if(i != object)
			callClientFunc(i, "ReceiveOOC", format("(G-OOC %s): %s", getPlayerName(object), text), 150, 150, 150);
		//else if (i == object)
			//callClientFunc(i, "ReceiveOOC", format("(G-OOC ��): %s", text), 150, 150, 150);
			
	globalChatLog.addTimedLog(format("(G-OOC %s): %s", getPlayerName(object), text));
}

function SendGM(object, text)
{
	local objectP = getPlayerPosition(object);
	for (local i = 0; i < getMaxSlots (); ++ i)
		//if(i != object)
		{
			local P = getPlayerPosition(i);
			if(getPlayerWorld(object) == getPlayerWorld(i))
				if(getPlayerVirtualWorld(object) == getPlayerVirtualWorld(i))
					if(getDistance3D(objectP.x, objectP.y, objectP.z, P.x, P.y, P.z) <= 1200)
						callClientFunc(i, "ReceiveOOC", format("(GM %s): %s", getPlayerName(object), text), 20, 255, 20);
		}
		//else if (i == object)
			//callClientFunc(i, "ReceiveOOC", format("(GM ��): %s", text), 20, 255, 20);
			
	globalChatLog.addTimedLog(format("(GM %s): %s", getPlayerName(object), text));
}

function SendNews(object, text)
{
	for (local i = 0; i < getMaxSlots (); ++ i)
		//if(i != object)
			callClientFunc(i, "ReceiveOOC", format("(News %s): %s", getPlayerName(object), text), 10, 240, 10);
		//else if (i == object)
		//	callClientFunc(i, "ReceiveOOC", format("(News ��): %s", text), 10, 240, 10);
		
	globalChatLog.addTimedLog(format("(News %s): %s", getPlayerName(object), text));
}

function SendRollMessage(object, c, f)
{
	if(f < 0 || c < 0 || c > 100 || f > 10000)
	{
		f = 6; c = 2;
	}
	
	local result = 0;
	for(local i = 1; i <= c; i++)
	{
		local roll = 1.0 * rand() / RAND_MAX;
		roll = (roll * f) + 1;
		result = result + roll.tointeger();
	}
	
	local objectP = getPlayerPosition(object);
	for (local i = 0; i < getMaxSlots (); ++ i)
		//if(i != object)
		{
			local P = getPlayerPosition(i);
			if(getPlayerWorld(object) == getPlayerWorld(i))
				if(getPlayerVirtualWorld(object) == getPlayerVirtualWorld(i))
					if(getDistance3D(objectP.x, objectP.y, objectP.z, P.x, P.y, P.z) <= 1000)
						callClientFunc(i, "ReceiveAdditionalServer", "dice", format("%s ������ �����. ���-��: %d �����: %d ���������: %d", getPlayerName(object), c, f, result), 255, 255, 0);
		}
		//else if (i == object)
		//	callClientFunc(i, "ReceiveAdditionalServer", "dice", format("�� ������� �����. ���-��: %d �����: %d ���������: %d", c, f, result), 255, 255, 0);
		
	globalChatLog.addTimedLog(format("%s ������ �����. ���-��: %d �����: %d ���������: %d", getPlayerName(object), c, f, result));
}


addEventHandler("onPlayerCommand", function(pid, command, params)
{
	switch (command)
	{	
		case "name":	
		case "���":
			local uid_name = sscanf("ds", params);
			if(id_name)
			{
				if(CheckNameApr(uid_name[1]))
					{
						AddKnown(getPlayerName(pid), uid_name[0], id_name[1]);
						callClientFunc(pid, "AddKnown", uid_name[0], id_name[1]);
					}
				else
					individualMessage(pid, "������������ ��� �� �����", "stroke");
			}
			break;
		case "�":
		case "s":
		local par = sscanf("s", params);
			if(par)
			{
				local mes = par[0];
				mes.tostring(); mes = strip(mes);
				if(mes.len() > 0)
					SendShoutMessage(pid, par[0]);
			}
			break;
		case "�":
		case "o":
		local par = sscanf("s", params);
			if(par)
			{
				local mes = par[0];
				mes.tostring(); mes = strip(mes);
				if(mes.len() > 0)
					SendOOC(pid, par[0]);
			}
			break;
		case "��":
		case "og":
		local par = sscanf("s", params);
			if(par)
			{
				local mes = par[0];
				mes.tostring(); mes = strip(mes);
				if(mes.len() > 0)
					SendGlobalOOC(pid, par[0]);
			}
			break;
		case "������":
		case "report":
		local par = sscanf("s", params);
			if(par)
			{
				AdminMessage(format("%s(ID: %d): %s", getPlayerName(pid), pid, par[0]));
				sendMessageToPlayer(pid, 255, 255, 0, "������������� ���� ����������!");
			}
			break;
		case "�":
		case "w":
		local par = sscanf("s", params);
			if(par)
			{
				local mes = par[0];
				mes.tostring(); mes = strip(mes);
				if(mes.len() > 0)
					SendWhisperMessage(pid, par[0]);		
			}
			break;
		case "�":
		case "me":
		local par = sscanf("s", params);
			if(par)
			{
				local mes = par[0];
				mes.tostring(); mes = strip(mes);
				if(mes.len() > 0)
					SendFPMessage(pid, par[0]);
			}
			break;
		case "��":
			local par = sscanf("s", params);
			if(par)
			{
				local mes = par[0];
				mes.tostring(); mes = strip(mes);
				if(mes.len() > 0)
					SendTHPMessage(pid, par[0]);
			}				
		break;
		case "����":
		case "roll":
			local par = sscanf("dd", params);
			if(par)
				SendRollMessage(pid, par[0], par[1]);
			else
				SendRollMessage(pid, 2, 6);
		break;
		case "d":
		case "drop":
		case "�":
		case "����":
		local par;
			if(par = sscanf("dd", params))
				callClientFunc(pid, "DropItemBySlot", par[0], par[1]);
			else if(par = sscanf("ds", params))
				callClientFunc(pid, "DropItemByName", par[1], par[0]);
		break;
		case "������":
		case "help":
			//HelpInfo(pid);
			callClientFunc(pid, "ShowHelp");
			break;
		case "anim":
		case "����":
			AnimInfo(pid);
			break;
		default:
		local rights = "User";
		local qHandler = mysql_connect ("46.39.224.200", "ORSAdmin", "lubluushkischottlera", "g2o", 3911);
		mysql_query(qHandler, "SET NAMES 'cp1251'");
		mysql_query(qHandler, "SET CHARACTER SET 'cp1251'");
		local result = mysql_query (qHandler, "SELECT status FROM playerdata WHERE name='" + getPlayerName(pid) + "'");
				
		if (result)
		if (mysql_num_rows (result) != 0)
			{
					local row_assoc = mysql_fetch_assoc (result);
					if (row_assoc)
				{
					rights = row_assoc["status"];
				}
			}
		mysql_close(qHandler);
		if(isPlayerSpawned(pid) && (rights == "Admin" ||  rights == "Technical Admin" || rights == "Game Master" || rights == "Moderator" || rights == "Assistant"))
		switch(command)
		{			
			case "instance":
			case "�����":
				callClientFunc(pid, "ShowInstanceMenu");
			break;			
			case "myname":	
			case "������":
				local new_name = sscanf("s", params);
				if(new_name)
				{
					if(CheckNameApr(new_name[0]))
						ChangeName(pid, new_name[0]);
					else
						individualMessage(pid, "������������ ������� �� �����", "stroke");
				}
			break;
			case "gm":
			case "��":
				local txt = sscanf("s", params);
				if(txt)
					SendAdminTHPMessage(pid, txt[0]);
			break;
			//case "ado":
			//case "���":
				//local txt = sscanf("s", params);
				//if(txt)
				//	SendGM(pid, txt[0]);
			break;
			case "��":
			case "tp":
				local id = sscanf("dd", params);
				if(id)
				{
					local pos = getPlayerPosition(id[1]);
					setPlayerPosition(id[0], pos.x,  pos.y,  pos.z);
				}
			break;			
			case "goh":
			case "����":
				local id = sscanf("d", params);
				local pos = getPlayerPosition(pid);
				setPlayerPosition(id[0],  pos.x,  pos.y,  pos.z);
			break;
			case "invis":
			case "�����":
				local par = sscanf("d", params)
				if(par)
					if(par[0] == 0)
					{
						callClientFunc(pid, "AddLocalAM", "�����: ����");
						//setPlayerInvisible(pid, false);
						callClientFunc(pid, "removeOverlay", "HUMANS_SPRINT.MDS");
					}
					else
					{
						callClientFunc(pid, "AddLocalAM", "�����: ���");
						//setPlayerInvisible(pid, true);
						callClientFunc(pid, "applyOverlay", "HUMANS_SPRINT.MDS");
					}
			break;
		}
		
		if(isPlayerSpawned(pid) && (rights == "Admin" ||  rights == "Technical Admin" || rights == "Game Master" || rights == "Moderator"))
		switch(command)
		{
			case "news":
			case "�������":
				local txt = sscanf("s", params);
				if(txt)
					SendNews(pid, txt[0]);
			break;
			case "addname":
			case "������":
				local id_name = sscanf("ds", params);
				if(id_name)
					ChangeAdditionalName(id_name[0], id_name[1])
			break;
			case "kick":
			case "���":
				local id = sscanf("ds", params);
				if(id)
					kick(id[0], pid, id[1]);
			break;	
			case "heal":
			case "����":
				local id = sscanf("d", params);
				if(id)
					callClientFunc(id[0], "completeHeal");
				StatisticMessage(getPlayerName(pid) +" ������� ������ "+ getPlayerName(id[0]));
			break;			
			case "freeze":
			case "����":
				local id = sscanf("d", params);
				callClientFunc(id[0], "Freeze", true);
				callClientFunc(id[0], "AddLocalAM", "�������?");
				StatisticMessage(getPlayerName(pid) +" ��������� ������ "+ getPlayerName(id[0]));
			break;	
			case "antifreeze":
			case "��������":
				local id = sscanf("d", params);
				callClientFunc(id[0], "Freeze", false);
				callClientFunc(id[0], "AddLocalAM", "��� ���� ��������. �����");
				StatisticMessage(getPlayerName(pid) +" ���������� ������ "+ getPlayerName(id[0]));
			break;		
		}
		if(isPlayerSpawned(pid) && (rights == "Admin" ||  rights == "Technical Admin" || rights == "Game Master"))
		switch(command)
		{
			case "class":
			case "�����":
				local id_class = sscanf("dd", params);
				if(id_class)
				{
					if(id_class[1] > 0 && id_class[1] <= 17)
					{
						giveClass(id_class[0], id_class[1]);
						StatisticMessage(getPlayerName(pid) +" ����� ����� ������ " + getPlayerName(id_class[0]) + " > (" + getClassName(id_class[1]) + ")");
						sendMessageToPlayer (id_class[0], 255, 120, 0, "��� ������ ����� �����: " + getClassName(id_class[1]));
					}
					else
						individualMessage(pid, "������������ ������� �� �����", "stroke");
				}
				break;
			case "g":
			case "�":
				local txt = sscanf("s", params);
				spreadGlobalMessage(txt[0], "fade");
			break;
			case "gs":
			case "��":
				local txt = sscanf("s", params);
				spreadGlobalMessage(txt[0], "stroke");
			break;
			case "fly":
			case "������":
				local id = sscanf("d", params);
				local pos = getPlayerPosition(id[0]);
				setPlayerPosition(id[0],  pos.x,  pos.y+2000,  pos.z);
			break;
			case "kill":
			case "�����":
				local id = sscanf("d", params);
				callClientFunc(id[0], "setHealth", 0);
				StatisticMessage(getPlayerName(pid) +" ���� (�������) ������ "+ getPlayerName(id[0]));
			break;
			case "give":
				local item = sscanf("dsd", params);
				callClientFunc(item[0], "giveItem", item[1], item[2]);
				StatisticMessage(getPlayerName(pid) +" ����� ������ "+ getPlayerName(item[0]) + " " + item[1] + " x " + item[2]);
			break;
			case "ban":
			case "���":
				local id = sscanf("dds", params);
				if(id)
					ban(id[0], pid, id[1], id[2]);
			break;
			case "spectate":
			case "���������":
				local id = sscanf("d", params);
				callClientFunc(pid, "Spectate", id[0]);
			break;					
			case "nick":	
			case "���":
				local id_name = sscanf("ds", params);
				if(id_name)
				{
					if(CheckNameApr(id_name[1]))
						ChangeName(id_name[0], id_name[1]);
					else
						individualMessage(pid, "������������ ������� �� �����", "stroke");
				}
			break;
			case "marvin":
			case "������":
				local par = sscanf("d", params)
				if(par)
					if(par[0] == 0)
					{
						callClientFunc(pid, "AddLocalAM","������: ����");
						StatisticMessage(getPlayerName(pid) +" �������� ������");
						callClientFunc(pid, "enableMarvin", false);
					}
					else
					{
						callClientFunc(pid, "AddLocalAM","������: ���");
						StatisticMessage(getPlayerName(pid) +" ������� ������");
						callClientFunc(pid, "enableMarvin", true);
					}
			break;
		}
		break;		
	}	
});