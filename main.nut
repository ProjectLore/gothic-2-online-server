function onInit()
{
	print("Gamemode init!");
}

addEventHandlerHandler("onInit", onInit);

function onPlayerJoin(pid)
{
	sendMessageToAll(0, 255, 0, getPlayerName(pid) + " connected with the server.");
	
	spawnPlayer(pid);
	setPlayerHealth(pid, 3000);
	setPlayerMaxHealth(pid, 3000);
	setPlayerStrength(pid, 200);
	setPlayerDexterity(pid, 200);
	setPlayerSkillWeapon(pid, 0, 100);
	setPlayerSkillWeapon(pid, 1, 100);
	setPlayerSkillWeapon(pid, 2, 100);
	setPlayerSkillWeapon(pid, 3, 100);
	equipArmor(pid, "ITAR_PAL_M");
	equipMeleeWeapon(pid, "ITMW_1H_SPECIAL_04");

	setPlayerPosition(pid, 0, 0, 0);
}

addEventHandlerHandler("onPlayerJoin", onPlayerJoin);

function onPlayerRespawn(pid)
{
	sendMessageToAll(255, 100, 0, getPlayerName(pid) + " has respawned.");
	
	spawnPlayer(pid);
	setPlayerHealth(pid, 3000);
	setPlayerMaxHealth(pid, 3000);
	setPlayerStrength(pid, 200);
	setPlayerDexterity(pid, 200);
	setPlayerSkillWeapon(pid, 0, 100);
	setPlayerSkillWeapon(pid, 1, 100);
	setPlayerSkillWeapon(pid, 2, 100);
	setPlayerSkillWeapon(pid, 3, 100);
	equipArmor(pid, "ITAR_PAL_M");
	equipMeleeWeapon(pid, "ITMW_1H_SPECIAL_04");

	setPlayerPosition(pid, 0, 0, 0);
}

addEventHandlerHandler("onPlayerRespawn", onPlayerRespawn);

function onPlayerDead(pid, kid)
{
	if (kid == -1)
		sendMessageToAll(255, 30, 0, getPlayerName(pid) + " kill himself.");
	else
		sendMessageToAll(255, 30, 0, getPlayerName(kid) + " killed " + getPlayerName(pid));
}

addEventHandlerHandler("onPlayerDead", onPlayerDead);


function onPlayerMessage(pid, message)
{
	print(getPlayerName(pid) + " said: " + message);
	sendPlayerMessageToAll(pid, 255, 255, 255, message);
}

addEventHandlerHandler("onPlayerMessage", onPlayerMessage);


function onPlayerDisconnect(pid, reason)
{
	switch (reason)
	{
	case 0:
		sendMessageToAll(255, 0, 0, getPlayerName(pid) + " disconnected from the server.");
		break;
		
	case 1:
		sendMessageToAll(255, 0, 0, getPlayerName(pid) + " lost connection with the server.");
		break;
		
	case 2:
		sendMessageToAll(255, 0, 0, getPlayerName(pid) + " has crashed.");
		break;
	}
}

addEventHandlerHandler("onPlayerDisconnect", onPlayerDisconnect);

function onPlayerEquipArmor(pid, instance)
{
	print(instance)
}

addEventHandlerHandler("onPlayerEquipArmor", onPlayerEquipArmor)